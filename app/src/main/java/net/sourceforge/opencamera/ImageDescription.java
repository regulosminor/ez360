package net.sourceforge.opencamera;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;

public class ImageDescription implements Parcelable {
    public final File image;
    public final String featureTitle;
    public final String featureCode;
    public int sequenceNumber;

    private ImageDescription(Parcel in) {
        image = new File(in.readString());
        featureTitle = in.readString();
        featureCode = in.readString();
        sequenceNumber = in.readInt();
    }

    public ImageDescription(File image, String featureTitle, String featureCode, int sequenceNumber) {
        this.image = image;
        this.featureTitle = featureTitle;
        this.featureCode = featureCode;
        this.sequenceNumber = sequenceNumber;
    }

    public ImageDescription(File image, int sequenceNumber) {
        this.image = image;
        this.featureTitle = "";
        this.featureCode = "";
        this.sequenceNumber = sequenceNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(image.getAbsolutePath());
        parcel.writeString(featureTitle);
        parcel.writeString(featureCode);
        parcel.writeInt(sequenceNumber);
    }

    public static final Parcelable.Creator<ImageDescription> CREATOR
            = new Parcelable.Creator<ImageDescription>() {
        public ImageDescription createFromParcel(Parcel in) {
            return new ImageDescription(in);
        }

        public ImageDescription[] newArray(int size) {
            return new ImageDescription[size];
        }
    };
}
