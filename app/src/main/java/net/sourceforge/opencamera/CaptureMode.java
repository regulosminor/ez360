package net.sourceforge.opencamera;

import android.view.PointerIcon;

public enum CaptureMode {
    POI,
    DETAIL,
    PIP,
    ANIMATED_PICTURE,

    // TODO: not sure if we need this or not
    VIDEO_EXTRACT
}
