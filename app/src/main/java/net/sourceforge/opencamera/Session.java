package net.sourceforge.opencamera;


import android.content.Intent;
import android.os.Bundle;

import com.sister.ivana.CameraRole;

public class Session {
    public static class Builder {
        private static final String EXTRA_MAX_PICTURES = "EXTRA_MAX_PICTURES";
        private static final String EXTRA_AUTOMATIC_PICTURE_MODE = "EXTRA_AUTOMATIC_PICTURE_MODE";
        private static final String EXTRA_MANUAL_PICTURE_MODE = "EXTRA_MANUAL_PICTURE_MODE";
        private static final String EXTRA_SHUTTER_ANIMATION_ENABLED = "EXTRA_SHUTTER_ANIMATION_ENABLED";
        private static final String EXTRA_ANIMATED_GIF_ENABLED = "EXTRA_ANIMATED_GIF_ENABLED";
        private static final String EXTRA_PICTURE_SIZE_WIDTH = "EXTRA_PICTURE_SIZE_WIDTH";
        private static final String EXTRA_PICTURE_SIZE_HEIGHT = "EXTRA_PICTURE_SIZE_HEIGHT";
        private static final String EXTRA_SHOW_GRID_LINES = "EXTRA_SHOW_GRID_LINES";
        private static final String EXTRA_SHOW_VIDEO_PROMPT = "EXTRA_SHOW_VIDEO_PROMPT";
        private static final String EXTRA_SHOW_PICTURE_PROMPT = "EXTRA_SHOW_PICTURE_PROMPT";
        private static final String EXTRA_HAS_SHOT_MAGAZINE = "EXTRA_HAS_SHOT_MAGAZINE";
        private static final String EXTRA_DECLINE_SELECT_MAGAZINE = "EXTRA_DECLINE_SELECT_MAGAZINE";
        private static final String EXTRA_SHOW_COUNTDOWN_TIMER = "EXTRA_SHOW_COUNTDOWN_TIMER";
        private static final String EXTRA_VIN_TO_SHOW = "EXTRA_VIN_TO_SHOW";
        private static final String EXTRA_REPLACE_MEDIA = "EXTRA_REPLACE_MEDIA";
        private static final String EXTRA_SLAVE_ROLE = "EXTRA_SLAVE_ROLE";

        private final Intent intent;

        public Builder(Intent intent) {
            this.intent = intent;
        }

        Builder setMaxPictures(int maxPictures) {
            intent.putExtra(EXTRA_MAX_PICTURES, maxPictures);
            return this;
        }

        Builder enableManualPictureMode() {
            intent.putExtra(EXTRA_MANUAL_PICTURE_MODE, true);
            return this;
        }

        Builder enableAutomaticPictureMode() {
            intent.putExtra(EXTRA_AUTOMATIC_PICTURE_MODE, true);
            return this;
        }

        Builder enableShutterAnimation() {
            intent.putExtra(EXTRA_SHUTTER_ANIMATION_ENABLED, true);
            return this;
        }

        Builder enableAnimatedGif() {
            intent.putExtra(EXTRA_ANIMATED_GIF_ENABLED, true);
            return this;
        }

        Builder setPictureSize(int width, int height) {
            intent.putExtra(EXTRA_PICTURE_SIZE_WIDTH, width);
            intent.putExtra(EXTRA_PICTURE_SIZE_HEIGHT, height);
            return this;
        }

        Builder setShowGridLines(boolean show) {
            intent.putExtra(EXTRA_SHOW_GRID_LINES, show);
            return this;
        }

        Builder setShowVideoPrompt(boolean show) {
            intent.putExtra(EXTRA_SHOW_VIDEO_PROMPT, show);
            return this;
        }

        Builder setShowPicurePrompt(boolean show) {
            intent.putExtra(EXTRA_SHOW_PICTURE_PROMPT, show);
            return this;
        }

        Builder setHasShotMagazine(boolean hasShotMagazine) {
            intent.putExtra(EXTRA_HAS_SHOT_MAGAZINE, hasShotMagazine);
            return this;
        }


        Builder declineSelectMagazine() {
            intent.putExtra(EXTRA_DECLINE_SELECT_MAGAZINE, true);
            return this;
        }

        Builder setShowCountdownTimer(boolean show) {
            intent.putExtra(EXTRA_SHOW_COUNTDOWN_TIMER, show);
            return this;
        }

        Builder setVinToShow(String vin) {
            intent.putExtra(EXTRA_VIN_TO_SHOW, vin);
            return this;
        }

        Builder setSlaveRole(CameraRole role) {
            intent.putExtra(EXTRA_SLAVE_ROLE, role);
            return this;
        }

        Builder setReplaceMedia(boolean replace) {
            intent.putExtra(EXTRA_REPLACE_MEDIA, replace);
            return this;
        }

        public static Session create(Bundle bundle) {
            Session session = new Session();
            session.maxPictures = bundle.getInt(EXTRA_MAX_PICTURES);
            session.manualPictureMode = bundle.getBoolean(EXTRA_MANUAL_PICTURE_MODE);
            session.automaticPictureMode = bundle.getBoolean(EXTRA_AUTOMATIC_PICTURE_MODE);
            session.shutterAnimationEnabled = bundle.getBoolean(EXTRA_SHUTTER_ANIMATION_ENABLED);
            session.animatedGifEnabled = bundle.getBoolean(EXTRA_ANIMATED_GIF_ENABLED);
            session.showGridLines = bundle.getBoolean(EXTRA_SHOW_GRID_LINES, false);
            session.showVideoPrompt = bundle.getBoolean(EXTRA_SHOW_VIDEO_PROMPT, false);
            session.showPicturePrompt = bundle.getBoolean(EXTRA_SHOW_PICTURE_PROMPT, false);
            session.hasShotMagazine = bundle.getBoolean(EXTRA_HAS_SHOT_MAGAZINE, false);
            session.declinedSelectMagazine = bundle.getBoolean(EXTRA_DECLINE_SELECT_MAGAZINE, false);
            session.showCountdownTimer = bundle.getBoolean(EXTRA_SHOW_COUNTDOWN_TIMER, false);
            session.vinToShow = bundle.getString(EXTRA_VIN_TO_SHOW);
            session.replaceMedia = bundle.getBoolean(EXTRA_REPLACE_MEDIA, false);
            session.slaveRole = (CameraRole)(bundle.getSerializable(EXTRA_SLAVE_ROLE));
            return session;
        }
    }

    private int maxPictures;
    private boolean manualPictureMode;
    private boolean automaticPictureMode;
    private boolean shutterAnimationEnabled;
    private boolean animatedGifEnabled;
    private boolean showGridLines;
    private boolean showVideoPrompt;
    private boolean showPicturePrompt;
    private boolean hasShotMagazine;
    private boolean declinedSelectMagazine;
    private boolean showCountdownTimer;
    private boolean replaceMedia;
    private String vinToShow;
    private CameraRole slaveRole;

    private Session() {
    }

    public int getMaxPictures() {
        return maxPictures;
    }

    public boolean isManualPictureMode() {
        return manualPictureMode;
    }

    public boolean isAutomaticPictureMode() {
        return automaticPictureMode;
    }

    public boolean isShutterAnimationEnabled() {
        return shutterAnimationEnabled;
    }

    public boolean isAnimatedGifEnabled() {
        return animatedGifEnabled;
    }

    public boolean isShowGridLines() {
        return showGridLines;
    }

    public boolean isShowVideoPrompt() {
        return showVideoPrompt;
    }

    public String getVinToShow() {
        return vinToShow;
    }

    public boolean isShowPicturePrompt() {
        return showPicturePrompt;
    }

    public boolean hasShotMagazine() {
        return hasShotMagazine;
    }

    public boolean declinedSelectMagazine() {
        return declinedSelectMagazine;
    }

    public boolean isShowCountdownTimer() {
        return showCountdownTimer;
    }

    public boolean shouldReplaceMedia() {
        return replaceMedia;
    }

    public CameraRole getCameraRole() {
        return slaveRole;
    }
}
