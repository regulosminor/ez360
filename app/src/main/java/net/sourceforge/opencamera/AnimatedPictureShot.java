package net.sourceforge.opencamera;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.SparseArray;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AnimatedPictureShot implements Parcelable {
    public File video;
    int frameCount;
    public ArrayList<File> frames = new ArrayList<>();

    public AnimatedPictureShot() {}

    public AnimatedPictureShot(Parcel in) {
        video = new File(in.readString());

        List<String> pathnames = new ArrayList<String>();
        in.readStringList(pathnames);
        for (String pathname : pathnames) {
            frames.add(new File(pathname));
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(video.getAbsolutePath());

        ArrayList<String> images = new ArrayList<>(frames.size());
        for (File file : frames) {
            images.add(file.getAbsolutePath());
        }
        parcel.writeStringList(images);

    }

    public static final Parcelable.Creator<AnimatedPictureShot> CREATOR
            = new Parcelable.Creator<AnimatedPictureShot>() {
        public AnimatedPictureShot createFromParcel(Parcel in) {
            return new AnimatedPictureShot(in);
        }

        public AnimatedPictureShot[] newArray(int size) {
            return new AnimatedPictureShot[size];
        }
    };

}
