package net.sourceforge.opencamera;


import android.util.SparseArray;

import com.sister.ivana.database.MagazineShot;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

import static net.sourceforge.opencamera.MagazineState.ShotState.CURRENT;
import static net.sourceforge.opencamera.MagazineState.ShotState.NOT_TAKEN;
import static net.sourceforge.opencamera.MagazineState.ShotState.TAKEN;

public class MagazineState {
    enum ShotState {TAKEN, CURRENT, NOT_TAKEN}
    private List<MagazineShot> magazineShots;

    public MagazineState(List<MagazineShot> magazineShots) {
        this.magazineShots = magazineShots;
    }

    public MagazineShot getMagazineShot(int position) {
        return magazineShots.get(position);
    }

    public int getMagazineCount() {
        return magazineShots.size();
    }
}
