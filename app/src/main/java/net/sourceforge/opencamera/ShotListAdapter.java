package net.sourceforge.opencamera;


import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.sister.ivana.FFmpeg;
import com.sister.ivana.R;
import com.sister.ivana.database.MagazineShot;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class ShotListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public interface ShotListListener {
        void poiShotClicked(int index);

        void poiShotLongClicked(int index);

        void addPoiClicked();

        void pipShotClicked(int index);

        void detailShotClicked(int index);

        void animatedPictureClicked(int index);

        void customPoiClicked(int index);
    }

    private final static int MINIMUM_SHOT_COUNT = 6;

    private final Context context;
    private final int shotImageWidth;
    private final int shotImageHeight;
    private final MagazineState magazineState;
    private final ShotCollection shotCollection;
    private final List<MagazineShot> magazineShots;
    private ShotListListener listener;
    private CaptureMode captureMode;
    private int selectedIndex;
    private Handler handler = new Handler();
    private boolean showAll;
    private boolean showAddPoi;

    public ShotListAdapter(Context context, MagazineState magazineState, ShotCollection shotCollection, List<MagazineShot> magazineShots) {
        this.context = context;
        this.magazineState = magazineState;
        this.shotCollection = shotCollection;
        this.magazineShots = magazineShots;
        this.captureMode = CaptureMode.DETAIL;
        shotImageHeight = Math.round(context.getResources().getDimension(R.dimen.shot_list_height));
        shotImageWidth = Math.round(context.getResources().getDimension(R.dimen.shot_list_width));
    }

    public void setListener(ShotListListener listener) {
        this.listener = listener;
    }

    public boolean toggleShowAll() {
        showAll = !showAll;
        notifyDataSetChanged();
        return showAll;
    }

    public void setShowAll(boolean showAll) {
        this.showAll = showAll;
        notifyDataSetChanged();;
    }

    public boolean isShowAllEnabled() {
        return showAll;
    }

    public void enableAddPoi() {
        if (captureMode == CaptureMode.POI) {
            selectedIndex++;
        }
        showAddPoi = true;
    }

    public void setMode(CaptureMode mode) {
        this.captureMode = mode;
        resetSelectedIndex();
        this.notifyDataSetChanged();
    }

    private void resetSelectedIndex() {
        switch (captureMode) {
            case POI:
                selectedIndex = getPoiSelectedIndex();
                break;
            case DETAIL:
                selectedIndex = shotCollection.detailShots.size();
                break;
            case PIP:
                selectedIndex = shotCollection.pipShots.size();
                break;
            case ANIMATED_PICTURE:
                selectedIndex = shotCollection.animatedPictureShots.size();
                break;
        }
    }

    private int getPoiSelectedIndex() {
        for (int i = 0; i < magazineState.getMagazineCount(); i++) {
            if (null == shotCollection.poiShots.get(i)) {
                return isAddPoiAvailable() ? i + 1 : i;
            }
        }

        return -1;
    }

    public int getItemViewType(int position) {
        if (showAll) {
            return R.layout.cell_shot_list_shot;
        }

        if (position == 0 && captureMode == CaptureMode.POI && isAddPoiAvailable()) {
            return R.layout.cell_shot_list_add_poi;
        } else {
            return R.layout.cell_shot_list_shot;
        }
    }

    public boolean isAddPoiAvailable() {
        return captureMode == CaptureMode.POI && showAddPoi;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);

        if (viewType == R.layout.cell_shot_list_add_poi) {
            return new AddPoiHolder(context, view, new AddPoiHolder.AddPoiListener() {
                @Override
                public void addPoiClicked() {
                    if (listener != null) {
                        listener.addPoiClicked();
                    }
                }
            });
        }

        return new ShotListHolder(context, view, new ShotListHolder.ShotListener() {
            @Override
            public void shotClicked(CaptureMode mode, int index) {
                if (listener == null)  {
                    return;
                }

                switch (mode) {
                    case PIP:
                        if (index < shotCollection.pipShots.size()) {
                            listener.pipShotClicked(index);
                        }
                        break;
                    case DETAIL:
                        if (index < shotCollection.detailShots.size()) {
                            listener.detailShotClicked(index);
                        }
                        break;
                    case ANIMATED_PICTURE:
                        listener.animatedPictureClicked(index);
                        break;
                    case POI:
                        int i = isAddPoiAvailable() ? (index - 1) : index;
                        if (i < magazineState.getMagazineCount()) {
                            listener.poiShotClicked(i);
                        } else {
                            listener.customPoiClicked(i - magazineState.getMagazineCount());
                        }
                        break;
                }
            }

            @Override
            public void shotLongClicked(CaptureMode mode, int index) {
                if (listener == null) {
                    return;
                }

                switch (mode) {
                    case PIP:
                        if (index < shotCollection.pipShots.size()) {
                            listener.pipShotClicked(index);
                        }
                        break;
                    case DETAIL:
                        if (index < shotCollection.detailShots.size()) {
                            listener.detailShotClicked(index);
                        }
                        break;
                    case ANIMATED_PICTURE:
                        listener.animatedPictureClicked(index);
                        break;
                    case POI:
                        int i = isAddPoiAvailable() ? (index - 1) : index;
                        if (i < magazineState.getMagazineCount()) {
                            listener.poiShotLongClicked(i);
                        } else {
                            listener.customPoiClicked(i - magazineState.getMagazineCount());
                        }
                        break;
                }
            }
        });
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (!(holder instanceof ShotListHolder)) {
            return;
        }

        if (showAll) {
            bindShowAll((ShotListHolder)holder, position);
            return;
        }

        switch (captureMode) {
            case PIP:
                bindPipShot((ShotListHolder) holder, position);
                break;
            case POI:
                bindPoiShot((ShotListHolder) holder, position);
                break;
            case ANIMATED_PICTURE:
                bindAnimatedPictureShot((ShotListHolder) holder, position);
                break;
            case DETAIL:
                bindDetailShot((ShotListHolder) holder, position);
        }
    }

    private void bindShowAll(ShotListHolder holder, int position) {
        if (position < shotCollection.poiShots.size()) {
            final PoiShot shot = shotCollection.getPoiShot(position);
            holder.bindPoiShot(shot, "", position, shotImageWidth, shotImageHeight);
            holder.setShotState(MagazineState.ShotState.TAKEN);
            return;
        }
        position -= shotCollection.poiShots.size();

        if (position < shotCollection.customPoiShots.size()) {
            CustomPoiShot shot = shotCollection.customPoiShots.get(position);
            holder.bindCustomPoiShot(shot, false, position, shotImageWidth, shotImageHeight);
            holder.setShotState(MagazineState.ShotState.TAKEN);
            return;
        }
        position -= shotCollection.customPoiShots.size();

        if (position < shotCollection.detailShots.size()) {
            final DetailShot shot = position < shotCollection.detailShots.size() ? shotCollection.detailShots.get(position) : null;
            holder.bindDetail(shot, "", position, shotImageWidth, shotImageHeight);
            holder.setShotState(MagazineState.ShotState.TAKEN);
            return;
        }
        position -= shotCollection.detailShots.size();

        if (position < shotCollection.pipShots.size()) {
            final Shot shot = position < shotCollection.pipShots.size() ? shotCollection.pipShots.get(position) : null;
            holder.bindPip(shot, "", position, shotImageWidth, shotImageHeight);
            holder.setShotState(MagazineState.ShotState.TAKEN);
            return;
        }
        position -= shotCollection.pipShots.size();

        ShotCollection.AnimatedPicturePosition animatedPosition = shotCollection.getAnimatedPicturePosition(position);
        if (animatedPosition.frameIndex > -1) {
            final AnimatedPictureShot shot = shotCollection.animatedPictureShots.get(animatedPosition.shotIndex);
            File image = null;
            if (animatedPosition.frameIndex < shot.frames.size()) {
                image = shot.frames.get(animatedPosition.frameIndex);
            }
            holder.bindAnimatedPictureFrame(image, "", position, shotImageWidth, shotImageHeight);
            holder.setShotState(MagazineState.ShotState.TAKEN);
            return;
        } else {
            final AnimatedPictureShot shot = shotCollection.animatedPictureShots.get(animatedPosition.shotIndex);
            holder.bindAnimatedPictureShot(shot, "", position, shotImageWidth, shotImageHeight);
            holder.setShotState(MagazineState.ShotState.TAKEN);
            return;
        }
    }

    @Override
    public int getItemCount() {
        if (showAll) {
            return getShowAllCount();
        }

        int count = 0;

        switch (captureMode) {
            case POI:
                count = magazineState.getMagazineCount() + shotCollection.customPoiShots.size();
                if (isAddPoiAvailable()) {
                    count++;
                }
                break;

            case PIP:
                // Add 1 so there is always an empty shot at the end of the list.
                count = Math.max(shotCollection.pipShots.size() + 1, MINIMUM_SHOT_COUNT);
                break;

            case ANIMATED_PICTURE:

                int items = 0;
                for (AnimatedPictureShot shot : shotCollection.animatedPictureShots) {
                    // We need a spot for each video and for each frame extracted from it
                    if (shot.video != null) {
                        items++;
                    }
                    items += shot.frameCount;
                }

                // Add 1 so there is always an empty shot at the end of the list.
                items += 1;

                count = Math.max(items, MINIMUM_SHOT_COUNT);
                break;

            case DETAIL:
                // Add 1 so there is always an empty shot at the end of the list.
                count = Math.max(shotCollection.detailShots.size() + 1, MINIMUM_SHOT_COUNT);
                break;
        }

        return count;
    }

    private int getShowAllCount() {
        int count = shotCollection.poiShots.size()
                + shotCollection.customPoiShots.size()
                + shotCollection.detailShots.size()
                + shotCollection.pipShots.size()
                + shotCollection.getAnimatedPictureCount();

        return count;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int index) {
        selectedIndex = isAddPoiAvailable() ? index + 1 : index;
    }

    public void addPoiShot(PoiShot poiShot) {
        final int index = isAddPoiAvailable() ? getSelectedIndex() - 1 : getSelectedIndex();
        shotCollection.poiShots.put(index, poiShot);

        // User can skip around when taking POI shots so advance selection to next
        // shot that hasn't been taken, including wrapping around to the beginning.
        for (int current = index; current < magazineState.getMagazineCount(); current++) {
            if (null == shotCollection.poiShots.get(current)) {
                selectedIndex = isAddPoiAvailable() ? current + 1 : current;
                return;
            }
        }

        // If we got to the end, restart from the beginning and see if there are any
        // empty spots
        for (int current = 0; current < magazineState.getMagazineCount(); current++) {
            if (null == shotCollection.poiShots.get(current)) {
                selectedIndex = isAddPoiAvailable() ? current + 1 : current;
                return;
            }
        }

        selectedIndex = -1;
    }

    public void addCustomPoiShot(CustomPoiShot shot) {
        shotCollection.customPoiShots.add(shot);
        // This doesn't affect selected index because custom POI shots can't be selected.
    }

    public void addPipShot(PipShot pipShot) {
        shotCollection.pipShots.add(pipShot);
        selectedIndex++;
    }

    public void addDetailShot(DetailShot detailShot) {
        shotCollection.detailShots.add(detailShot);
        selectedIndex++;
    }

    public void addAnimatedPictureShot(final AnimatedPictureShot shot, final String vin) {
        shotCollection.animatedPictureShots.add(shot);
        selectedIndex += (1 + shot.frameCount);

        // Extract frames. It shouldn't take long since animated pictures are small so we do it
        // here instead of sending it off to media processing service.
        final File video = shot.video;
        Thread thread = new Thread() {
            @Override
            public void run() {
                final ArrayList<ImageDescription> pictures = FFmpeg.getFFmpeg().extractFrames(context, shot.video.getAbsolutePath(), vin, shot.frameCount, false);

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        for (ImageDescription picture : pictures) {
                            shot.frames.add(picture.image);
                        }

                        if (shot.video == null) {
                            // User deleted video shot while extraction was in progress so delete file now.
                            if (video.delete()) {
                                Timber.i("Deleted %s", video.getAbsoluteFile());
                            } else {
                                Timber.e("Failed deleting %s", video.getAbsoluteFile());
                            }
                        }

                        notifyDataSetChanged();
                    }
                });
            }
        };
        thread.start();
    }

    public void removePoiShot(int index) {
        shotCollection.poiShots.remove(index);
        // Set selected frame to newly deleted spot
        if (isAddPoiAvailable()) {
            selectedIndex = index + 1;
        } else {
            selectedIndex = index;
        }

        notifyDataSetChanged();
    }

    public void removePipShot(int index) {
        shotCollection.pipShots.remove(index);
        selectedIndex--;
        notifyDataSetChanged();
    }

    public void removeDetailShot(int index) {
        shotCollection.detailShots.remove(index);
        selectedIndex--;
        notifyDataSetChanged();
    }

    public void removeAnimatedPictureShot(int index) {
        ShotCollection.AnimatedPicturePosition position = shotCollection.getAnimatedPicturePosition(index);
        AnimatedPictureShot shot = shotCollection.animatedPictureShots.get(position.shotIndex);
        if (position.frameIndex > -1) {
            shot.frameCount--;
            shot.frames.remove(position.frameIndex);
            selectedIndex--;
        } else {
            if (shot.frameCount == 0 || !shot.frames.isEmpty()) {
                // Extraction is complete so safe to delete source file
                if (shot.video.delete()) {
                    Timber.i("Successfully deleted %s", shot.video.getAbsoluteFile());
                } else {
                    Timber.e("Failed deleting %s", shot.video.getAbsoluteFile());
                }
            }
            shot.video = null;
            selectedIndex--;
        }
        notifyDataSetChanged();
    }

    private void bindPipShot(ShotListHolder holder, int position) {
        final String title = String.valueOf(position + 1);
        final Shot shot = position < shotCollection.pipShots.size() ? shotCollection.pipShots.get(position) : null;
        holder.bindPip(shot, title, position, shotImageWidth, shotImageHeight);

        MagazineState.ShotState state;
        if (position < shotCollection.pipShots.size()) {
            state = MagazineState.ShotState.TAKEN;
        } else if (position == shotCollection.pipShots.size()) {
            state = MagazineState.ShotState.CURRENT;
        } else {
            state = MagazineState.ShotState.NOT_TAKEN;
        }
        holder.setShotState(state);
    }

    private void bindPoiShot(ShotListHolder holder, int position) {
        final boolean showAddPoiButton = captureMode == CaptureMode.POI && isAddPoiAvailable();
        final int magazineShotCount = magazineState.getMagazineCount();

        if (showAddPoiButton && position == 0) {
            holder.setShotState(MagazineState.ShotState.NOT_TAKEN);
            return;
        }

        // Adjust for Add POI button
        int index = showAddPoiButton ? position - 1 : position;
        if (index < magazineShotCount) {
            MagazineShot magazineShot = magazineState.getMagazineShot(index);
            final PoiShot shot = shotCollection.poiShots.get(index);
            if (shot != null) {
                holder.bindPoiShot(shot, magazineShot.getTitle(), position, shotImageWidth, shotImageHeight);

            } else {
                holder.bindMagazinePlaceholder(magazineShot, position, shotImageWidth, shotImageHeight);
            }

            MagazineState.ShotState state;
            if (position == selectedIndex) {
                state = MagazineState.ShotState.CURRENT;
            } else if (null != shotCollection.poiShots.get(index)) {
                state = MagazineState.ShotState.TAKEN;
            } else {
                state = MagazineState.ShotState.NOT_TAKEN;
            }
            holder.setShotState(state);
        } else {
            CustomPoiShot shot = shotCollection.customPoiShots.get(index - magazineShotCount);
            holder.bindCustomPoiShot(shot, true, position, shotImageWidth, shotImageHeight);
            holder.setShotState(MagazineState.ShotState.TAKEN);
        }
    }

    private void bindDetailShot(ShotListHolder holder, int position) {
        final String title = String.valueOf(position + 1);
        final DetailShot shot = position < shotCollection.detailShots.size() ? shotCollection.detailShots.get(position) : null;
        holder.bindDetail(shot, title, position, shotImageWidth, shotImageHeight);

        MagazineState.ShotState state;
        if (position < shotCollection.detailShots.size()) {
            state = MagazineState.ShotState.TAKEN;
        } else if (position == shotCollection.detailShots.size()) {
            state = MagazineState.ShotState.CURRENT;
        } else {
            state = MagazineState.ShotState.NOT_TAKEN;
        }
        holder.setShotState(state);

    }

    private void bindAnimatedPictureShot(ShotListHolder holder, int index) {
        // This could be either the video itself or one of the frames extracted from it.
        ShotCollection.AnimatedPicturePosition position = shotCollection.getAnimatedPicturePosition(index);
        String title = String.valueOf(index + 1);
        if (position == null) {
            holder.bindAnimatedPictureShot(null, title, index, shotImageWidth, shotImageHeight);
        } else if (position.frameIndex > -1) {
            final AnimatedPictureShot shot = shotCollection.animatedPictureShots.get(position.shotIndex);
            File image;
            if (position.frameIndex < shot.frames.size()) {
                image = shot.frames.get(position.frameIndex);
            } else {
                image = null;
                title += " - Extracting";
            }
            holder.bindAnimatedPictureFrame(image, title, index, shotImageWidth, shotImageHeight);
        } else {
            final AnimatedPictureShot shot = shotCollection.animatedPictureShots.get(position.shotIndex);
            holder.bindAnimatedPictureShot(shot, title + " - Video", index, shotImageWidth, shotImageHeight);
        }

        int items = 0;
        for (AnimatedPictureShot s : shotCollection.animatedPictureShots) {
            // We need a spot for each video and for each frame extracted from it
            if (s.video != null) {
                items++;
            }
            items += s.frameCount;
        }

        MagazineState.ShotState state;
        if (index < items) {
            state = MagazineState.ShotState.TAKEN;
        } else if (index == items) {
            state = MagazineState.ShotState.CURRENT;
        } else {
            state = MagazineState.ShotState.NOT_TAKEN;
        }
        holder.setShotState(state);
    }
}
