package net.sourceforge.opencamera;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.sister.ivana.R;

public class ModeControlView extends LinearLayout {
    public interface ModeControlListener {
        public void onModeSelected(CaptureMode mode);
    }

    private static final String TITLE_POIS = "You are capturing Points Of Interest";
    private static final String TITLE_PIPS = "You are capturing Picture in Picture";
    private static final String TITLE_ANIMATED_PICTURE = "You are capturing Animated Picture";
    private static final String TITLE_PICTURES_ONLY = "You are capturing pictures only";

    private View view;
    private Context context;
    private CaptureMode mode;

    private LinearLayout modeSelectorContainer;
    private TextView titleTextView;
    private ToggleButton modePoiButton;
    private ToggleButton modePipButton;
    private ToggleButton modeLongClickButton;
    private CheckBox poiCheckBox;
    private Session session;
    private RecyclerView shotListRecyclerView;
    private ImageButton shutterButton;
    private ModeControlListener listener;
    private RadioGroup radioGroup;
    private boolean isProcessingCheckbox;

    private LayoutInflater inflater;

    public ModeControlView(Context context) {
        super(context);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        init();
    }

    public ModeControlView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        init();
    }

    public ModeControlView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        init();
    }

    public void init() {
        view = inflater.inflate(R.layout.view_mode_control, this);

        modeSelectorContainer = view.findViewById(R.id.mode_selector);
        titleTextView = view.findViewById(R.id.title_text_view);

        modePoiButton = view.findViewById(R.id.mode_poi_button);
        modePoiButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onToggle(view);
            }
        });

        modePipButton = view.findViewById(R.id.mode_pip_button);
        modePipButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onToggle(view);
            }
        });

        modeLongClickButton = view.findViewById(R.id.mode_longclick_button);
        modeLongClickButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onToggle(view);
            }
        });

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                dimModeSelector();
            }
        });

        radioGroup = findViewById(R.id.toggle_group);
        radioGroup.setOnCheckedChangeListener(ToggleListener);
    }

    public void setListener(ModeControlListener listener) {
        this.listener = listener;
    }

    public void setDependancies(Session session, ImageButton shutterButton, CheckBox poiCheckBox, RecyclerView shotListRecyclerView) {
        this.session = session;
        this.shutterButton = shutterButton;
        this.poiCheckBox = poiCheckBox;
        this.shotListRecyclerView = shotListRecyclerView;

        CaptureMode mode;
        if (session.hasShotMagazine()) {
            mode = CaptureMode.POI;
            radioGroup.check(modePoiButton.getId());
        } else {
            mode = CaptureMode.DETAIL;
            this.modePoiButton.setBackgroundColor(getResources().getColor(R.color.disabled_gray));
            this.modePoiButton.setEnabled(false);
        }

        setMode(mode);
        listener.onModeSelected(mode);
    }

    private void updateCheckboxWithNoProcessing(boolean isChecked) {
        isProcessingCheckbox = true;
        poiCheckBox.setChecked(isChecked);
        isProcessingCheckbox = false;
    }

    public void setMode(CaptureMode mode) {
        if (isProcessingCheckbox) {
            return;
        }

        this.mode = mode;
        switch (mode) {
            case POI:
                poiCheckBox.setBackgroundColor(getResources().getColor(R.color.mode_selector_blue));
                shutterButton.setBackgroundColor(getResources().getColor(R.color.mode_selector_blue));
                titleTextView.setText(TITLE_POIS);
                poiCheckBox.setText("POIs");
                modePoiButton.setChecked(true);
                updateCheckboxWithNoProcessing(true);
                break;
            case PIP:
                poiCheckBox.setBackgroundColor(getResources().getColor(R.color.mode_selector_red));
                shutterButton.setBackgroundColor(getResources().getColor(R.color.mode_selector_red));
                titleTextView.setText(TITLE_PIPS);
                poiCheckBox.setText("PIPs");
                modePipButton.setChecked(true);
                updateCheckboxWithNoProcessing(true);
                break;
            case ANIMATED_PICTURE:
                poiCheckBox.setBackgroundColor(getResources().getColor(R.color.mode_selector_yellow));
                shutterButton.setBackgroundColor(getResources().getColor(R.color.mode_selector_yellow));
                titleTextView.setText(TITLE_ANIMATED_PICTURE);
                poiCheckBox.setText("ANIM");
                modeLongClickButton.setChecked(true);
                updateCheckboxWithNoProcessing(true);
                break;
            case DETAIL:
                poiCheckBox.setBackgroundColor(getResources().getColor(R.color.mode_selector_none));
                updateCheckboxWithNoProcessing(false);
                shutterButton.setBackgroundColor(getResources().getColor(R.color.mode_selector_none));
                titleTextView.setText(TITLE_PICTURES_ONLY);

                for (int j = 0; j < radioGroup.getChildCount(); j++) {
                    final ToggleButton view = (ToggleButton) radioGroup.getChildAt(j);
                    view.setChecked(false);
                }

                break;
        }
    }

    public CaptureMode getMode() {
        return this.mode;
    }

    static final RadioGroup.OnCheckedChangeListener ToggleListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(final RadioGroup radioGroup, final int i) {
            for (int j = 0; j < radioGroup.getChildCount(); j++) {
                final ToggleButton view = (ToggleButton) radioGroup.getChildAt(j);
                view.setChecked(view.getId() == i);
            }
        }
    };

    public void onToggle(View view) {
        RadioGroup radioGroup = (RadioGroup) view.getParent();
        radioGroup.check(view.getId());

        ToggleButton toggleButton = (ToggleButton) view;
        CaptureMode mode = null;
        if (!toggleButton.isChecked()) {
            mode = CaptureMode.DETAIL;
        } else {
            switch (view.getId()) {
                case R.id.mode_poi_button:
                    mode = CaptureMode.POI;
                    break;

                case R.id.mode_pip_button:
                    mode = CaptureMode.PIP;
                    break;

                case R.id.mode_longclick_button:
                    mode = CaptureMode.ANIMATED_PICTURE;
                    break;
            }
        }

        swapShotList(mode);
    }

    public void dimModeSelector() {
        AlphaAnimation animation1 = new AlphaAnimation(1.0f, 0.2f);
        animation1.setDuration(1000);
        animation1.setStartOffset(1500);
        animation1.setFillAfter(true);
        modeSelectorContainer.startAnimation(animation1);
        shotListRecyclerView.startAnimation(animation1);
    }

    public void swapShotList(final CaptureMode mode) {
        if (isProcessingCheckbox) {
            return;
        }

        dimModeSelector();

        ObjectAnimator mover = ObjectAnimator.ofFloat(shotListRecyclerView, "translationX", 0, -1 * shotListRecyclerView.getWidth());
        mover.setDuration(300);
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(mover);
        animatorSet.start();
        mover.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (listener != null && mode != null) {
                    if (ModeControlView.this.mode != mode) {
                        setMode(mode);
                        listener.onModeSelected(mode);
                    }
                }

                ObjectAnimator fadeOut = ObjectAnimator.ofFloat(shotListRecyclerView, "translationX", shotListRecyclerView.getWidth());
                fadeOut.setDuration(1);
                ObjectAnimator mover = ObjectAnimator.ofFloat(shotListRecyclerView, "translationX", -1 * shotListRecyclerView.getWidth(), 0f);
                mover.setDuration(300);
                AnimatorSet animatorSet = new AnimatorSet();
                animatorSet.play(mover).after(fadeOut);
                animatorSet.start();
            }
        });

    }

}
