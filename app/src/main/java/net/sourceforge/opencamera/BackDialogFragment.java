package net.sourceforge.opencamera;


import android.app.Dialog;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import com.sister.ivana.R;
import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class BackDialogFragment extends AppDialogFragment {
    public interface BackListener {
        void onBackUploadContent();

        void onBackCancel();
    }

    private Unbinder unbinder;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Dialog dialog = new Dialog(getActivity(), R.style.AppDialogTheme);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_camera_cancel, null);
        dialog.setContentView(view);
        unbinder = ButterKnife.bind(this, dialog);
        return dialog;
    }

    @Override
    public void onResume() {
        super.onResume();

        // The cancel button has a lot of text and it doesn't lay out nicely using the default
        // behavior so make it wider
        final Rect frame = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        final int width = (int)(frame.width() * 0.8);
        getDialog().getWindow().setLayout(width, WRAP_CONTENT);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }

    @OnClick(R.id.positive_button)
    public void onPositiveClick() {
        // OK means the user wants to cancel the upload
        final BackListener listener = (BackListener) getActivity();
        listener.onBackCancel();
    }

    @OnClick(R.id.negative_button)
    public void onNegativeClick() {
        final BackListener listener = (BackListener) getActivity();
        listener.onBackUploadContent();
    }
}
