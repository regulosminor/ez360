package net.sourceforge.opencamera;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;

public class PipShot implements Shot, Parcelable {
    public String wideImagePathname;
    public String detailImagePathname;
    public float positionX;
    public float positionY;
    boolean waitingForDetailPhoto;

    public PipShot() {}

    public PipShot(Parcel in) {
        wideImagePathname = in.readString();
        detailImagePathname = in.readString();
        positionX = in.readFloat();
        positionY = in.readFloat();
    }

    @Override
    public File getShotListThumbnail() {
        return new File(detailImagePathname);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(wideImagePathname);
        parcel.writeString(detailImagePathname);
        parcel.writeFloat(positionX);
        parcel.writeFloat(positionY);
    }

    public static final Parcelable.Creator<PipShot> CREATOR
            = new Parcelable.Creator<PipShot>() {
        public PipShot createFromParcel(Parcel in) {
            return new PipShot(in);
        }

        public PipShot[] newArray(int size) {
            return new PipShot[size];
        }
    };
}
