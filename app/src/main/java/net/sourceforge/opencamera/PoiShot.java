package net.sourceforge.opencamera;

import java.io.File;

class PoiShot {
    File file;
    CaptureMode type;

    PoiShot(File file, CaptureMode type) {
        this.file = file;
        this.type = type;
    }
}
