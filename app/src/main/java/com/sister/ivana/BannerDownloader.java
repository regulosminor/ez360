package com.sister.ivana;


import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;

import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledFuture;

import timber.log.Timber;

import static android.content.Context.DOWNLOAD_SERVICE;

public class BannerDownloader {
    public interface BannerDownloaderListener {
        void bannerDownloadFailed();

        void bannerDownloadComplete();
    }

    private static final int STATUS_POLL_PERIOD_MS = 5000;
    private final Context context;
    private final DownloadManager downloadManager;
    private BannerDownloaderListener listener;
    private final Handler mainHandler;
    private final Set<Long> requestIDs = new HashSet<>();
    private ScheduledFuture<?> statusTask;
    private final HashMap<String, String> urls;

    final private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
            downloadComplete(id);
        }
    };

    public BannerDownloader(Context context, Map<String, String> urlMap, BannerDownloaderListener listener) {
        this.mainHandler = new Handler(context.getMainLooper());
        this.context = context;
        this.listener = listener;
        urls = new HashMap<>(urlMap);
        downloadManager = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
        context.registerReceiver(downloadReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public void start() {
        deleteImages();

        for (Map.Entry<String, String> entry : urls.entrySet()) {
            final String project = entry.getKey();
            final String url = entry.getValue();
            final File bannerFile = FileUtil.getBannerFile(project);

            // Invalidate cache since we save into same filename
            Picasso.get().invalidate(bannerFile);

            final DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url))
                    .setDestinationUri(Uri.fromFile(bannerFile))
                    .setAllowedOverMetered(false)
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);

            final long id = downloadManager.enqueue(request);
            Timber.d("starting banner download id: %d project: %s url: %s", id, project, url);
            requestIDs.add(id);
        }

        scheduleStatusCheck(false);
    }

    public void stop() {
        context.unregisterReceiver(downloadReceiver);
        cancelRequests();
    }


    private void scheduleStatusCheck(boolean immediate) {
        if (immediate) {
            checkStatus();
        } else {
            mainHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    checkStatus();
                }
            }, STATUS_POLL_PERIOD_MS);
        }
        if (statusTask != null) {
            statusTask.cancel(false);
            statusTask = null;
        }
    }

    private static String getStatusString(Integer status) {
        if (status == null) {
            return "unknown";
        }

        String statusString = "";
        switch (status) {
            case DownloadManager.STATUS_FAILED:
                statusString = "STATUS_FAILED";
                break;
            case DownloadManager.STATUS_PAUSED:
                statusString = "STATUS_PAUSED";
                break;
            case DownloadManager.STATUS_PENDING:
                statusString = "STATUS_PENDING";
                break;
            case DownloadManager.STATUS_RUNNING:
                statusString = "STATUS_RUNNING";
                break;
            case DownloadManager.STATUS_SUCCESSFUL:
                statusString = "STATUS_SUCCESSFUL";
                break;
            default:
                statusString = Integer.toString(status);
        }

        return statusString;
    }

    /*
     * We don't get a broadcast from the download service if it is not downloading, for example if
     * WiFi is not available. Periodically we poll to see if it looks like it is still running.
     */
    private void checkStatus() {
        if (requestIDs.isEmpty()) {
            if (listener != null) {
                listener.bannerDownloadComplete();
                listener = null;
            }
        }

        for (long id : requestIDs) {
            Integer status = getRequestStatus(id);
            Timber.d("banner status id: %d status: %s", id, getStatusString(status));
            if (status == null || status == DownloadManager.STATUS_PAUSED || status == DownloadManager.STATUS_FAILED) {
                cancelRequests();
                if (listener != null) {
                    listener.bannerDownloadFailed();
                    listener = null;
                }
                return;
            }
        }
    }

    private void downloadComplete(long requestID) {
        Timber.d("banner download complete id: %d", requestID);

        Integer status = getRequestStatus(requestID);
        if (status != null && status == DownloadManager.STATUS_SUCCESSFUL) {
            requestIDs.remove(requestID);
        }

        scheduleStatusCheck(true);
    }

    private Integer getRequestStatus(long id) {
        Integer status = null;
        Cursor cursor = null;
        try {
            final DownloadManager.Query query = new DownloadManager.Query().setFilterById(id);
            cursor = downloadManager.query(query);
            if (cursor.moveToNext()) {
                status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
            }

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return status;
    }

    private void cancelRequests() {
        for (Long id : requestIDs) {
            downloadManager.remove(id);
        }
        requestIDs.clear();
    }

    private void deleteImages() {
        File[] files = FileUtil.getBannersDirectory().listFiles();
        if (files != null) {
            for (File file : files) {
                deleteFile(file);
            }
        }
    }

    private void deleteFile(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f : files) {
                deleteFile(f);
            }
            if (!file.delete()) {
                Util.logError("Failed deleting directory " + file.getAbsolutePath());
            }
        } else {
            if (!file.delete()) {
                Util.logError("Failed deleting file " + file.getAbsolutePath());
            }
        }
    }
}
