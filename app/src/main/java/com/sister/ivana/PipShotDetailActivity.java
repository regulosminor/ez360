package com.sister.ivana;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PipShotDetailActivity extends AppCompatActivity implements DeletePhotoDialogFragment.DeletePhotoListener {
    public static final int RESULT_DELETE = 101;

    public static final String EXTRA_SHOT_INDEX = "EXTRA_SHOT_INDEX";

    private final float aspectRatio = 4f / 3f;
    private File image;
    private File wideImage;
    private int index;
    private ViewTreeObserver.OnGlobalLayoutListener layoutListener;

    private static final String EXTRA_IMAGE_PATHNAME = "EXTRA_IMAGE_PATHNAME";
    private static final String EXTRA_WIDE_IMAGE_PATHNAME = "EXTRA_WIDE_IMAGE_PATHNAME";
    private static final String EXTRA_INDEX = "EXTRA_INDEX";

    @BindView(R.id.top_layout) RelativeLayout topLayout;
    @BindView(R.id.image_view) ImageView imageView;
    @BindView(R.id.insetImageView) ImageView wideImageView;
    @BindView(R.id.button_close) ImageView closeButton;
    @BindView(R.id.button_delete) ImageView deleteButton;
    @BindView(R.id.bottom_label) TextView bottomLabel;

    public static Intent createIntent(Context context, File image, File wideImage, int index) {
        Intent intent = new Intent(context, PipShotDetailActivity.class);
        intent.putExtra(EXTRA_IMAGE_PATHNAME, image.getAbsolutePath());
        intent.putExtra(EXTRA_WIDE_IMAGE_PATHNAME, wideImage.getAbsolutePath());
        intent.putExtra(EXTRA_INDEX, index);

        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pip_shot_detail);
        ButterKnife.bind(this);

        image = new File(getIntent().getStringExtra(EXTRA_IMAGE_PATHNAME));
        wideImage = new File(getIntent().getStringExtra(EXTRA_WIDE_IMAGE_PATHNAME));
        index = getIntent().getIntExtra(EXTRA_INDEX, -1);

        layoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)wideImageView.getLayoutParams();
                params.width = (int)(imageView.getMeasuredWidth() * 0.32);
                params.height = (int)(imageView.getMeasuredHeight() * 0.32);

                PipShotDetailActivity.this.loadImages();
            }
        };

        topLayout.getViewTreeObserver().addOnGlobalLayoutListener(layoutListener);
    }

    @Override
    protected void onDestroy() {
        if (topLayout != null && topLayout.getViewTreeObserver().isAlive() && layoutListener != null) {
            topLayout.getViewTreeObserver().removeOnGlobalLayoutListener(layoutListener);
        }

        super.onDestroy();
    }

    private void loadImages() {
        Picasso.get()
                .load(image)
                .fit()
                .centerCrop()
                .into(imageView);

        Picasso.get()
                .load(wideImage)
                .fit()
                .centerCrop()
                .into(wideImageView);
    }

    @Override
    protected void onStart() {
        super.onStart();

        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int height = (int) (metrics.heightPixels * 0.80);
        int width = Math.round(aspectRatio * (float) metrics.heightPixels);

        getWindow().setLayout(width, height);
    }

    @OnClick(R.id.button_close)
    void onCloseClick() {
        finish();
    }

    @OnClick(R.id.button_delete)
    void onDeleteClick() {
        final DeletePhotoDialogFragment fragment = DeletePhotoDialogFragment.newInstance();
        fragment.show(getFragmentManager(), "delete_photo");
    }

    @Override
    public void onDeletePhotoPositiveClick() {
        Intent intent = new Intent();
        intent.putExtra(EXTRA_SHOT_INDEX, index);
        setResult(RESULT_DELETE, intent);
        finish();
    }

    @Override
    public void onDeletePhotoNegativeClick() {
    }
}
