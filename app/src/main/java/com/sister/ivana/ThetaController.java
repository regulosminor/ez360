package com.sister.ivana;


import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.PowerManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;

import com.sister.ivana.dialog.AppDialogFragment;
import com.sister.ivana.theta.ThetaConnectionInfoDialog;
import com.sister.ivana.theta.ThetaDownloadingImageDialogFragment;
import com.sister.ivana.theta.ThetaReadyToShootDialog;
import com.sister.ivana.webapi.theta.ThetaResponse;
import com.sister.ivana.webapi.theta.ThetaStateResponse;
import com.sister.ivana.webapi.theta.ThetaUpdatesResponse;
import com.sister.ivana.webapi.theta.ThetaWebService;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class ThetaController {
    private boolean activityActive = true;
    private boolean hdrSupported;

    private static final String DIALOG_DOWNLOADING_TAG = "ThetaController.DIALOG_DOWNLOADING_TAG";
    private static final String DIALOG_CONNECTING_TAG = "ThetaController.DIALOG_CONNECTING_TAG";
    private static final String SESSION_WAKE_LOCK_TAG = "ThetaController:WAKE_LOCK_TAG";
    private final AppCompatActivity activity;
    private final String vin;
    private final ThetaControllerListener listener;
    private String currentFingerprint;
    private ConnectionTask connectionTask;
    private PowerManager.WakeLock sessionWakeLock;
    private String sessionID;
    private final Context context;

    private String cameraSSID;
    private List<String> listCameraSSID;

    public ThetaController(AppCompatActivity activity, String vin, List<String> listCameraSSID, ThetaControllerListener listener) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.vin = vin;
        this.listener = listener;
        this.listCameraSSID = listCameraSSID;

        this.listCameraSSID = (ArrayList<String>) new ArrayList(listCameraSSID);
    }

    public void start() {
        if (listCameraSSID == null || listCameraSSID.size() == 0) {
            final DialogFragment dialog = ThetaNotFoundDialogFragment.newInstance();
            dialog.show(activity.getFragmentManager(), "theta_not_found");
            Timber.e("Theta camera not found");
            return;
        } else if (listCameraSSID.size() == 1) {
            cameraSSID = listCameraSSID.get(0);
            onConnectionInfoPositiveClick(cameraSSID);
        } else {
            Timber.i("showing wifi selection dialog");
            ThetaConnectionInfoDialog dialog = ThetaConnectionInfoDialog.newInstance(listCameraSSID);
            dialog.show(activity.getFragmentManager(), "theta_info");
        }
    }

    public void onConnectionInfoPositiveClick(String cameraSSID) {
        this.cameraSSID = cameraSSID;
        if (!cameraSSID.startsWith("THETA") || !cameraSSID.endsWith(".OSC")) {
            MissingThetaSSIDDialogFragment dialog = MissingThetaSSIDDialogFragment.newInstance();
            dialog.show(activity.getFragmentManager(), "no_ssid");
            Timber.e("unrecognized ssid: %s", cameraSSID);
            return;
        }

        connectionTask = new ConnectionTask(activity);
        connectionTask.execute();
    }

    public void onConnectingDialogCancel() {
        Timber.i("onConnectingDialogCancel");
        if (connectionTask != null) {
            connectionTask.cancel(false);
        }
    }

    private void onSessionError() {
        if (activityActive) {
            AppDialogFragment.show(activity, R.string.dialog_theta_session_error_title, R.string.dialog_theta_session_error_message);
            Timber.e("Error getting Theta image");
        }
        sessionComplete();
    }

    public void onSaveInstanceState() {
        DialogFragment dialog = (DialogFragment) activity.getFragmentManager().findFragmentByTag(DIALOG_DOWNLOADING_TAG);
        if (dialog != null) {
            activity.getFragmentManager().beginTransaction().remove(dialog).commit();
        }
        activityActive = false;
    }

    private void sessionComplete() {
        new Network(activity).connectToLastSSID();

        DialogFragment dialog = (DialogFragment) activity.getFragmentManager().findFragmentByTag(DIALOG_DOWNLOADING_TAG);
        if (dialog != null) {
            dialog.dismiss();
        }

        if (sessionWakeLock != null) {
            sessionWakeLock.release();
            sessionWakeLock = null;
        }
    }

    private void onCameraConnected() {
        ThetaReadyToShootDialog dialog = ThetaReadyToShootDialog.newInstance(cameraSSID);
        dialog.show(activity.getFragmentManager(), "theta_ready");
    }

    public void onReadyToShootPositiveClick() {
        // Keep a wake lock while we are communicating with the camera so the user doesn't have
        // to keep tapping to keep the screen awake and so that we are more likely to be able
        // to connect to the original SSID when we are done.
        PowerManager powerManager = (PowerManager) activity.getSystemService(Context.POWER_SERVICE);
        sessionWakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, SESSION_WAKE_LOCK_TAG);
        sessionWakeLock.acquire();

        getCameraInfo();
    }

    public void onReadyToShootCancel() {
        new Network(activity).connectToLastSSID();
    }

    /**
     * Get information about the camera that is useful for troubleshooting such as firmware
     * and supported API levels.
     */
    private void getCameraInfo() {
        Intent intent = new Intent(context, Theta360Activity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

//        Call<ResponseBody> call = ThetaWebService.getApi(context).getInfo();
//        call.enqueue(new Callback<ResponseBody>() {
//            @Override
//            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
//                if (response.code() < 200 || response.code() > 299) {
//                    Util.logError("Failed getting camera info. HTTP status: " + response.code());
//                    onSessionError();
//                    return;
//                }
//
//                // We don't do anything with the result here but it is recorded in the log file.
//                startSession();
//            }
//
//            @Override
//            public void onFailure(Call<ResponseBody> call, Throwable t) {
//                Util.logError("Failed getting camera info", t);
//                onSessionError();
//            }
//        });
    }

    private void startSession() {
        Call<ThetaResponse> call = ThetaWebService.startSession(context);
        call.enqueue(new Callback<ThetaResponse>() {
            @Override
            public void onResponse(Call<ThetaResponse> call, Response<ThetaResponse> response) {
                if (response.code() < 200 || response.code() > 299) {
                    Util.logError("Failed starting camera session. HTTP status: " + response.code());
                    onSessionError();
                    return;
                }

                final ThetaResponse thetaResponse = response.body();
                if (thetaResponse == null) {
                    Util.logError(("Failed starting camera session: null response"));
                    onSessionError();
                    return;
                }

                if (thetaResponse.getExecutionStatus() != ThetaResponse.ExecutionStatus.DONE) {
                    Util.logError(("Failed starting camera session. Unexpected state: " + thetaResponse.state));
                    onSessionError();
                    return;
                }

                if (thetaResponse.results == null || TextUtils.isEmpty(thetaResponse.results.sessionId)) {
                    Util.logError("Failed starting camera session. No session ID");
                    onSessionError();
                    return;
                }

                sessionID = thetaResponse.results.sessionId;
                setHDR();
            }

            @Override
            public void onFailure(Call<ThetaResponse> call, Throwable t) {
                Util.logError("Failed starting camera session", t);
                onSessionError();
            }
        });
    }

    private void setHDR() {
        Call<ThetaResponse> call = ThetaWebService.setHDR(context, sessionID);
        call.enqueue(new Callback<ThetaResponse>() {
            @Override
            public void onResponse(Call<ThetaResponse> call, Response<ThetaResponse> response) {
                // Indicates whether the camera supports HDR. Older versions of the firmware do not.
                hdrSupported = false;
                if (response.code() < 200 || response.code() > 299) {
                    Util.logError("Failed setting filter to HDR. HTTP status: " + response.code());
                } else {
                    final ThetaResponse thetaResponse = response.body();
                    if (thetaResponse == null) {
                        Util.logError(("Failed setting filter to HDR: null response"));
                    } else if (thetaResponse.getExecutionStatus() != ThetaResponse.ExecutionStatus.DONE) {
                        Util.logError(("Failed setting filter to HDR. Unexpected state: " + thetaResponse.state));
                    } else {
                        // We should get a 200 and correctly formed response if the camera supports HDR.
                        hdrSupported = true;
                    }
                }

                // We keep going even if we fail to set HDR since cameras with an older version
                // of firmware don't support it.
                getState(true);
            }

            @Override
            public void onFailure(Call<ThetaResponse> call, Throwable t) {
                Util.logError("Failed setting filter to HDR", t);
                getState(true);
            }
        });
    }

    private void takePicture() {
        Call<ThetaResponse> call = ThetaWebService.takePicture(context, sessionID);
        call.enqueue(new Callback<ThetaResponse>() {
            @Override
            public void onResponse(Call<ThetaResponse> call, Response<ThetaResponse> response) {
                if (response.code() < 200 || response.code() > 299) {
                    Util.logError("Failed taking picture. HTTP status: " + response.code());
                    onSessionError();
                    return;
                }

                final ThetaResponse thetaResponse = response.body();
                if (thetaResponse == null) {
                    Util.logError(("Failed taking picture: null response"));
                    onSessionError();
                    return;
                }

                final ThetaResponse.ExecutionStatus status = thetaResponse.getExecutionStatus();
                if (status != ThetaResponse.ExecutionStatus.IN_PROGRESS && status != ThetaResponse.ExecutionStatus.DONE) {
                    Util.logError(("Failed taking picture. Unexpected status: " + status));
                    onSessionError();
                    return;
                }

                // It should only take a second to take the picture, but then it takes several seconds
                // for it to become ready for download and then another several seconds for us
                // to download it, so display a dialog with a progress bar.
                ThetaDownloadingImageDialogFragment fragment = ThetaDownloadingImageDialogFragment.newInstance();
                fragment.show(activity.getFragmentManager(), DIALOG_DOWNLOADING_TAG);

                // Start waiting for photo
                getUpdates();
            }

            @Override
            public void onFailure(Call<ThetaResponse> call, Throwable t) {
                Util.logError("Failed taking picture", t);
                onSessionError();
            }
        });
    }

    private void deleteFile(final String thetaUri, final String localPathname) {
        Call<ThetaResponse> call = ThetaWebService.delete(context, thetaUri);
        call.enqueue(new Callback<ThetaResponse>() {
            @Override
            public void onResponse(Call<ThetaResponse> call, Response<ThetaResponse> response) {
                // If we get this far, log any errors deleting the Theta image but consider the
                // overall operation a success.
                if (response.code() < 200 || response.code() > 299) {
                    Util.logError("Failed deleting Theta image. HTTP status: " + response.code() + " URI: " + thetaUri);
                } else {
                    final ThetaResponse thetaResponse = response.body();
                    if (thetaResponse == null) {
                        Util.logError(("Failed deleting Theta image: null response URI: " + thetaUri));
                    } else {
                        final ThetaResponse.ExecutionStatus status = thetaResponse.getExecutionStatus();
                        if (status != ThetaResponse.ExecutionStatus.DONE) {
                            Util.logError(("Failed deleting Theta image. Unexpected status: " + status + " URI: " + thetaUri));
                        }
                    }
                }

                sessionComplete();
                listener.onThetaPictureReady(localPathname, hdrSupported);
            }

            @Override
            public void onFailure(Call<ThetaResponse> call, Throwable t) {
                Util.logError("Failed deleting Theta image. URI: " + thetaUri);
                sessionComplete();
                listener.onThetaPictureReady(localPathname, hdrSupported);
            }
        });
    }

    /*
     * Waits for the fingerprint to change, which indicates that the photo on the camera is ready
     * for download.
     */
    private void getUpdates() {
        Call<ThetaUpdatesResponse> call = ThetaWebService.checkForUpdates(context, currentFingerprint);
        call.enqueue(new Callback<ThetaUpdatesResponse>() {
            @Override
            public void onResponse(Call<ThetaUpdatesResponse> call, Response<ThetaUpdatesResponse> response) {
                if (response.code() < 200 || response.code() > 299) {
                    Util.logError("Failed waiting for picture. HTTP status: " + response.code());
                    onSessionError();
                    return;
                }

                final ThetaUpdatesResponse thetaResponse = response.body();
                if (thetaResponse == null) {
                    Util.logError(("Failed waiting for picture: null response"));
                    onSessionError();
                    return;
                }

                if (!currentFingerprint.contentEquals(thetaResponse.stateFingerprint)) {
                    // Fingerprint has changed so image might be ready for download.
                    getState(false);
                } else {
                    // Keep trying until this fails or we get it. The requests seem to take about
                    // one second while waiting so the camera seems to be throttling them.
                    getUpdates();
                }
            }

            @Override
            public void onFailure(Call<ThetaUpdatesResponse> call, Throwable t) {
                Util.logError("Failed waiting for picture", t);
                onSessionError();
            }
        });
    }

    /*
     * As the docs suggest, we query state at two different times during a session:
     *
     * 1. Immediately after starting the session we get the state so that we have the current
     *    fingerprint. We use this after the picture is taken to know when the picture is ready to
     *    download.
     * 2. After taking the picture and determining that it is ready, we call this again to get
     *    the filename of the photo on the phone.
     */
    private void getState(final boolean isInitial) {
        Call<ThetaStateResponse> call = ThetaWebService.getApi(context).getState();
        call.enqueue(new Callback<ThetaStateResponse>() {
            @Override
            public void onResponse(Call<ThetaStateResponse> call, Response<ThetaStateResponse> response) {
                if (response.code() < 200 || response.code() > 299) {
                    Util.logError("Failed checking state. HTTP status: " + response.code());
                    onSessionError();
                    return;
                }

                final ThetaStateResponse thetaResponse = response.body();
                if (thetaResponse == null) {
                    Util.logError(("Failed checking state: null response"));
                    onSessionError();
                    return;
                }

                if (isInitial) {
                    if (!TextUtils.isEmpty(thetaResponse.fingerprint)) {
                        currentFingerprint = thetaResponse.fingerprint;
                        takePicture();
                    } else {
                        Util.logError(("Failed getting initial fingerprint from camera"));
                        onSessionError();
                    }
                } else {
                    if (thetaResponse.state == null) {
                        Util.logError(("Failed getting photo URI from camera"));
                        onSessionError();
                    } else if (!TextUtils.isEmpty(thetaResponse.state._latestFileUri)) {
                        // Go download photo
                        new ImageDownloadTask(context, thetaResponse.state._latestFileUri).execute();
                    } else {
                        // The photo is not always ready when the fingerprint changes. It looks
                        // like other states can also cause the fingerprint to change, for example
                        // when camera error changes from:
                        // "_cameraError":["NO_DATE_SETTING","COMPASS_CALIBRATION"]
                        // to:
                        // "_cameraError":["NO_DATE_SETTING"]
                        //
                        currentFingerprint = thetaResponse.fingerprint;
                        getUpdates();
                    }
                }
            }

            @Override
            public void onFailure(Call<ThetaStateResponse> call, Throwable t) {
                Util.logError("Failed waiting for photo", t);
                onSessionError();
            }
        });
    }








    public interface ThetaControllerListener {
        void onThetaPictureReady(String pathname, boolean hdrSupported);
    }

    /**
     * Task that connects to Theta S WiFi access point
     */
    private class ConnectionTask extends AsyncTask<Void, Void, Boolean> {
        private static final String DIALOG_CONNECTING_TAG = "ThetaController:DIALOG_CONNECTING_TAG";
        private static final String WAKE_LOCK_TAG = "ConnectionTask:WAKE_LOCK_TAG";
        private static final int NETWORK_RETRY_INTERVAL_MS = 200;
        private static final int NETWORK_CONNECT_TIMEOUT_SEC = 40;
        private PowerManager.WakeLock wakeLock;
        private Network network;
        private WeakReference<AppCompatActivity> context;
        ThetaConnectingDialogFragment dialog;


        ConnectionTask(AppCompatActivity context) {
            this.context = new WeakReference<>(context);
        }

        @Override
        protected void onPreExecute() {

            Timber.i("showing wifi connecting dialog");
            dialog = ThetaConnectingDialogFragment.newInstance();
            dialog.setCancelable(false);
            dialog.show(activity.getFragmentManager(), DIALOG_CONNECTING_TAG);



            //Todo: replace with a helper class

            if (context != null && context.get() != null) {
                PowerManager powerManager = (PowerManager) context.get().getSystemService(Context.POWER_SERVICE);
                if (powerManager != null) {
                    wakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, WAKE_LOCK_TAG);
                }
                if (wakeLock != null) {
                    wakeLock.acquire(10*60*1000L /*10 minutes*/);
                }
            }
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if (context == null || context.get() == null) {
                return false;
            }

            network = new Network(context.get());

            final String originalSSID = network.getCurrentSSID();
            if (!TextUtils.isEmpty(originalSSID) && !originalSSID.contentEquals(cameraSSID)) {
                Timber.i("saving original ssid: %s", originalSSID);
                new AppSharedPreferences(context.get()).putLastSSID(originalSSID);
            }

            if (!isCancelled() && !network.connectToNetwork(cameraSSID)) {
                return false;
            }


            if (!isCancelled() && !waitForConnection(cameraSSID)) {
                return false;
            }

            return !isCancelled();
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (context == null || context.get() == null) {
                return;
            }

//            DialogFragment dialog = (DialogFragment) context.get().getFragmentManager().findFragmentByTag(DIALOG_CONNECTING_TAG);
            if (dialog != null) {
                dialog.dismiss();
            }

            if (success) {
                onCameraConnected();
            } else {
                Timber.i("camera not found and connecting to last ssid");
                if (network != null) {
                    network.connectToLastSSID();
                }
                final DialogFragment notFoundDialog = ThetaNotFoundDialogFragment.newInstance();
                notFoundDialog.show(context.get().getFragmentManager(), "theta_not_found");
            }

            wakeLock.release();
        }

        @Override
        protected void onCancelled() {

            network.connectToLastSSID();
            wakeLock.release();
        }

        private boolean waitForConnection(String ssid) {
            final long start = System.currentTimeMillis();
            for (; ; ) {
                if (network.isConnected(ssid)) {
                    Timber.i("connected to network %s", ssid);
                    return true;
                }

                if ((System.currentTimeMillis() - start) > (NETWORK_CONNECT_TIMEOUT_SEC * 1000)) {
                    Util.logError("Timed out waiting to connect network " + ssid);
                    return false;
                }

                if (isCancelled()) {
                    return false;
                }

                try {
                    Thread.sleep(NETWORK_RETRY_INTERVAL_MS);
                } catch (InterruptedException ignore) {
                }
            }
        }
    }

    private class ImageDownloadTask extends AsyncTask<Void, Void, String> {
        private final String imageURI;
        private final Context context;

        public ImageDownloadTask(Context context, String imageURI) {
            this.context = context;
            this.imageURI = imageURI;
        }

        @Override
        protected String doInBackground(Void... voids) {
            Call<ResponseBody> call = ThetaWebService.getImage(context, imageURI);
            InputStream inputStream = null;
            try {
                inputStream = getInputStream(call.execute());
                if (inputStream == null) {
                    return "";
                }

                return readFile(inputStream);
            } catch (IOException e) {
                Util.logError("Error downloading camera image", e);
                return "";
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException ignore) {
                    }
                }
            }
        }

        @Override
        protected void onPostExecute(String imagePathname) {
            if (!TextUtils.isEmpty(imagePathname)) {
                deleteFile(imageURI, imagePathname);
            } else {
                onSessionError();
            }
        }

        private InputStream getInputStream(Response<ResponseBody> response) {
            if (response.code() < 200 || response.code() > 299) {
                Util.logError("Failed downloading image. HTTP status: " + response.code());
                onSessionError();
                return null;
            }

            final ResponseBody body = response.body();
            if (body == null) {
                Util.logError(("Failed downloading image: empty body"));
                onSessionError();
                return null;
            }

            final InputStream inputStream = body.byteStream();
            if (inputStream == null) {
                Util.logError(("Failed downloading image: null stream"));
                onSessionError();
                return null;
            }

            return inputStream;
        }

        private String readFile(InputStream stream) {
            final Bitmap bitmap = BitmapFactory.decodeStream(stream);
            if (bitmap == null) {
                Util.logError("Error decoding input stream from camera");
                return "";
            }

            FileOutputStream outputStream = null;
            try {
                final File destination = FileUtil.getNextEZ360File(vin);
                outputStream = new FileOutputStream(destination);
                if (!bitmap.compress(Bitmap.CompressFormat.JPEG, 95, outputStream)) {
                    Util.logError("Error compressing camera image");
                    return "";
                }
                return destination.getAbsolutePath();
            } catch (FileNotFoundException e) {
                Util.logError("Error writing image file", e);
                return "";
            } finally {
                if (outputStream != null) {
                    try {
                        outputStream.close();
                    } catch (IOException ignore) {
                    }
                }
            }
        }
    }

}
