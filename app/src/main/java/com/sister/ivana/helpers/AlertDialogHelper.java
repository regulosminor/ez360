package com.sister.ivana.helpers;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.sister.ivana.R;

public class AlertDialogHelper {

    public static AlertDialog showProgressBar(Context context, String textViewMessage){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view1 = inflater.inflate(R.layout.layout_custom_progress_bar, null);
        TextView textView = view1.findViewById(R.id.progress_bar_text_view);
        textView.setText(textViewMessage);
        builder.setView(view1);
        AlertDialog dialog = builder.create();
        dialog.setCancelable(false);

        return  dialog;
    }
}

