package com.sister.ivana;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.support.v7.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import timber.log.Timber;

public class ThetaScanner {
    public interface ThetaScannerListener {
        void onThetaScanComplete(List<String> scanResults);
    }

    private WifiManager wifiManager;
    private Context context;
    private BroadcastReceiver wifiScanReceiver;

//    public static boolean isThetaNetwork(String ssid) {
//        return ssid.startsWith("THETAXS") && ssid.endsWith(".OSC");
//    }

    // TODO: Elbert's changes
    public static boolean isThetaNetwork(String ssid) {
        return ssid.startsWith("THETA") && ssid.endsWith(".OSC");
    }

    public ThetaScanner(final Context context, final ThetaScannerListener listener) {

        this.context = context;

        wifiScanReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context c, Intent intent) {
                ThetaScanner.this.cancelScan();
                List<String> matchingScanResults = new ArrayList<String>();

                Timber.i("onReceive: %s", intent.getAction());

                if (intent.getAction().equals(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)) {
                    List<ScanResult> scanResults = wifiManager.getScanResults();
                    for (ScanResult i : scanResults) {
                        Timber.i("scan result: %s", i.SSID);
                        if (isThetaNetwork(i.SSID)) {
                            if (Network.networkIsConfigured(wifiManager, i.SSID)) {
                                Util.logInfo(String.format("Network %s is already configured", i.SSID));
                                matchingScanResults.add(i.SSID);
                            } else {
                                String[] start = i.SSID.split("THETA"); // THETAXS
                                String[] end = start[1].split(".OSC");
                                String networkPass = end[0].substring(2);

                                WifiConfiguration conf = new WifiConfiguration();
                                conf.SSID = i.SSID;
                                conf.preSharedKey = "\""+ networkPass +"\"";
                                final int networkID = wifiManager.addNetwork(conf);
                                if (networkID != -1) {
                                    if (!wifiManager.enableNetwork(networkID, false)) {
                                        Util.logError(String.format("Failed enabling network %s %s", i.SSID, networkID));
                                    } else {
                                        Timber.i("Enabled network: %s", i.SSID);
                                    }
                                } else {
                                    Util.logError("Failed adding network " + i.SSID);
                                }
                                matchingScanResults.add(i.SSID);
                            }
                        }
                    }
                }

                listener.onThetaScanComplete(matchingScanResults);
            }
        };

        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        context.registerReceiver(wifiScanReceiver, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));

        Timber.i("Starting scan for Theta");
        wifiManager.startScan();
    }

    public void cancelScan() {
        Timber.i("cancelScan");
        if (wifiScanReceiver != null) {
            context.unregisterReceiver(wifiScanReceiver);
            wifiScanReceiver = null;
        }
    }
}
