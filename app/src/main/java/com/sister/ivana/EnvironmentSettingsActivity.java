package com.sister.ivana;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.widget.CheckBox;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class EnvironmentSettingsActivity extends Activity {
    @BindView(R.id.turn_table_enabled) CheckBox turnTableCheckBox;
    @BindView(R.id.auto_upload_enabled) CheckBox autoUploadCheckBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_environment_settings);
        ButterKnife.bind(this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        turnTableCheckBox.setChecked(preferences.getUseTurntable());
        autoUploadCheckBox.setChecked(preferences.areUploadsEnabled());
    }

    @OnCheckedChanged(R.id.turn_table_enabled)
    public void onTurnTableChanged() {
        new AppSharedPreferences(this).putUseTurntable(turnTableCheckBox.isChecked());
    }

    @OnCheckedChanged(R.id.auto_upload_enabled)
    public void onAutoUploadChanged() {
        new AppSharedPreferences(this).putUploadsEnabled(autoUploadCheckBox.isChecked());
    }

    @OnClick(R.id.next_button)
    public void onNextClick() {
        startActivity(new Intent(this, VinSearchActivity.class));
    }
}
