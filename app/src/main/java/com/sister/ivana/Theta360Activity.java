package com.sister.ivana;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sister.ivana.thetaskd.network.HttpConnector;
import com.sister.ivana.thetaskd.network.HttpEventListener;
import com.sister.ivana.thetaskd.view.MJpegInputStream;
import com.sister.ivana.thetaskd.view.MJpegView;

import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class Theta360Activity extends AppCompatActivity {

    @BindView(R.id.live_view) MJpegView mMv;
    @BindView(R.id.btn_shoot) Button btnShoot;
    @BindView(R.id.tv_logs) TextView tvLogs;
    //private LogView logViewer;

    private static final String CAMERA_IP_ADDRESS = "192.168.1.1";
    private static final String THETA_LIVE_VIEW_INIT_DIALOG_TAG = "theta_live_view_init_dialog";

    private ShowLiveViewTask livePreviewTask = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theta_360);
        ButterKnife.bind(this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        mMv.stopPlay();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mMv.play();
//
//        if (livePreviewTask != null) {
//            livePreviewTask.cancel(true);
//            livePreviewTask = new ShowLiveViewTask();
//            livePreviewTask.execute(CAMERA_IP_ADDRESS);
//        }
    }

    @OnClick({R.id.btn_start_live_view, R.id.btn_shoot})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_start_live_view:
                if (livePreviewTask == null) {
                    livePreviewTask = new ShowLiveViewTask();
                    livePreviewTask.execute(CAMERA_IP_ADDRESS);
                } else {
                    mMv.play();

                    livePreviewTask.cancel(true);
                    livePreviewTask = new ShowLiveViewTask();
                    livePreviewTask.execute(CAMERA_IP_ADDRESS);
                }
                break;
            case R.id.btn_shoot:
                btnShoot.setEnabled(false);
                new ShootTask().execute();
                break;
        }
    }


    private class ShowLiveViewTask extends AsyncTask<String, String, MJpegInputStream> {

        AlertDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            ThetaLiveViewDialogFragment dialog = ThetaLiveViewDialogFragment.newInstance();
            dialog.setCancelable(true);
            dialog.show(getFragmentManager(), THETA_LIVE_VIEW_INIT_DIALOG_TAG);
        }

        @Override
        protected MJpegInputStream doInBackground(String... ipAddress) {
            MJpegInputStream mjis = null;
            final int MAX_RETRY_COUNT = 20;

            for (int retryCount = 0; retryCount < MAX_RETRY_COUNT; retryCount++) {
                try {
                    publishProgress("start Live view");
                    HttpConnector camera = new HttpConnector(ipAddress[0]);
                    InputStream is = camera.getLivePreview();
                    mjis = new MJpegInputStream(is);
                    retryCount = MAX_RETRY_COUNT;
                } catch (IOException | JSONException e) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
            }

            return mjis;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            for (String log : values) {
                //logViewer.append(log);
                Log.e("THETA", log);
            }
        }

        @Override
        protected void onPostExecute(MJpegInputStream mJpegInputStream) {
            ThetaLiveViewDialogFragment fragment = (ThetaLiveViewDialogFragment) getFragmentManager().findFragmentByTag(THETA_LIVE_VIEW_INIT_DIALOG_TAG);
            if (fragment != null) {
                fragment.dismiss();
            }

            btnShoot.setEnabled(true);

            if (mJpegInputStream != null) {
                mMv.setSource(mJpegInputStream);
            } else {
                //logViewer.append("failed to start live view");
            }
        }
    }

    private class ShootTask extends AsyncTask<Void, Void, HttpConnector.ShootResult> {

        @Override
        protected void onPreExecute() {
            System.out.println("Theta: take picture");
            //logViewer.append("takePicture");
        }

        @Override
        protected HttpConnector.ShootResult doInBackground(Void... params) {
            mMv.setSource(null);
            CaptureListener postviewListener = new CaptureListener();
            HttpConnector camera = new HttpConnector(getResources().getString(R.string.theta_ip_address));
            HttpConnector.ShootResult result = camera.takePicture(postviewListener);

            return result;
        }

        @Override
        protected void onPostExecute(HttpConnector.ShootResult result) {
           // tvLogs.setVisibility(View.VISIBLE);
            if (result == HttpConnector.ShootResult.FAIL_CAMERA_DISCONNECTED) {
                //.setText("takePicture:FAIL_CAMERA_DISCONNECTED");
            } else if (result == HttpConnector.ShootResult.FAIL_STORE_FULL) {
                //tvLogs.setText("takePicture:FAIL_STORE_FULL");
            } else if (result == HttpConnector.ShootResult.FAIL_DEVICE_BUSY) {
               // tvLogs.setText("takePicture:FAIL_DEVICE_BUSY");
            } else if (result == HttpConnector.ShootResult.SUCCESS) {
                //tvLogs.setText("takePicture:SUCCESS");
            }
        }

        private class CaptureListener implements HttpEventListener {
            private String latestCapturedFileId;
            private boolean ImageAdd = false;

            @Override
            public void onCheckStatus(boolean newStatus) {
                if(newStatus) {
                    System.out.println("Theta: Take picture finished");
                    //appendLogView("takePicture:FINISHED");
                } else {
                    //tvLogs.setText("takePicture:IN PROGRESS");
                    //appendLogView("takePicture:IN PROGRESS");
                }
            }

            @Override
            public void onObjectChanged(String latestCapturedFileId) {
                this.ImageAdd = true;
                this.latestCapturedFileId = latestCapturedFileId;

                System.out.println("ImageAdd:FileId " + this.latestCapturedFileId);
                //appendLogView("ImageAdd:FileId " + this.latestCapturedFileId);
            }

            @Override
            public void onCompleted() {
                //appendLogView("CaptureComplete");
                if (ImageAdd) {
//                    btnShoot.setEnabled(true);
                    System.out.println("Theta: View photo");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                           // btnShoot.setEnabled(true);
                            //textCameraStatus.setText(R.string.text_camera_standby);
                            new GetThumbnailTask(latestCapturedFileId).execute();
                        }
                    });
                }
            }

            @Override
            public void onError(String errorMessage) {
                //appendLogView("CaptureError " + errorMessage);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                       // btnShoot.setEnabled(true);
                        //textCameraStatus.setText(R.string.text_camera_standby);
                    }
                });
            }
        }

    }

    private class GetThumbnailTask extends AsyncTask<Void, String, Void> {

        private String fileId;

        public GetThumbnailTask(String fileId) {
            this.fileId = fileId;
        }

        @Override
        protected Void doInBackground(Void... params) {
            HttpConnector camera = new HttpConnector(getResources().getString(R.string.theta_ip_address));
            Bitmap thumbnail = camera.getThumb(fileId);
            if (thumbnail != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] thumbnailImage = baos.toByteArray();

                ThetaPhotoActivity.startActivityForResult(Theta360Activity.this, CAMERA_IP_ADDRESS, fileId, thumbnailImage, true);
            } else {
                publishProgress("failed to get file data.");
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            for (String log : values) {
                //logViewer.append(log);
            }
        }
    }
}
