package com.sister.ivana.webapi.elasticsearch;


import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ESWebApi {
    @GET("vehicles/inventory/_search")
    Call<InventoryResults> getInventory(@Query("size") int size, @Query("q") String query);

    @GET("misc/project_settings/_search")
    Call<ProjectResults> getProjectSettings(@Query("q") String project);

//    @GET("ivana/magazine/AVv5fGAYBnlLp5CnvCyb")
    @GET("ivana/magazine/_search")
    Call<MagazineResults> getMagazines(@Query("size") int size, @Query("q") String project);

    @GET("misc/banner_settings/_search")
    Call<BannerResults> getBannerSettings(@Query("size") int size, @Query("q") String query);

    @GET("settings/ivana_app/{project}/_source")
    Call<JsonObject> getAppSettings(@Path("project") String project);

    @Headers("Content-type: application/json")
    @PUT("settings/ivana_app/{project}")
    Call<JsonObject> putAppSettings(@Path("project") String project, @Body JsonObject text);

    @PUT("settings")
    Call<JsonObject> createAppSettings();
}
