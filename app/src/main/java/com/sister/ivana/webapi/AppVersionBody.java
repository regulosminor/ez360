package com.sister.ivana.webapi;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.sister.ivana.Util;

public class AppVersionBody {
    public String appVersion = AppVersionBody.sAppVersion;

    public static String sAppVersion;
    public static void updateAppVersion(Context context) {
        try {
            PackageManager packageManager = context.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(context.getPackageName(), 0);
            sAppVersion = String.format("iVana%s", packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Util.logError("Error reading version info", e);
            sAppVersion = "iVanaUnknown";
        }
    }
}
