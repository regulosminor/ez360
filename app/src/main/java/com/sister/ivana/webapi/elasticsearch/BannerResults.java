package com.sister.ivana.webapi.elasticsearch;


import java.util.List;

public class BannerResults {
    public static class BannerData {
        public String banner_url;
    }

    public static class BannerHit {
        public BannerData _source;
    }

    public static class HitCollection {
        public List<BannerHit> hits;
    }

    public HitCollection hits;
}
