package com.sister.ivana.webapi;


import java.util.ArrayList;

public class DetailsBody extends AppVersionBody {
    public static class Image {
        public String image_url;
        public String feature_code;
        public String feature_label;
        public String animated_picture_url;
        public String animated_picture_thumbnail_url;
    }

    public static class Images {
        public ArrayList<Image> images;
    }

    public String library_id;
    public String project_id;
    public String vin;
    public boolean replace;
    public Images detail_pic_object;
}
