package com.sister.ivana.webapi;


public class PanoramaBody extends AppVersionBody {
    public String library_id;
    public String project_id;
    public String vin;
    public boolean replace;
    public String interior_panorama_url;
}
