package com.sister.ivana.webapi.elasticsearch;


import java.util.List;

public class ProjectResults {
    public static class ProjectData {
        public String dealer_name;
    }

    public static class ProjectHit {
        public ProjectData _source;
    }

    public static class HitCollection {
        public List<ProjectHit> hits;
    }

    public HitCollection hits;
}
