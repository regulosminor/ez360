package com.sister.ivana.webapi;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface AuthWebApi {
    @GET("sis-auth/user-detail/{user}/")
    Call<UserDetail> getUserDetail(@Header("AUTHORIZATION") String authorization, @Path("user") String user);

    @POST("api-token-auth/")
    Call<Token> postTokenRequest(@Body TokenRequest request);
}
