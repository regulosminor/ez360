package com.sister.ivana.webapi;

public class TokenRequest {
    public TokenRequest(String username, String password) {
        // SiSTeR wants to allow people to enter mixed case email addresses but their back end
        // expects all lowercase
        this.username = username.toLowerCase();
        this.password = password;
    }

    public final String username;
    public final String password;
}
