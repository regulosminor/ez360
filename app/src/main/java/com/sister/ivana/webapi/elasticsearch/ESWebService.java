package com.sister.ivana.webapi.elasticsearch;


import android.content.Context;

import com.google.gson.JsonObject;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.logging.AppLoggingInterceptor;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Query;

public class ESWebService {
    private static final String BASE_URL = "http://es-data-secure.sister.tv/";
    private static ESWebApi api;

    private static ESWebApi getApi(Context context) {
        if (api == null) {
            OkHttpClient httpClient = new OkHttpClient.Builder()
                    .addInterceptor(new AppLoggingInterceptor(new AppSharedPreferences(context.getApplicationContext())))
                    .readTimeout(1, TimeUnit.MINUTES)
                    .build();

            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(httpClient)
                    .addConverterFactory(GsonConverterFactory.create());

            api = builder.build().create(ESWebApi.class);
        }

        return api;
    }

    public Call<InventoryResults> getInventory(Context context, int size, String projectID) {
        final DateTimeFormatter formatter = ISODateTimeFormat.yearMonthDay();
        final String from = formatter.print(new DateTime().plusDays(-1));
        final String to = formatter.print(new DateTime(2020, 1, 1, 0, 0));

        final String query = String.format("project_id:%s AND expire_on:[%s TO %s] AND (NOT is_soft_del:true)", projectID, from, to);
        return getApi(context).getInventory(size, query);
    }

    public Call<ProjectResults> getProjectSettings(Context context, String projectID) {
        final String query = String.format("project_id:%s", projectID);
        return getApi(context).getProjectSettings(query);
    }

    public Call<MagazineResults> getMagazines(Context context, String projectID) {
        final String query = String.format("project_id:%s", projectID);
        return getApi(context).getMagazines(10000, query);
    }

    public Call<BannerResults> getBannerSettings(Context context, String projectID) {
        final String query = String.format("project_id:%s", projectID);
        return getApi(context).getBannerSettings(1, query);
    }

    public Call<JsonObject> getAppSettings(Context context, String projectID) {
        return getApi(context).getAppSettings(projectID);
    }

    public Call<JsonObject> putAppSettings(Context context, String projectID, JsonObject settingsJson) {
        return getApi(context).putAppSettings(projectID, settingsJson);
    }

    public Call<JsonObject> createAppSettings(Context context) {
        return getApi(context).createAppSettings();
    }

    public Call<InventoryResults> getVehicle(Context context, String vin) {
        final String query = String.format("vin:%s", vin);
        return getApi(context).getInventory(1, query);
    }
}
