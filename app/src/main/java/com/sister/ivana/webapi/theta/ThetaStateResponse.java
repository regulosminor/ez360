package com.sister.ivana.webapi.theta;


public class ThetaStateResponse {
    static public class State {
        public String _latestFileUri;
    }

    public String fingerprint;
    public State state;
}
