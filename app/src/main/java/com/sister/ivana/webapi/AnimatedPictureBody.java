package com.sister.ivana.webapi;


import java.util.ArrayList;

public class AnimatedPictureBody extends AppVersionBody {
    public String library_id;
    public String project_id;
    public String vin;
    public boolean replace;
    public String video;
    public ArrayList<String> images;
}
