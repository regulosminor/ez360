package com.sister.ivana.webapi;


public class FullMotionVideoUploadDescription extends AppVersionBody {
    public String library_id;
    public boolean replace;
    public String user;
    public String project_id;
    public String vin;
    public String fullmo_raw;
}
