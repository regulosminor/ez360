package com.sister.ivana.webapi;


public class PipBody extends AppVersionBody {
    public static class Coordinate {
        public double x;
        public double y;
    }

    public String library_id;
    public String project_id;
    public String vin;
    public String wide_image_url;
    public String detail_image_url;
    public String camera_id;
    public Coordinate picture_coordinate;
}

