package com.sister.ivana.webapi;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface WebApi {
    @POST("ivana/fullmo_raw/")
    Call<Void> postFullMotionVideo(@Body FullMotionVideoUploadDescription upload);

    @POST("ivana/details/")
    Call<Void> postDetails(@Body DetailsBody upload);

    @POST("ivana/long_click/")
    Call<Void> postAnimatedPicture(@Body AnimatedPictureBody upload);

    @POST("ivana/exterior_poi/")
    Call<Void> postAddPoi(@Body AddPoiBody upload);

    // TODO: endpoint has not been created yet and might change
    @POST("ivana/pip/")
    Call<Void> postPip(@Body PipBody upload);

    @POST("ivana/interior_panorama/")
    Call<Void> postInteriorPanorama(@Body PanoramaBody upload);

    @POST("ivana/exterior_spin/")
    Call<Void> postExteriorSpin(@Body ExteriorSpinBody upload);

    @GET("ivana/cur_version/")
    Call<CurrentVersion> getCurrentVersion();
}
