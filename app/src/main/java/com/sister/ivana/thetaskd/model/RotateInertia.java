package com.sister.ivana.thetaskd.model;

/**
 * Indicates the rotation inertia
 */
public enum RotateInertia {
    /** none */
    INERTIA_0,
    /** weak */
    INERTIA_50,
    /** strong */
    INERTIA_100,;
}