package com.sister.ivana;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.j256.ormlite.stmt.PreparedQuery;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.Magazine;
import com.sister.ivana.database.OrmLiteBaseAppCompatActivity;
import com.sister.ivana.database.Vehicle;
import com.sister.ivana.preview.GalleryActivity;
import com.sister.ivana.upload.PendingMediaCounts;
import com.sister.ivana.upload.UploadQueue;
import com.sister.ivana.upload.UploaderService;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import net.sourceforge.opencamera.AnimatedPictureShot;
import net.sourceforge.opencamera.CameraActivity;
import net.sourceforge.opencamera.CustomPoiShot;
import net.sourceforge.opencamera.ImageDescription;
import net.sourceforge.opencamera.PipShot;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnLongClick;
import timber.log.Timber;

public class VehicleActivity
        extends OrmLiteBaseAppCompatActivity<DatabaseHelper>
        implements
        ExteriorSpinOptionsTurntableDialogFragment.SpinOptionsTurntableListener,
        MagazineSelectorDialogFragment.MagazineSelectorListener,
        ExteriorSpinOptionsNoTurntableDialogFragment.SpinOptionsNoTurntableListener,
        ThetaScanningDialogFragment.ThetaScanningListener,
        ThetaScanner.ThetaScannerListener,
        HdrAvailableDialogFragment.HdrListener {

    private static class MagazineData {
        ArrayList<Magazine> magazines;
        int defaultSelection;
    }

    @BindView(R.id.vin_text_view) TextView vinTextView;
    @BindView(R.id.stock_id_textview) TextView stockTextView;
    @BindView(R.id.year_textview) TextView yearTextView;
    @BindView(R.id.make_textview) TextView makeTextView;
    @BindView(R.id.model_textview) TextView modelTextView;
    @BindView(R.id.mileage_textview) TextView milesTextView;
    @BindView(R.id.new_used_textview) TextView newUsedTextView;
    @BindView(R.id.replace_content) RadioButton replaceContentButton;
    @BindView(R.id.append_content) RadioButton appendContentButton;
    @BindView(R.id.replace_group) RadioGroup replaceGroup;
    @BindView(R.id.spin_media_count) TextView spinMediaCount;
    @BindView(R.id.exterior_pictures_media_count) TextView exteriorPicturesMediaCount;
    @BindView(R.id.interior_360_media_count) TextView interior360MediaCount;
    @BindView(R.id.video_media_count) TextView videoMediaCount;
    @BindView(R.id.thumbnail) ImageView thumbnailImageView;

    private static final String EXTRA_VIN = "Ivana.EXTRA_VIN";
    private static final int REQUEST_CODE_PICTURES = 101;
    private static final int REQUEST_CODE_VIDEO = 102;
    private static final int REQUEST_CODE_SPINIT_PICTURES = 103;
    private static final int REQUEST_CODE_SPINIT_EXTRACT = 104;
    private static final int REQUEST_CODE_SPINIT_EXTRACT_AND_VIDEO = 105;
    private static final int REQUEST_CODE_SPINIT_NO_TURNTABLE_PICTURES = 106;
    private static final int REQUEST_CODE_SPINIT_NO_TURNTABLE_EXTRACT = 107;
    private static final int REQUEST_CODE_SPINIT_NO_TURNTABLE_EXTRACT_AND_VIDEO = 108;
    private Vehicle vehicle;
    private ActionBarUploadStatus actionBarStatus;
    private AppSharedPreferences preferences;
    private ThetaController thetaController;
    private ThetaScanner thetaScanner;

    private final String THETA_SCANNING_DIALOG_TAG = "theta_scanning_dialog";

    public static Intent createIntent(Context context, String vin) {
        Intent intent = new Intent(context, VehicleActivity.class);
        intent.putExtra(EXTRA_VIN, vin);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        preferences = new AppSharedPreferences(this);
        final String vin = getIntent().getStringExtra(EXTRA_VIN);
        try {
            vehicle = getHelper().getVehicleDao().queryForId(vin);
            if (vehicle == null) {
                Util.logError("Couldn't find vehicle in database");
                finish();
            }
        } catch (SQLException e) {
            Util.logError("Error finding car by VIN", e);
        }

        replaceGroup.check(R.id.append_content);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Util.getBus().register(this);

        actionBarStatus = new ActionBarUploadStatus();
        actionBarStatus.onStart(this);

        final String url = vehicle.getThumbnail();
        if (!TextUtils.isEmpty(url)) {
            Picasso.get()
                    .load(url)
                    .fit()
                    .into(thumbnailImageView);
        }

        populateFields();
        invalidateOptionsMenu();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (thetaScanner != null) {
            dismissThetaScanningDialog();
            thetaScanner.cancelScan();
            thetaScanner = null;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (thetaController != null) {
            thetaController.onSaveInstanceState();
        }
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onStop() {
        super.onStop();
        actionBarStatus.onStop();
        Util.getBus().unregister(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }

        switch (requestCode) {
            case REQUEST_CODE_PICTURES: {
                if (resultCode == CameraActivity.HAVE_IMAGES_RESULT_CODE) {
                    final ArrayList<ImageDescription> shots = data.getParcelableArrayListExtra(CameraActivity.EXTRA_IMAGE_DESCRIPTIONS);
                    enqueueDetailPictures(shots);

                    final ArrayList<CustomPoiShot> customPoiShots = data.getParcelableArrayListExtra(CameraActivity.EXTRA_CUSTOM_POIS);
                    enqueueCustomPoiShots(customPoiShots);

                    final ArrayList<PipShot> pipShots = data.getParcelableArrayListExtra(CameraActivity.EXTRA_PIPS);
                    enqueuePipShots(pipShots);

                    final ArrayList<AnimatedPictureShot> animatedPictureShots = data.getParcelableArrayListExtra(CameraActivity.EXTRA_ANIMATED_PICTURES);
                    enqueueAnimatedPictureShots(animatedPictureShots);
                }
            }
            break;

            case REQUEST_CODE_VIDEO: {
                if (resultCode == CameraActivity.HAVE_VIDEO_RESULT_CODE) {
                    final String pathname = data.getStringExtra(CameraActivity.EXTRA_VIDEO_PATHNAME);

                    if (vehicle.isCameraOnly()) {
                        if (new File(pathname).delete()) {
                            Timber.i("Deleted camera only video %s", pathname);
                        } else {
                            Timber.e("Failed deleting camera only video %s", pathname);
                        }

                        break;
                    }

                    final Intent intent = UploaderService.createFullMotionVideoIntent(
                            VehicleActivity.this,
                            vehicle.getVin(),
                            vehicle.getYear(),
                            vehicle.getMake(),
                            vehicle.getModel(),
                            vehicle.getProject(),
                            vehicle.getLibrary(),
                            shouldReplaceMedia(),
                            pathname);
                    startService(intent);
                }
            }
            break;

            case REQUEST_CODE_SPINIT_PICTURES: {
                if (resultCode == CameraActivity.HAVE_IMAGES_RESULT_CODE) {
                    final ArrayList<ImageDescription> shots = data.getParcelableArrayListExtra(CameraActivity.EXTRA_IMAGE_DESCRIPTIONS);
                    final AppSharedPreferences preferences = new AppSharedPreferences(this);
                    enqueueSpinitPictures(shots, true, preferences.getSpinNoTurntableAutoPictureCount(), preferences.getSpinItVideoMaxDuration(), preferences.getSpinitTotalFrames());
                }
            }
            break;

            case REQUEST_CODE_SPINIT_EXTRACT:
            case REQUEST_CODE_SPINIT_EXTRACT_AND_VIDEO: {
                if (resultCode == CameraActivity.HAVE_VIDEO_RESULT_CODE) {
                    final String pathname = data.getStringExtra(CameraActivity.EXTRA_VIDEO_PATHNAME);
                    final File video = new File(pathname);
                    if (video.exists()) {
                        final int frameCount = new AppSharedPreferences(this).getSpinitTotalFrames();
                        final AppSharedPreferences preferences = new AppSharedPreferences(this);
                        final CameraRole role = preferences.getCameraRole();
                        final String roleSymbol = role == null ? "" : role.getSymbol();
                        enqueueSpinitVideo(pathname, true, frameCount, preferences.getSpinNoTurntableAutoPictureCount(), preferences.getSpinItVideoMaxDuration(), preferences.getSpinitTotalFrames(), roleSymbol);
                        if (requestCode == REQUEST_CODE_SPINIT_EXTRACT_AND_VIDEO) {
                            enqueueVideo(pathname);
                        }
                    }
                }
            }
            break;

            case REQUEST_CODE_SPINIT_NO_TURNTABLE_PICTURES: {
                if (resultCode == CameraActivity.HAVE_IMAGES_RESULT_CODE) {
                    final ArrayList<ImageDescription> images = data.getParcelableArrayListExtra(CameraActivity.EXTRA_IMAGE_DESCRIPTIONS);
                    final AppSharedPreferences preferences = new AppSharedPreferences(this);
                    enqueueSpinitPictures(images, false, preferences.getSpinNoTurntableAutoPictureCount(), preferences.getSpinItVideoMaxDuration(), preferences.getSpinitTotalFrames());
                }
            }
            break;

            case REQUEST_CODE_SPINIT_NO_TURNTABLE_EXTRACT:
            case REQUEST_CODE_SPINIT_NO_TURNTABLE_EXTRACT_AND_VIDEO: {
                if (resultCode == CameraActivity.HAVE_VIDEO_RESULT_CODE) {
                    final String pathname = data.getStringExtra(CameraActivity.EXTRA_VIDEO_PATHNAME);
                    final File video = new File(pathname);
                    if (video.exists()) {
                        final int frameCount = new AppSharedPreferences(this).getSpinNoTurntableExtractPictureCount();
                        final AppSharedPreferences preferences = new AppSharedPreferences(this);
                        enqueueSpinitVideo(pathname, false, frameCount, preferences.getSpinNoTurntableAutoPictureCount(), preferences.getSpinItVideoMaxDuration(), preferences.getSpinitTotalFrames(), "");
                        if (requestCode == REQUEST_CODE_SPINIT_NO_TURNTABLE_EXTRACT_AND_VIDEO) {
                            enqueueVideo(pathname);
                        }
                    }
                }
            }
            break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_vehicle_activity, menu);

        MenuItem enableUploads = menu.findItem(R.id.action_enable_upload);
        enableUploads.setVisible(!preferences.areUploadsEnabled());
        MenuItem disableUploads = menu.findItem(R.id.action_disable_upload);
        disableUploads.setVisible(preferences.areUploadsEnabled());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_logout:
                Util.logout(this);
                return true;

            case R.id.action_enable_upload:
                preferences.putUploadsEnabled(true);
                invalidateOptionsMenu();
                return true;

            case R.id.action_disable_upload:
                preferences.putUploadsEnabled(false);
                invalidateOptionsMenu();
                return true;

            case R.id.action_settings:
                startActivity(new Intent(this, com.sister.ivana.settings.SettingsActivity.class));
//                startActivity(new Intent(this, SettingsActivity.class));
                return true;

            case R.id.action_preview:
                startActivity(GalleryActivity.createIntent(this, vehicle.getVin(), vehicle.getProject()));
                return true;

            default:
                return false;
        }
    }

    @Override
    public void onSpinOptionsPositiveClick(boolean cutFramesFromVideo, boolean createFullMotionVideo) {
        final Intent intent;
        final int requestCode;
        if (!cutFramesFromVideo) {
            // Take individual photos
            requestCode = REQUEST_CODE_SPINIT_PICTURES;
            intent = CameraActivity.createSpinItTurntableAutomatedPictureIntent(this, vehicle.getVin());
        } else if (createFullMotionVideo) {
            // Take video, cut individual photos from it and use the video
            requestCode = REQUEST_CODE_SPINIT_EXTRACT_AND_VIDEO;
            intent = CameraActivity.createSpinItVideoTurntableIntent(this, vehicle.getVin(), shouldReplaceMedia());
        } else {
            // Take video and cut individual photos from it (don't do anything with video)
            requestCode = REQUEST_CODE_SPINIT_EXTRACT;
            intent = CameraActivity.createSpinItVideoTurntableIntent(this, vehicle.getVin(), shouldReplaceMedia());
        }

        startActivityForResult(intent, requestCode);
    }

    @Override
    public void onSpinOptionsNoTurntableManualPictures() {
        final Intent intent = CameraActivity.createSpinItNoTurntableManualPictureIntent(this, vehicle.getVin());
        startActivityForResult(intent, REQUEST_CODE_SPINIT_NO_TURNTABLE_PICTURES);
    }

    @Override
    public void onSpinOptionsNoTurntableAutomaticPictures(int frames, int interval) {
        final Intent intent = CameraActivity.createSpinItNoTurntableAutomatedPictureIntent(this, vehicle.getVin(), interval, frames);
        startActivityForResult(intent, REQUEST_CODE_SPINIT_NO_TURNTABLE_PICTURES);
    }

    @Override
    public void onSpinOptionsNoTurntableCutFromVideo(int frames, boolean createVideo) {
        final Intent intent = CameraActivity.createUnlimitedVideoIntent(this, vehicle.getVin());
        final int requestCode = createVideo ? REQUEST_CODE_SPINIT_NO_TURNTABLE_EXTRACT_AND_VIDEO : REQUEST_CODE_SPINIT_NO_TURNTABLE_EXTRACT;
        startActivityForResult(intent, requestCode);
    }

    @OnClick(R.id.pictures_button)
    void onPicturesClick() {
        final MagazineData magazineData = getMagazines();
        if (magazineData.magazines.isEmpty()) {
            startCameraActivity(REQUEST_CODE_PICTURES);
            return;
        }

        final MagazineSelectorDialogFragment dialog;
        dialog = MagazineSelectorDialogFragment.newInstance(magazineData.magazines, REQUEST_CODE_PICTURES);
        dialog.show(getSupportFragmentManager(), "magazine_selector");
    }

    @SuppressWarnings("SameReturnValue")
    @OnLongClick(R.id.pictures_button)
    boolean onPicturesLongClick() {
        final Intent intent = GalleryActivity.createIntent(this, vehicle.getVin(), vehicle.getProject(), GalleryActivity.Mode.DETAIL_PICTURES);
        startActivity(intent);
        return true;
    }

    @OnClick(R.id.video_button)
    void onVideoClick() {
        final Intent intent = CameraActivity.createManualVideoIntent(this, vehicle.getVin());
        startActivityForResult(intent, REQUEST_CODE_VIDEO);
    }

    @SuppressWarnings("SameReturnValue")
    @OnLongClick(R.id.video_button)
    boolean onVideoLongClick() {
        final Intent intent = GalleryActivity.createIntent(this, vehicle.getVin(), vehicle.getProject(), GalleryActivity.Mode.FULLMO_VIDEO);
        startActivity(intent);
        return true;
    }

    @OnClick(R.id.exterior_spin_button)
    void onExteriorSpinClick() {
        if (new AppSharedPreferences(this).getUseTurntable()) {
            ExteriorSpinOptionsTurntableDialogFragment dialog = ExteriorSpinOptionsTurntableDialogFragment.newInstance();
            dialog.show(getFragmentManager(), "spin_it_options");
        } else {
            ExteriorSpinOptionsNoTurntableDialogFragment dialog = ExteriorSpinOptionsNoTurntableDialogFragment.newInstance();
            dialog.show(getFragmentManager(), "spin_it_options_no_turntable");
        }
    }

    @SuppressWarnings("SameReturnValue")
    @OnLongClick(R.id.exterior_spin_button)
    boolean onExteriorSpinLongClick() {
        final Intent intent = GalleryActivity.createIntent(this, vehicle.getVin(), vehicle.getProject(), GalleryActivity.Mode.SPINIT_PICTURES);
        startActivity(intent);
        return true;
    }

    @OnClick(R.id.interior_360_button)
    void on360Click() {
        ThetaScanningDialogFragment dialog = ThetaScanningDialogFragment.newInstance();
        dialog.setCancelable(false);
        dialog.show(getFragmentManager(), THETA_SCANNING_DIALOG_TAG);

        thetaScanner = new ThetaScanner(getApplicationContext(), this);
    }

    private boolean shouldReplaceMedia() {
        boolean shouldReplace = replaceContentButton.isChecked();
        return shouldReplace;
    }

    public void onThetaScanningNegativeClick() {
        if (thetaScanner != null) {
            thetaScanner.cancelScan();
            thetaScanner = null;
        }
    }

    public void onThetaScanComplete(List<String> scanResults) {
        Timber.i("onThetaScanComplete: %s", scanResults);
        dismissThetaScanningDialog();
        thetaController = new ThetaController(this, vehicle.getVin(), scanResults, new ThetaController.ThetaControllerListener() {
            @Override
            public void onThetaPictureReady(String pathname, boolean hdrSupported) {
                Timber.i("onThetaPictureReady: %s hdr: %s", pathname, hdrSupported);
                enqueueEZ360Picture(pathname);
                if (!hdrSupported) {
                    showHdrDialog();
                }
            }
        });
        thetaController.start();
        thetaScanner = null;
    }

    private void showHdrDialog() {
        if (System.currentTimeMillis() > preferences.getHdrNextWarning()) {
            HdrAvailableDialogFragment fragment = HdrAvailableDialogFragment.newInstance();
            fragment.show(getFragmentManager(), "hdr_info");
        }
    }

    @Override
    public void onHdrOk() {
        final long nextWarning = System.currentTimeMillis() + 3 * 60 * 1000;
        preferences.setHdrNextWarning(nextWarning);
    }

    @Override
    public void onHdrDoNotShowAgain() {
        final long nextWarning = System.currentTimeMillis() + 24 * 60 * 60 * 1000;
        preferences.setHdrNextWarning(nextWarning);
    }

    private void dismissThetaScanningDialog() {
        ThetaScanningDialogFragment fragment = (ThetaScanningDialogFragment) getFragmentManager().findFragmentByTag(THETA_SCANNING_DIALOG_TAG);
        if (fragment != null) {
            fragment.dismiss();
        }
    }

    @SuppressWarnings("SameReturnValue")
    @OnLongClick(R.id.interior_360_button)
    boolean on360LongClick() {
        final Intent intent = GalleryActivity.createIntent(this, vehicle.getVin(), vehicle.getProject(), GalleryActivity.Mode.EZ_360);
        startActivity(intent);
        return true;
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onUpdateCounts(PendingMediaCounts counts) {
        if (counts.getVin().contentEquals(vehicle.getVin())) {
            updateCounts(counts);
        }
    }

    @Override
    public void onMagazineListenerPositiveClick(int magazineID, int requestCode) {
        startCameraActivity(requestCode, magazineID);
    }

    @Override
    public void onMagazineListenerNegativeClick(int requestCode) {
        startCameraActivity(requestCode);
    }

    public ThetaController getThetaController() {
        return thetaController;
    }

    private void populateFields() {
        vinTextView.setText(String.format("VIN: %s", vehicle.getVin()));
        stockTextView.setText(vehicle.getStock());
        yearTextView.setText(vehicle.getYear());
        makeTextView.setText(vehicle.getMake());
        modelTextView.setText(vehicle.getModel());
        milesTextView.setText(vehicle.getMiles());
        newUsedTextView.setText(vehicle.getNewUsed());

        PendingMediaCounts counts = UploadQueue.getCounts(getHelper(), vehicle.getVin());
        updateCounts(counts);
    }

    private void updateCounts(PendingMediaCounts counts) {
        updateCount(exteriorPicturesMediaCount, counts.getPicturesCount());
        updateCount(videoMediaCount, counts.getFullMotionVideoCount());
        updateCount(spinMediaCount, counts.getSpinItPictureCount());
        updateCount(interior360MediaCount, counts.getEz360PictureCount());
    }

    private void updateCount(TextView textView, long count) {
        if (count > 0) {
            textView.setText(String.valueOf(count));
            textView.setVisibility(View.VISIBLE);
        } else {
            textView.setVisibility(View.INVISIBLE);
        }
    }

    private void enqueueDetailPictures(ArrayList<ImageDescription> shots) {
        if (vehicle.isCameraOnly()) {
            for (ImageDescription shot : shots) {
                if (shot.image.delete()) {
                    Timber.i("Deleted camera only detail picture %s", shot.image.getAbsolutePath());
                } else {
                    Timber.e("Failed deleting camera only detail picture %s", shot.image.getAbsolutePath());
                }
            }
            return;
        }

        if (shots != null && shots.size() > 0) {
            // Magazine shots and non-magazine shots are already merged in the order we want
            // them so update the sequence numbers
            int sequence = 0;
            for (ImageDescription shot : shots) {
                shot.sequenceNumber = sequence++;
            }

            final Intent intent = UploaderService.createDetailPicturesIntent(
                    this,
                    vehicle.getVin(),
                    vehicle.getYear(),
                    vehicle.getMake(),
                    vehicle.getModel(),
                    vehicle.getProject(),
                    vehicle.getLibrary(),
                    shouldReplaceMedia(),
                    shots);
            startService(intent);
        }
    }

    private void enqueueCustomPoiShots(ArrayList<CustomPoiShot> shots) {
        if (shots == null) {
            return;
        }

        if (vehicle.isCameraOnly()) {
            for (CustomPoiShot shot : shots) {
                if (new File(shot.filename).delete()) {
                    Timber.i("Deleted camera only custom POI picture %s", shot.filename);
                } else {
                    Timber.e("Failed deleting camera only custom POI picture %s", shot.filename);
                }
            }
            return;
        }

        for (CustomPoiShot shot : shots) {
            final Intent intent = UploaderService.createAddPoiIntent(
                    this,
                    vehicle.getVin(),
                    vehicle.getYear(),
                    vehicle.getMake(),
                    vehicle.getModel(),
                    vehicle.getProject(),
                    vehicle.getLibrary(),
                    shouldReplaceMedia(),
                    shot.title,
                    shot.description,
                    shot.filename,
                    0,
                    shot.position,
                    shot.addToMagazine,
                    shot.positionX,
                    shot.positionY);
            startService(intent);
        }
    }

    private void enqueuePipShots(ArrayList<PipShot> shots) {
        if (shots == null) {
            return;
        }

        if (vehicle.isCameraOnly()) {
            for (PipShot shot : shots) {
                if (new File(shot.wideImagePathname).delete()) {
                    Timber.i("Deleted camera only PIP wide angle picture %s", shot.wideImagePathname);
                } else {
                    Timber.e("Failed deleting camera only PIP wide angle picture %s", shot.wideImagePathname);
                }

                if (new File(shot.detailImagePathname).delete()) {
                    Timber.i("Deleted camera only PIP detail picture %s", shot.detailImagePathname);
                } else {
                    Timber.e("Failed deleting camera only PIP detail picture %s", shot.detailImagePathname);
                }

            }
            return;
        }

        for (PipShot shot : shots) {
            final Intent intent = UploaderService.createPipIntent(
                    this,
                    vehicle.getVin(),
                    vehicle.getYear(),
                    vehicle.getMake(),
                    vehicle.getModel(),
                    vehicle.getProject(),
                    vehicle.getLibrary(),
                    shouldReplaceMedia(),
                    shot.wideImagePathname,
                    shot.detailImagePathname,
                    shot.positionX,
                    shot.positionY);
            startService(intent);
        }
    }

    private void enqueueAnimatedPictureShots(ArrayList<AnimatedPictureShot> shots) {
        if (shots == null) {
            return;
        }

        if (vehicle.isCameraOnly()) {
            for (AnimatedPictureShot shot : shots) {
                if (shot.video != null) {
                    if (shot.video.delete()) {
                        Timber.i("Deleted camera only animated picture video %s", shot.video);
                    } else {
                        Timber.e("Failed deleting camera only animated picture video %s", shot.video);
                    }
                }

                for (File frame : shot.frames) {
                    if (frame.delete()) {
                        Timber.i("Deleted camera only animated picture frame %s", frame.getAbsoluteFile());
                    } else {
                        Timber.e("Failed deleting camera only animated picture frame %s", frame.getAbsoluteFile());
                    }
                }
            }
            return;
        }

        for (AnimatedPictureShot shot : shots) {
            final ArrayList<String> images = new ArrayList<>();
            for (File frame : shot.frames) {
                images.add(frame.getAbsolutePath());
            }

            final Intent intent = UploaderService.createAnimatedPictureIntent(
                    this,
                    vehicle.getVin(),
                    vehicle.getYear(),
                    vehicle.getMake(),
                    vehicle.getModel(),
                    vehicle.getProject(),
                    vehicle.getLibrary(),
                    shouldReplaceMedia(),
                    shot.video.getAbsolutePath(),
                    images);

            // Waiting for endpoint to be implemented on server.
            startService(intent);
        }
    }

    private void enqueueEZ360Picture(String pathname) {
        if (vehicle.isCameraOnly()) {
            if (new File(pathname).delete()) {
                Timber.i("Deleted camera only spinit video %s", pathname);
            } else {
                Timber.e("Failed deleting camera only spinit video %s", pathname);
            }

            return;
        }

        final Intent intent = UploaderService.createEZ360PictureIntent(
                this,
                vehicle.getVin(),
                vehicle.getYear(),
                vehicle.getMake(),
                vehicle.getModel(),
                vehicle.getProject(),
                vehicle.getLibrary(),
                shouldReplaceMedia(),
                pathname);
        startService(intent);
    }

    private void enqueueSpinitPictures(ArrayList<ImageDescription> images, boolean usedTurntable, int automatedPictureCount, int spinDuration, int extractedPictureCount) {
        if (vehicle.isCameraOnly()) {
            if (images != null) {
                for (ImageDescription shot : images) {
                    if (shot.image.delete()) {
                        Timber.i("Deleted camera only spinit picture %s", shot.image.getAbsolutePath());
                    } else {
                        Timber.e("Failed deleting camera only spinit picture %s", shot.image.getAbsolutePath());
                    }
                }
            }
            return;
        }

        if (images != null && images.size() > 0) {
            final Intent intent = UploaderService.createSpinItPicturesIntent(
                    this,
                    vehicle.getVin(),
                    vehicle.getYear(),
                    vehicle.getMake(),
                    vehicle.getModel(),
                    vehicle.getProject(),
                    vehicle.getLibrary(),
                    shouldReplaceMedia(),
                    usedTurntable,
                    images,
                    automatedPictureCount,
                    spinDuration,
                    extractedPictureCount);
            startService(intent);
        }
    }

    private void enqueueSpinitVideo(String videoPathname, boolean usedTurntable, int frameCount, int automatedPictureCount, int spinDuration, int extractedPictureCount, String cameraRole) {
        if (vehicle.isCameraOnly()) {
            if (new File(videoPathname).delete()) {
                Timber.i("Deleted camera only spinit video %s", videoPathname);
            } else {
                Timber.e("Failed deleting camera only spinit video %s", videoPathname);
            }

            return;
        }

        final Intent intent = UploaderService.createSpinItVideoIntent(
                this,
                vehicle.getVin(),
                vehicle.getYear(),
                vehicle.getMake(),
                vehicle.getModel(),
                vehicle.getProject(),
                vehicle.getLibrary(),
                shouldReplaceMedia(),
                usedTurntable,
                videoPathname,
                frameCount,
                automatedPictureCount,
                spinDuration,
                extractedPictureCount,
                cameraRole);
        startService(intent);
    }

    private void enqueueVideo(String videoPathname) {
        if (vehicle.isCameraOnly()) {
            final File videoFile = new File(videoPathname);
            if (videoFile.exists()) {
                if (videoFile.delete()) {
                    Timber.i("Deleted camera only video %s", videoPathname);
                } else {
                    Timber.e("Failed deleting camera only video %s", videoPathname);
                }
            }

            return;
        }

        final Intent intent = UploaderService.createFullMotionVideoIntent(
                VehicleActivity.this,
                vehicle.getVin(),
                vehicle.getYear(),
                vehicle.getMake(),
                vehicle.getModel(),
                vehicle.getProject(),
                vehicle.getLibrary(),
                shouldReplaceMedia(),
                videoPathname);
        startService(intent);
    }

    private void startCameraActivity(int requestCode) {
        final Intent intent = CameraActivity.createDetailPicturesIntent(this, vehicle.getVin());
        startActivityForResult(intent, requestCode);
    }

    private void startCameraActivity(int requestCode, int magazineID) {
        final Intent intent = CameraActivity.createDetailPicturesIntent(this, vehicle.getVin(), magazineID);
        startActivityForResult(intent, requestCode);
    }

    private MagazineData getMagazines() {
        MagazineData data = new MagazineData();
        data.magazines = new ArrayList<>();
        data.defaultSelection = -1;

        try {
            final PreparedQuery<Magazine> query = getHelper().getMagazineDao().queryBuilder()
                    .orderBy(Magazine.COLUMN_INDEX, true)
                    .where()
                    .eq(Magazine.COLUMN_PROJECT, vehicle.getProject())
                    .prepare();

            List<Magazine> results = getHelper().getMagazineDao().query(query);
            if (results == null) {
                return data;
            }

            for (Magazine magazine : results) {
                if (magazine.getYear().equalsIgnoreCase(vehicle.getYear())
                        && magazine.getMake().equalsIgnoreCase(vehicle.getMake())
                        && magazine.getModel().equalsIgnoreCase(vehicle.getModel())) {
                    data.magazines.add(0, magazine);
                    data.defaultSelection = 0;
                } else {
                    data.magazines.add(magazine);
                }
            }
        } catch (SQLException e) {
            Util.logError("Error querying magazines", e);
        }


        return data;
    }
}
