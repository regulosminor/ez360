package com.sister.ivana;

import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;

import java.lang.reflect.Array;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;

public class AddExteriorPoiPictureAdapter extends RecyclerView.Adapter<AddExteriorPoiPictureAdapter.Holder> {
    public interface AddExternalPoiPictureListener {
        void shotClicked(int position);
    }

    final private Picasso picasso;
    final int width;
    private final AddExternalPoiPictureListener listener;
    private ArrayList<String> thumbnailUrls;

    public AddExteriorPoiPictureAdapter(Picasso picasso, int width, ArrayList<String> thumbnailUrls, AddExternalPoiPictureListener listener) {
        this.picasso = picasso;
        this.width = width;
        this.listener = listener;
        this.thumbnailUrls = thumbnailUrls;
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.imageView) ImageView imageView;
        int position;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.imageView)
        public void onClick(View view) {
            listener.shotClicked(position);
        }
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cell_add_external_poi_select_picture, parent, false);

        ViewGroup.LayoutParams params = view.getLayoutParams();
        params.width = (int) (Math.ceil((float) width / 4));
        params.height = (int) (Math.ceil((double) params.width / 1.78));

        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        holder.position = position;
        final Uri uri = Uri.parse(thumbnailUrls.get(position));
        picasso.load(uri)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return thumbnailUrls.size();
    }
}
