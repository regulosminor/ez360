package com.sister.ivana.messaging;

/**
 * Data slave camera needs to upload media
 */

public class SlaveCaptureData {
    public String vin;
    public String year;
    public String make;
    public String model;
    public String project;
    public String library;
    public boolean replace;
}
