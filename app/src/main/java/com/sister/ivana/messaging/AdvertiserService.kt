package com.sister.ivana.messaging

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.net.wifi.WifiManager
import android.os.Build
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.text.format.Formatter
import com.google.gson.GsonBuilder
import com.j256.ormlite.android.apptools.OrmLiteBaseService
import com.sister.ivana.AppSharedPreferences
import com.sister.ivana.Util
import com.sister.ivana.database.DatabaseHelper
import timber.log.Timber
import java.io.IOException
import java.net.DatagramPacket
import java.net.DatagramSocket
import java.net.InetAddress
import java.util.concurrent.ScheduledThreadPoolExecutor
import java.util.concurrent.TimeUnit

class AdvertiserService : OrmLiteBaseService<DatabaseHelper>() {
    private var executor: ScheduledThreadPoolExecutor? = null
    private val ADVERTISING_INTERVAL_SECONDS = 15L

    private fun createMessage(preferences: AppSharedPreferences): AdvertisementMessage? {
        val manager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        val connectionInfo = manager?.connectionInfo
        if (connectionInfo == null) {
            return null
        }
        val address = Formatter.formatIpAddress(connectionInfo.ipAddress)
        return AdvertisementMessage(Util.getLibrary(helper), Util.getLogDeviceID(this), address)
    }

    private fun advertise() {
        val preferences = AppSharedPreferences(this)
        val message = createMessage(preferences)
        if (message == null) {
            Timber.e("Failed creating advertisement message")
            return
        }

        val gson = GsonBuilder().setPrettyPrinting().create()
        val payload = gson.toJson(message)

        val wifiManager = getApplicationContext().getSystemService(Context.WIFI_SERVICE) as WifiManager
        val dhcpInfo = wifiManager.dhcpInfo
        val broadcast = dhcpInfo.ipAddress or dhcpInfo.netmask.inv()

        val bytes = ByteArray(4)
        for (i in 0..3) {
            bytes[i] = (broadcast ushr(i * 8) and 0xff).toByte()
        }
        val broadcastAddress = InetAddress.getByAddress(bytes)
        val port = preferences.udpCommunicationsPort

        val datagram = DatagramPacket(payload.toByteArray(), payload.length, broadcastAddress, port);
        val socket = DatagramSocket()
        socket.use {
            try {
                socket.broadcast = true
                socket.send(datagram)
                Timber.i("Sent advertisement to %s:%d %s", broadcastAddress, port, payload)
            } catch (e: IOException) {
                Timber.e(e, "Exception sending advertisement: %s", e.localizedMessage)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.i("onDestroy")
        executor?.shutdown()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        if (Build.VERSION.SDK_INT >= 26) {
            val CHANNEL_ID = "my_channel_01"
            val channel = NotificationChannel(CHANNEL_ID,
                    "Background service running",
                    NotificationManager.IMPORTANCE_DEFAULT)

            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(channel)

            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("").build()

            startForeground(1, notification)
        }

        Timber.i("onStartCommand")

        if (executor == null) {
            Timber.i("scheduling advertisements")
            executor = ScheduledThreadPoolExecutor(1)
            executor?.scheduleWithFixedDelay({ advertise() }, 0, ADVERTISING_INTERVAL_SECONDS, TimeUnit.SECONDS)
        }

        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }
}
