package com.sister.ivana.messaging

class AdvertisementMessage {
    public val library: String
    public val deviceID: String
    public val address: String

    constructor(library: String, deviceID: String, address: String) {
        this.library = library
        this.deviceID = deviceID
        this.address = address
    }
}