package com.sister.ivana.messaging

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.support.v4.app.NotificationCompat
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.j256.ormlite.android.apptools.OpenHelperManager
import com.j256.ormlite.android.apptools.OrmLiteBaseService
import com.sister.ivana.*
import com.sister.ivana.database.DatabaseHelper
import com.sister.ivana.database.Peer
import com.sister.ivana.database.Vehicle
import com.squareup.otto.Subscribe
import net.sourceforge.opencamera.CameraActivity
import timber.log.Timber
import java.io.IOException
import java.net.InetAddress
import java.net.ServerSocket
import java.net.Socket
import java.sql.SQLException
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue

class NetworkService : OrmLiteBaseService<DatabaseHelper>() {
    public class PeerUpdate(val peer: Peer) {
    }

    var listenerThread: Thread? = null
    var isRunning = true
    var receivers = ConcurrentLinkedQueue<MessageReceiver>()
    var senders = ConcurrentHashMap<String, MessageSender>()
    var mainHandler: Handler? = null
    var serverSocket: ServerSocket? = null

    companion object {
        private val ACTION_SEND_MESSAGE = "ACTION_SEND_MESSAGE"
        private val EXTRA_MESSAGE_PAYLOAD = "EXTRA_MESSAGE_PAYLOAD"

        @JvmStatic fun sendMessage(context: Context, command: InterDeviceCommand) {
            val gson = GsonBuilder().setPrettyPrinting().create()
            val json = gson.toJson(command)
            val intent = Intent(context, NetworkService::class.java)
            intent.action = ACTION_SEND_MESSAGE
            intent.putExtra(EXTRA_MESSAGE_PAYLOAD, json)
            context.startService(intent);
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        Timber.i("onDestroy")
        Util.getBus().unregister(this)
        isRunning = false
        serverSocket?.close()
        for (receiver in receivers) {
            receiver.shutdown()
        }
        for ((_, sender) in senders) {
            sender.shutdown()
        }
    }

    override fun onCreate() {
        super.onCreate()
        Timber.i("onCreate")
        Util.getBus().register(this)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (Build.VERSION.SDK_INT >= 26) {
            val CHANNEL_ID = "my_channel_01"
            val channel = NotificationChannel(CHANNEL_ID,
                    "Background service running",
                    NotificationManager.IMPORTANCE_DEFAULT)

            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(channel)

            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentTitle("")
                    .setContentText("").build()

            startForeground(1, notification)
        }

        Timber.i("onStartCommand")

        if (mainHandler == null) {
            mainHandler = Handler(Looper.getMainLooper())
        }

        if (listenerThread == null) {
            Timber.i("creating listening thread")
            listenerThread = Thread({listen()})
            listenerThread!!.start()
        }

        if (intent != null && intent!!.action != null) {
            if (intent.action.contentEquals(ACTION_SEND_MESSAGE)) {
                val message = intent.getStringExtra(EXTRA_MESSAGE_PAYLOAD)
                sendMessage(message)
            }
        }

        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    @Subscribe
    fun addPeer(update: PeerUpdate) {
        // Try to connect to peer after receiving advertising packet
        getSender(update.peer)
    }

    private fun addVehicle(vin: String?, project: String?, library: String?, isCameraOnly: Boolean) {
        if (vin == null || project == null || library == null) {
            return
        }

        try {
            val libraryID = library.replace(",", "")
            val projectID = project.replace(",", "")

            var databaseHelper: DatabaseHelper? = null
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper::class.java)
            databaseHelper!!.vehicleDao.createIfNotExists(Vehicle(vin, projectID, libraryID, isCameraOnly))
            OpenHelperManager.releaseHelper()
        } catch (e: SQLException) {
            Timber.e(e, "Database error adding new VIN: %s", e.localizedMessage)
        }

    }
    private fun onPayloadReceived(jsonString: String) {
        val gson = Gson()
        val data = gson.fromJson(jsonString, InterDeviceCommand::class.java)

        Timber.i("Received payload: %s", data)

        val sharedPreferences = AppSharedPreferences(this)
        if (InterDeviceCommand.COMMAND_CHANGE_VIN == data.data.command) {
            if (sharedPreferences.isMasterSlaveEnabled && !sharedPreferences.isMaster) {
                // Slave cameras only respond to COMMAND_CHANGE_SLAVE_VIN. Otherwise they could try
                // to process both that message and COMMAND_CHANGE_VIN.
                return
            }

            // In addition to checking whether VIN sharing is enabled we check whether a download is active. This is to avoid having
            // the inventory download interrupted when the activity is recreated.
            if (sharedPreferences.isLoggedIn && sharedPreferences.acceptSharedInput() && !VinSearchActivity.isInventoryDownloadActive()) {

                addVehicle(data.data.vin, data.data.project, data.data.library, data.data.isCameraOnly)

                val environmentSettingsActivity = Intent(this, EnvironmentSettingsActivity::class.java)
                environmentSettingsActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP)
                val vinSearchActivity = Intent(this, VinSearchActivity::class.java)
                val vehicleActivity = VehicleActivity.createIntent(this, data.data.vin)

                android.app.TaskStackBuilder.create(this)
                        .addNextIntentWithParentStack(environmentSettingsActivity)
                        .addNextIntentWithParentStack(vinSearchActivity)
                        .addNextIntentWithParentStack(vehicleActivity)
                        .startActivities()
            }
        }
        if (InterDeviceCommand.COMMAND_CHANGE_SLAVE_VIN == data.data.command) {
            // In addition to checking whether VIN sharing is enabled we check whether a download is active. This is to avoid having
            // the inventory download interrupted when the activity is recreated.
            if (sharedPreferences.isLoggedIn && sharedPreferences.isMasterSlaveEnabled && !sharedPreferences.isMaster && !VinSearchActivity.isInventoryDownloadActive()) {
                addVehicle(data.data.vin, data.data.project, data.data.library, data.data.isCameraOnly)

                val cameraBlankActivity = Intent(this, CameraBlankActivity::class.java)
                val cameraActivity = CameraActivity.createSlavePictureIntent(this, data.data.vin)

                val taskStackBuilder = android.app.TaskStackBuilder.create(this)
                        .addNextIntentWithParentStack(cameraBlankActivity)
                        .addNextIntentWithParentStack(cameraActivity)
                taskStackBuilder.startActivities()
            }
        } else if (InterDeviceCommand.COMMAND_ACTIVATE_CAMERA == data.data.command) {
            if (sharedPreferences.isLoggedIn && !sharedPreferences.isMaster) {
                Handler(Looper.getMainLooper()).post {
                    // Event Bus must be accessed from the main thread
                    Util.getBus().post(data.slaveCaptureData)
                }
            }
        } else if (InterDeviceCommand.COMMAND_CANCEL_CAPTURE == data.data.command) {
            if (sharedPreferences.isLoggedIn && !sharedPreferences.isMaster) {
                Handler(Looper.getMainLooper()).post {
                    // Event Bus must be accessed from the main thread
                    Util.getBus().post(SlaveCancelCapture())
                }
            }
        } else if (InterDeviceCommand.COMMAND_DELETE_MEDIA == data.data.command) {
            if (sharedPreferences.isLoggedIn && !sharedPreferences.isMaster) {
                Handler(Looper.getMainLooper()).post {
                    // Event Bus must be accessed from the main thread
                    Util.getBus().post(SlaveCancelCapture())
                }
            }

        } else if (InterDeviceCommand.COMMAND_ENQUEUE_MEDIA == data.data.command) {
            if (sharedPreferences.isLoggedIn && !sharedPreferences.isMaster) {
                Handler(Looper.getMainLooper()).post {
                    // Event Bus must be accessed from the main thread
                    Util.getBus().post(SlaveEnqueueMedia())
                }
            }

        } else if (InterDeviceCommand.COMMAND_ENABLE_UPLOADS == data.data.command) {
            if (sharedPreferences.isLoggedIn && !sharedPreferences.isMaster) {
                sharedPreferences.putUploadsEnabled(true)
            }
        } else if (InterDeviceCommand.COMMAND_DISABLE_UPLOADS == data.data.command) {
            if (sharedPreferences.isLoggedIn && !sharedPreferences.isMaster) {
                sharedPreferences.putUploadsEnabled(false)
            }
        }
    }

    private fun listen() {
        val port = AppSharedPreferences(this).udpCommunicationsPort + 1
        while (isRunning) {
            try {
                if (serverSocket == null || serverSocket!!.isClosed) {
                    serverSocket = ServerSocket(port)
                }

                val socket = serverSocket!!.accept()

                Timber.i("Accepted connection from %s", socket.inetAddress?.hostAddress)
                val receiver = MessageReceiver(socket, {receivers.remove(it)}) { payload ->
                    mainHandler.run {
                        onPayloadReceived(payload)
                    }
                }
                receivers.add(receiver)
            } catch (e: IOException) {
                Timber.e(e, "Exception listening for connections: %s", e.localizedMessage)
                if (serverSocket != null) {
                    try {
                        serverSocket!!.close()
                        serverSocket = null
                    } catch (e: IOException) {
                    }
                }

                Thread.sleep(5000)
            }
        }

        if (serverSocket != null) {
            try {
                serverSocket!!.close()
            } catch (e: IOException) {

            }
        }

        Timber.i("Listening thread exiting")
    }

    @Synchronized fun getSender(peer: Peer): MessageSender {
        var sender = senders[peer.deviceID]
        if (sender == null) {
            val port = AppSharedPreferences(this).udpCommunicationsPort + 1
            sender = MessageSender(InetAddress.getByName(peer.address), port) {
                senders.remove(peer.deviceID)
            }
            senders[peer.deviceID] = sender
            Timber.i("Added sender for peer %s", peer)
        } else {
            Timber.i("Found existing sender for %s", peer)
        }
        return sender
    }

    private fun sendMessage(message: String) {
        val peers = helper.peerDao.queryForAll()
        for (peer in peers) {
            if (System.currentTimeMillis() - peer.lastAdvertisement > 15 * 60 * 1000) {
                Timber.i("removing stale peer: %s", peer)
                helper.peerDao.delete(peer)
                continue
            }

            Timber.i("Sending message to %s: %s", peer, message)
            getSender(peer).send(message)
        }
    }
}
