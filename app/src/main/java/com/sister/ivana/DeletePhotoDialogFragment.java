package com.sister.ivana;

import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sister.ivana.dialog.AppDialogFragment;

public class DeletePhotoDialogFragment extends AppDialogFragment {
    public interface DeletePhotoListener {
        void onDeletePhotoPositiveClick();

        void onDeletePhotoNegativeClick();
    }

    public static DeletePhotoDialogFragment newInstance() {
        DeletePhotoDialogFragment fragment = new DeletePhotoDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setMessage(args, "Delete photo?");
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_button_yes);
        AppDialogFragment.setNegativeButtonText(args, R.string.dialog_button_no);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        DeletePhotoListener listener = null;

        Fragment fragment = getTargetFragment();
        if (fragment != null && fragment instanceof DeletePhotoListener) {
            listener = (DeletePhotoListener) fragment;
        } else {
            final AppCompatActivity activity = (AppCompatActivity) getActivity();
            if (activity != null && activity instanceof DeletePhotoListener) {
                listener = (DeletePhotoListener) activity;
            }
        }

        if (listener != null) {
            if (i == DialogInterface.BUTTON_POSITIVE) {
                listener.onDeletePhotoPositiveClick();
            } else if (i == DialogInterface.BUTTON_NEGATIVE) {
                listener.onDeletePhotoNegativeClick();
            }
        }

        dismiss();
    }
}
