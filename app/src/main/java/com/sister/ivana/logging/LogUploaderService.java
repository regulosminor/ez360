package com.sister.ivana.logging;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.HandlerThread;

import com.sister.ivana.FileUtil;
import com.sister.ivana.Util;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.OrmLiteBaseJobService;

import java.io.File;

import timber.log.Timber;

public class LogUploaderService extends OrmLiteBaseJobService<DatabaseHelper> {
    private static final int UPLOAD_JOB_EXECUTION_INTERVAL = 4 * 60 * 60 * 1000;

    private static final int UPLOADER_AUTOMATIC_JOB_ID = 112;
    private static final int UPLOADER_MANUAL_JOB_ID = 113;
    private static final int UPLOADER_RETRY_JOB_ID = 114;

    private LogUploader logUploader;

    public static void initialize(Context context) {
        final JobInfo.Builder builder = new JobInfo.Builder(UPLOADER_AUTOMATIC_JOB_ID, new ComponentName(context, LogUploaderService.class));
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED);
        builder.setPersisted(true);
        builder.setPeriodic(UPLOAD_JOB_EXECUTION_INTERVAL);
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        scheduler.schedule(builder.build());
    }

    public static void startUpload(Context context) {
        // We don't require NETWORK_TYPE_UNMETERED here because this is executed as a result of
        // a user action and presumably they know what they want.
        final JobInfo.Builder builder = new JobInfo.Builder(UPLOADER_MANUAL_JOB_ID, new ComponentName(context, LogUploaderService.class));
        builder.setOverrideDeadline(5 * 1000);
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        scheduler.schedule(builder.build());
    }

    /**
     * Schedule a one time retry. This is called if we fail to upload one or more files. This
     * happens most frequently because the logging library is still in the process of
     * compressing an older file when we try to upload it so AWS fails. The compression usually
     * only takes a few seconds so we'll wait about 30 seconds and then try again.
     */
    public static void scheduleRetry(Context context) {
        final JobInfo.Builder builder = new JobInfo.Builder(UPLOADER_RETRY_JOB_ID, new ComponentName(context, LogUploaderService.class));
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED);
        builder.setMinimumLatency(30 * 1000);
        builder.setOverrideDeadline(60 * 1000);
        JobScheduler scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        scheduler.schedule(builder.build());
    }

    @Override
    public boolean onStartJob(final JobParameters jobParameters) {
        Timber.d("onStartJob entered");

        final File logDirectory = FileUtil.getLogDirectory();
        if (!logDirectory.exists()) {
            Timber.d("Log directory doesn't exist");
            return false;
        }

        final File[] files = logDirectory.listFiles();
        if (files == null || files.length == 0) {
            Timber.d("No log files to upload");
            return false;
        }

        final HandlerThread thread = new HandlerThread("LogUploaderThread" + +System.currentTimeMillis());
        thread.start();

        final boolean isUserRequested = jobParameters.getJobId() == UPLOADER_MANUAL_JOB_ID;
        logUploader = new LogUploader(this, thread.getLooper(), Util.getLibrary(getHelper()), isUserRequested, new LogUploader.LogUploaderListener() {
            @Override
            public void uploadsComplete(boolean success) {
                Timber.d("job finished");

                // If we encountered an error attempt one retry after a short wait. If that
                // doesn't work we'll just wait until the next upload cycle.
                if (!success && jobParameters.getJobId() != UPLOADER_RETRY_JOB_ID) {
                    scheduleRetry(LogUploaderService.this);
                }

                jobFinished(jobParameters, false);
            }
        });
        logUploader.start();

        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        Timber.d("stopping upload job");
        if (logUploader != null) {
            logUploader.stop();
            logUploader = null;
        }
        return false;
    }
}
