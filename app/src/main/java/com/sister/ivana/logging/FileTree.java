package com.sister.ivana.logging;

import android.os.Environment;
import android.util.Log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

import timber.log.Timber;

/**
 * Send Timber messages through logger so that we can write them to a file.
 */

public class FileTree extends Timber.DebugTree {
    // There is nothing magic about this number but we don't need to check the free space for
    // every write in normal conditions.
    private static final int FREE_SPACE_CHECK_INTERVAL = 1000;
    private static final long FREE_SPACE_THRESHOLD = 500L * 1024L * 1024L;

    private boolean haveFreeSpace;
    private int freeSpaceCheckCounter = FREE_SPACE_CHECK_INTERVAL;

    private static boolean isAboveThreshold() {
        boolean haveSpace = false;

        File externalStorageDir = Environment.getExternalStorageDirectory();
        if (externalStorageDir != null) {
            haveSpace = externalStorageDir.getFreeSpace()  > FREE_SPACE_THRESHOLD;
        }

        return haveSpace;
    }

    @Override
    protected void log(int priority, String tag, String message, Throwable t) {
        Logger logger = LoggerFactory.getLogger(ch.qos.logback.classic.Logger.ROOT_LOGGER_NAME);

        if (freeSpaceCheckCounter++ >= FREE_SPACE_CHECK_INTERVAL) {
            haveFreeSpace = isAboveThreshold();
            freeSpaceCheckCounter = 0;
        }

        if (!haveFreeSpace) {
            logger.warn("Logging paused due to lack of free space on external storage directory");
            return;
        }

        StringBuilder builder = new StringBuilder();

        if (tag != null && !tag.isEmpty()) {
            builder.append(tag);
            builder.append(":");
        }

        if (message != null && !message.isEmpty()) {
            if (builder.length() > 0) {
                builder.append(' ');
            }
            builder.append(message);
        }

        if (t != null) {
            if (builder.length() > 0) {
                builder.append(' ');
            }
            builder.append(t);
        }
        final String msg = builder.toString();

        switch (priority) {
            case Log.VERBOSE:
                logger.debug(msg);
                break;
            case Log.DEBUG:
                logger.debug(msg);
                break;
            case Log.INFO:
                logger.info(msg);
                break;
            case Log.WARN:
                logger.warn(msg);
                break;
            case Log.ERROR:
                logger.error(msg);
                break;
            default:
                logger.info(msg);
        }
    }
}
