package com.sister.ivana.logging;

import android.content.Context;

import com.crashlytics.android.Crashlytics;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.Util;
import com.sister.ivana.database.DatabaseHelper;

import io.fabric.sdk.android.Fabric;

/**
 * Created by csgulley on 11/19/17.
 */

public class CrashReporter {
    public static void setIdentifiers(Context context, DatabaseHelper helper) {
        Crashlytics.setUserIdentifier(new AppSharedPreferences(context).getCurrentUser());
        Crashlytics.setString("library", Util.getLibrary(helper));
        Crashlytics.setString("device", Util.getLogDeviceID(context));
    }

    public static void resetIdentifiers() {
        Crashlytics.setUserIdentifier("");
        Crashlytics.setString("library", "");
    }
}
