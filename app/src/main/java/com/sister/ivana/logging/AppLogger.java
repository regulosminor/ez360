package com.sister.ivana.logging;

import android.content.Context;
import android.content.SharedPreferences;

import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.BuildConfig;
import com.sister.ivana.FileUtil;

import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.android.LogcatAppender;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy;
import timber.log.Timber;

public class AppLogger {
    private static AppLogger logger = new AppLogger();
    private FileTree fileTree;
    private AppLogger() {}

    private final SharedPreferences.OnSharedPreferenceChangeListener preferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            if (s.contentEquals(AppSharedPreferences.KEY_IS_LOGGING_ENABLED)) {
                setLoggingEnabled(sharedPreferences.getBoolean(AppSharedPreferences.KEY_IS_LOGGING_ENABLED, false));
            }
        }
    };

    public static AppLogger getLogger() {
        return logger;
    }

    public void initialize(Context context) {
        if (BuildConfig.DEBUG) {
            // Send to LogCat
            Timber.plant(new Timber.DebugTree());
        }

        final AppSharedPreferences preferences = new AppSharedPreferences(context);
        preferences.registerOnSharedPreferenceChangeListener(preferenceListener);

        if (preferences.isLoggingEnabled()) {
            setLoggingEnabled(true);
        }
    }

    public void setLoggingEnabled(boolean enabled) {
        if (enabled && fileTree == null) {
            initializeLogback();
            fileTree = new FileTree();
            Timber.plant(fileTree);
        } else if (!enabled && fileTree != null) {
            Timber.uproot(fileTree);
            fileTree = null;
        }
    }

    private void initializeLogback() {
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        context.reset();

        Logger root = (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
        root.addAppender(getFileAppender(context));
    }

    private Appender<ILoggingEvent> getFileAppender(LoggerContext context) {
        PatternLayoutEncoder encoder = new PatternLayoutEncoder();
        encoder.setContext(context);
        encoder.setPattern("%d [%thread] %msg%n");
        encoder.start();

        RollingFileAppender<ILoggingEvent> appender = new RollingFileAppender<>();
        appender.setAppend(true);
        appender.setContext(context);
        appender.setFile(FileUtil.getLogFile("log.txt").getAbsolutePath());

        TimeBasedRollingPolicy<ILoggingEvent> policy = new TimeBasedRollingPolicy<>();
        policy.setFileNamePattern(FileUtil.getLogFile("log.%d.txt.zip").getAbsolutePath());
        policy.setMaxHistory(7);
        policy.setParent(appender);
        policy.setContext(context);
        policy.start();

        appender.setRollingPolicy(policy);
        appender.setEncoder(encoder);
        appender.start();

        return appender;
    }

    public static String getTransferObserverString(TransferObserver observer) {
        if (observer != null) {
            return String.format("TransferObserver[id:%d, state:%s, path:%s]", observer.getId(), observer.getState(), observer.getAbsoluteFilePath());
        } else {
            return "null";
        }
    }
}
