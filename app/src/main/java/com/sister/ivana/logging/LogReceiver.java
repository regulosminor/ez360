package com.sister.ivana.logging;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import timber.log.Timber;

/**
 * Allows components in other processes to send their logs to the logger in the main process.
 */
public class LogReceiver extends BroadcastReceiver {
    private static final String ACTION_LOG = "com.sister.ivana.LOG";
    private static final String EXTRA_MESSAGE = "com.sister.ivana.MESSAGE";
    private static final String EXTRA_TAG = "com.sister.ivana.TAG";
    private static final String EXTRA_LOG_LEVEL = "com.sister.ivana.LOG_LEVEL";
    private static final String EXTRA_EXCEPTION = "com.sister.ivana.EXCEPTION";

    public static void d(Context context, String tag, String message, Object... args) {
        log(context, Log.DEBUG, tag, null, message, args);
    }

    public static void i(Context context, String tag, String message, Object... args) {
        log(context, Log.INFO, tag, null, message, args);
    }

    public static void w(Context context, String tag, String message, Object... args) {
        log(context, Log.WARN, tag, null, message, args);
    }

    public static void e(Context context, String tag, String message, Object... args) {
        log(context, Log.ERROR, tag, null, message, args);
    }

    public static void e(Context context, String tag, Throwable t, String message, Object... args) {
        log(context, Log.ERROR, tag, t, message, args);
    }

    private static void log(Context context, int level, String tag, Throwable t, String message, Object... args) {
        final Intent intent = new Intent(ACTION_LOG);
        intent.putExtra(EXTRA_TAG, tag);
        intent.putExtra(EXTRA_MESSAGE, String.format(message, args));
        intent.putExtra(EXTRA_LOG_LEVEL, level);
        if (t != null) {
            intent.putExtra(EXTRA_EXCEPTION, t);
        }
        context.sendBroadcast(intent);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        final String action = intent.getAction();
        if (action != null && action.contentEquals(ACTION_LOG)) {
            Timber.tag(intent.getStringExtra(EXTRA_TAG));
            final int level = intent.getIntExtra(EXTRA_LOG_LEVEL, Log.ERROR);
            final String message = intent.getStringExtra(EXTRA_MESSAGE);
            final Throwable t = (Throwable) intent.getSerializableExtra(EXTRA_EXCEPTION);
            Timber.log(level, t, message);
        }
    }
}
