package com.sister.ivana.dialog;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;

import com.sister.ivana.R;

public class AppDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
    private static final String KEY_TITLE_ID = "KEY_TITLE_ID";
    private static final String KEY_TITLE_TEXT = "KEY_TITLE_TEXT";
    private static final String KEY_MESSAGE_ID = "KEY_MESSAGE_ID";
    private static final String KEY_MESSAGE_TEXT = "KEY_MESSAGE_TEXT";
    private static final String KEY_VIEW_ID = "KEY_VIEW_ID";
    private static final String KEY_POSITIVE_BUTTON_ID = "KEY_POSITIVE_BUTTON_ID";
    private static final String KEY_NEGATIVE_BUTTON_ID = "KEY_NEGATIVE_BUTTON_ID";

    public static void show(AppCompatActivity activity, @StringRes int titleID) {
        newInstance(titleID, 0).show(activity.getFragmentManager(), "app_alert");
    }

    public static void show(AppCompatActivity activity, String title) {
        newInstance(title).show(activity.getFragmentManager(), "app_alert");
    }

    public static void show(AppCompatActivity activity, @StringRes int titleID, @StringRes int messageID) {
        newInstance(titleID, messageID).show(activity.getFragmentManager(), "app_alert");
    }

    public static void show(AppCompatActivity activity, String title, String message) {
        newInstance(title, message).show(activity.getFragmentManager(), "app_alert");
    }

    private static AppDialogFragment newInstance(String title) {
        return newInstance(title, null);
    }

    private static AppDialogFragment newInstance(String title, String message) {
        AppDialogFragment fragment = newInstance();
        setTitle(fragment.getArguments(), title);
        if (message != null) {
            setMessage(fragment.getArguments(), message);
        }
        return fragment;
    }

    private static AppDialogFragment newInstance(int title, int message) {
        AppDialogFragment fragment = newInstance();
        setTitle(fragment.getArguments(), title);
        setMessage(fragment.getArguments(), message);
        return fragment;
    }

    private static AppDialogFragment newInstance() {
        AppDialogFragment fragment = new AppDialogFragment();
        Bundle args = new Bundle();
        setPositiveButtonText(args, R.string.dialog_ok);
        fragment.setArguments(args);
        return fragment;
    }

    protected static void setTitle(Bundle args, @StringRes int title) {
        args.putInt(KEY_TITLE_ID, title);
    }

    protected static void setTitle(Bundle args, String title) {
        args.putString(KEY_TITLE_TEXT, title);
    }

    protected static void setMessage(Bundle args, @StringRes int message) {
        args.putInt(KEY_MESSAGE_ID, message);
    }

    protected static void setMessage(Bundle args, String message) {
        args.putString(KEY_MESSAGE_TEXT, message);
    }

    protected static void setContentView(Bundle args, @LayoutRes int contentView) {
        args.putInt(KEY_VIEW_ID, contentView);
    }

    protected static void setPositiveButtonText(Bundle args, @StringRes int text) {
        args.putInt(KEY_POSITIVE_BUTTON_ID, text);
    }

    protected static void setNegativeButtonText(Bundle args, @StringRes int text) {
        args.putInt(KEY_NEGATIVE_BUTTON_ID, text);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        final int titleID = getArguments().getInt(KEY_TITLE_ID);
        final int messageID = getArguments().getInt(KEY_MESSAGE_ID);
        final String title = getArguments().getString(KEY_TITLE_TEXT);
        final String message = getArguments().getString(KEY_MESSAGE_TEXT, "");
        final int contentViewID = getArguments().getInt(KEY_VIEW_ID);
        final int positiveButtonID = getArguments().getInt(KEY_POSITIVE_BUTTON_ID);
        final int negativeButtonID = getArguments().getInt(KEY_NEGATIVE_BUTTON_ID);

        AppAlertDialog.Builder builder = new AppAlertDialog.Builder(getActivity());

        if (titleID > 0) {
            builder.setTitle(titleID);
        }

        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }

        if (messageID > 0) {
            builder.setMessage(messageID);
        }

        if (!TextUtils.isEmpty(message)) {
            builder.setMessage(message);
        }

        if (contentViewID > 0) {
            final LayoutInflater inflater = getActivity().getLayoutInflater();
            final View contentView = inflater.inflate(contentViewID, null);
            builder.setView(contentView);
        }

        if (positiveButtonID > 0) {
            builder.setPositiveButton(positiveButtonID, this);
        }

        if (negativeButtonID > 0) {
            builder.setNegativeButton(negativeButtonID, this);
        }

        return builder.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        dismiss();
    }
}
