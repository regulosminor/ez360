package com.sister.ivana.dialog;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sister.ivana.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class AppAlertDialog {
    public static class Builder {
        private final AlertParams params = new AlertParams();
        private final Activity activity;

        @BindView(R.id.title_text) TextView titleTextView;
        @BindView(R.id.message_text) TextView messageTextView;
        @BindView(R.id.positive_button) Button positiveButton;
        @BindView(R.id.negative_button) Button negativeButton;
        @BindView(R.id.button_layout) View buttonLayout;
        @BindView(R.id.button_spacer) View buttonSpacer;
        @BindView(R.id.custom_view_holder) ViewGroup customViewHolder;

        public Builder(Activity activity) {
            this.activity = activity;
        }

        public Builder setTitle(@StringRes int titleID) {
            params.title = activity.getText(titleID);
            return this;
        }

        public Builder setTitle(CharSequence title) {
            params.title = title;
            return this;
        }

        public Builder setMessage(@StringRes int messageID) {
            params.message = activity.getText(messageID);
            return this;
        }

        public Builder setMessage(CharSequence message) {
            params.message = message;
            return this;
        }

        public Builder setPositiveButton(@StringRes int text, final DialogInterface.OnClickListener listener) {
            params.positiveButtonText = activity.getText(text);
            params.positiveButtonListener = listener;
            return this;
        }

        public Builder setNegativeButton(@StringRes int text, final DialogInterface.OnClickListener listener) {
            params.negativeButtonText = activity.getText(text);
            params.negativeButtonListener = listener;
            return this;
        }

        public Builder setView(View view) {
            params.view = view;
            return this;
        }

        public Dialog create() {
            LayoutInflater inflater = activity.getLayoutInflater();
            final View view = inflater.inflate(R.layout.dialog_app_alert, null);
            final Unbinder unbinder = ButterKnife.bind(this, view);

            final Dialog dialog = new Dialog(activity, R.style.AppDialogTheme);

            if (!TextUtils.isEmpty(params.title)) {
                titleTextView.setText(params.title);
                titleTextView.setVisibility(View.VISIBLE);
            }

            if (!TextUtils.isEmpty(params.message)) {
                messageTextView.setText(params.message);
                messageTextView.setVisibility(View.VISIBLE);

                if (TextUtils.isEmpty(params.title)) {
                    // Remove extra spacing if there is not title.
                    ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) messageTextView.getLayoutParams();
                    params.topMargin = 0;
                    messageTextView.setLayoutParams(params);
                }
            }

            // Needed for messages with web links in them
            messageTextView.setMovementMethod(LinkMovementMethod.getInstance());

            if (!TextUtils.isEmpty(params.positiveButtonText)) {
                positiveButton.setText(params.positiveButtonText);
                positiveButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        params.positiveButtonListener.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
                    }
                });
                positiveButton.setVisibility(View.VISIBLE);
                buttonLayout.setVisibility(View.VISIBLE);
            }

            if (!TextUtils.isEmpty(params.negativeButtonText)) {
                negativeButton.setText(params.negativeButtonText);
                negativeButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        params.negativeButtonListener.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
                    }
                });
                negativeButton.setVisibility(View.VISIBLE);
                buttonLayout.setVisibility(View.VISIBLE);
            }

            if (positiveButton.getVisibility() == View.VISIBLE && negativeButton.getVisibility() == View.VISIBLE) {
                buttonSpacer.setVisibility(View.VISIBLE);
            }

            if (params.view != null) {
                customViewHolder.addView(params.view);
                customViewHolder.setVisibility(View.VISIBLE);
            }

            unbinder.unbind();
            dialog.setContentView(view);
            return dialog;
        }
    }

    private static class AlertParams {
        public CharSequence title;
        public CharSequence message;
        public CharSequence positiveButtonText;
        public CharSequence negativeButtonText;
        public View view;
        public DialogInterface.OnClickListener positiveButtonListener;
        public DialogInterface.OnClickListener negativeButtonListener;

    }
}
