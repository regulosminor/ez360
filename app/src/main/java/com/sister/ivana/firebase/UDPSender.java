package com.sister.ivana.firebase;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import android.content.Context;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sister.ivana.AppSharedPreferences;

public class UDPSender {

    public static final String BROADCAST_SEND_ADDRESS = "255.255.255.255";
    public static final String BROADCAST_LISTEN_ADDRESS = "0.0.0.0";

    public void sendMessage(Context context, InterDeviceCommand command) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonString = gson.toJson(command);

        final AppSharedPreferences sharedPreferences = new AppSharedPreferences(context);
        final int port = sharedPreferences.getUDPCommunicationsPort();

        AsyncTaskRunner async_cient = new AsyncTaskRunner();
        async_cient.execute(jsonString, Integer.toString(port));
    }

    private class AsyncTaskRunner extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... strings) {
            DatagramSocket ds = null;

            try {
                String message = strings[0];
                String port = strings[1];
                ds = new DatagramSocket();
                DatagramPacket dp;
                dp = new DatagramPacket(message.getBytes(), message.length(), InetAddress.getByName(BROADCAST_SEND_ADDRESS), Integer.parseInt(port));
                ds.setBroadcast(true);
                ds.send(dp);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (ds != null) {
                    ds.close();
                }
            }
            return null;
        }
    }
}
