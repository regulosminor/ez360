package com.sister.ivana.firebase;

public class InterDeviceCommand {

    public static final String COMMAND_CHANGE_VIN = "Change VIN";
    public static final String COMMAND_CHANGE_SLAVE_VIN = "Change Slave VIN";
    public static final String COMMAND_ACTIVATE_CAMERA = "Activate Camera";
    public static final String COMMAND_ENABLE_UPLOADS = "Enable Uploads";
    public static final String COMMAND_DISABLE_UPLOADS = "Disable Uploads";
    public static final String COMMAND_ENQUEUE_MEDIA = "Enqueue Media";

    public String to;
    public CommandData data;
    public SlaveCaptureData slaveCaptureData;

    public static class CommandData {

        public String command;
        public String vin;
        public String project;
        public String library;
        public String senderToken;
        public boolean isCameraOnly;

    }
}
