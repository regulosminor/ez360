package com.sister.ivana.settings.types;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sister.ivana.R;
import com.sister.ivana.settings.BaseSettingsAdapter;
import com.sister.ivana.settings.ItemSelectedListener;
import com.sister.ivana.settings.SettingsEnterNumberDialog;
import com.sister.ivana.settings.SettingsEnterTextDialog;

import java.util.Map;

public class TextEntryRowHolder extends BaseRowHolder {

    public static final int LAYOUT_ID = R.layout.setting_text;

    public final View view;
    public final TextView title;
    public final TextView subtitle;
    public final LinearLayout divider;

    public TextEntryRowHolder(View view, BaseSettingsAdapter adapter) {
        super(view, adapter);
        this.view = view;

        title = (TextView) view.findViewById(R.id.title);
        subtitle = (TextView) view.findViewById(R.id.subtitle);
        divider = (LinearLayout) view.findViewById(R.id.divider);

        this.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int dialogButtonId = R.string.dialog_button_ok;
                if (rowValue.item.get("dialog_button") != null) {
                    dialogButtonId = Integer.parseInt(rowValue.item.get("dialog_button"));
                }

                SettingsEnterTextDialog dialog = SettingsEnterTextDialog.newInstance(rowValue.item.get("dialog_title"), dialogButtonId);
                dialog.setListener(new ItemSelectedListener() {
                    @Override
                    public void itemSelected(String selectedItem) {
                        subtitle.setText(selectedItem);
                        rowValue.storeValue(selectedItem);
                        if (TextEntryRowHolder.this.adapter.getListener() != null) {
                            TextEntryRowHolder.this.adapter.getListener().itemChanged(rowValue.item);
                        }
                    }
                });
                dialog.show(TextEntryRowHolder.this.adapter.getActivity().getFragmentManager(), "theta_info");
            }
        });
    }

    @Override
    public void bind(Context context, final Map<String,String> item) {
        super.bind(context, item);
        rowValue = new TextEntryRowValue(item, adapter);

        String subtitleInt = (String) rowValue.retrieveValue();
        String subtitleString = subtitleInt.toString();
        if (TextUtils.isEmpty(subtitleString)) {
            subtitleString = item.get("subtitle");
        }

        title.setText(item.get("title"));
        subtitle.setText(subtitleString);

        String showDivider = item.get("divider");
        divider.setVisibility("false".equals(showDivider) ? View.INVISIBLE : View.VISIBLE);
    }

}
