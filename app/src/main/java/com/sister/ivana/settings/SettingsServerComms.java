package com.sister.ivana.settings;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.j256.ormlite.dao.Dao;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.Util;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.Project;
import com.sister.ivana.settings.types.NumberEntryRowHolder;
import com.sister.ivana.settings.types.NumberSelectRowHolder;
import com.sister.ivana.settings.types.SwitchRowHolder;
import com.sister.ivana.webapi.elasticsearch.ESWebService;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SettingsServerComms {

    public static final String DEFAULTS_KEY = "defaults";
    public static final String NAME_VALUE_PAIRS_KEY = "nameValuePairs";

    private Call<JsonObject> postSettingsRequest;
    private Call<JsonObject> getSettingsRequest;
    private Call<JsonObject> createSettingsRequest;
    private AppCompatActivity activity;
    private final DatabaseHelper databaseHelper;
    private Context context;

    public interface GetSettingsCompleteListener {
        public void onCompletion(boolean success);
    }

    public SettingsServerComms(AppCompatActivity activity, DatabaseHelper databaseHelper) {
        this.activity = activity;
        this.context = activity.getApplicationContext();
        this.databaseHelper = databaseHelper;
    }

    public boolean shouldPollSettings() {
        AppSharedPreferences sharedPreferences = new AppSharedPreferences(this.activity);
        long lastPoll = sharedPreferences.getLastSettingsPoll();
        if (System.currentTimeMillis() - lastPoll > (5 * 60 * 1000)) {
            return true;
        }
        return false;
    }

    public void startPostSettingsRequest() {
        JsonObject settingsJson = getSettingsJson();
        if (!settingsJson.has(DEFAULTS_KEY)) {
            AppSharedPreferences sharedPreferences = new AppSharedPreferences(this.activity);
            JsonObject serverJson = sharedPreferences.retrieveServerSettings();
            if (serverJson != null && serverJson.has(DEFAULTS_KEY)) {
                Object defaultJson = serverJson.get(DEFAULTS_KEY);
                if (defaultJson instanceof String) {
                    String defaultJsonString = (String)defaultJson;
                    defaultJson = new JsonParser().parse(defaultJsonString).getAsJsonObject();
                }
                if (((JsonObject) defaultJson).has(NAME_VALUE_PAIRS_KEY)) {
                    defaultJson = ((JsonObject)defaultJson).get(NAME_VALUE_PAIRS_KEY);
                }
                settingsJson.add(DEFAULTS_KEY, (JsonObject)defaultJson);
            } else {
                JsonObject defaultJson = getDefaultSettingsJson();
                settingsJson.add(DEFAULTS_KEY, defaultJson);
            }
        }
        postSettingsRequest = new ESWebService().putAppSettings(context, getProject(), settingsJson);
        postSettingsRequest.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                postSettingsRequest = null;

                final int httpStatus = response.code();
                if (httpStatus < 200 || httpStatus > 299) {
                    Util.logError("HTTP error posting app settings: " + httpStatus);
                    return;
                }

                final JsonObject results = response.body();
                if (results == null) {
                    Util.logError("No response from post app settings request");
                    return;
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                postSettingsRequest = null;
                Util.logError("Failed posting settings", t);
            }
        });
    }

    public void startGetSettingsRequest(final GetSettingsCompleteListener listener) {
        AppSharedPreferences sharedPreferences = new AppSharedPreferences(this.activity);
        sharedPreferences.updateLastSettingsPoll();

        getSettingsRequest = new ESWebService().getAppSettings(context, getProject());
        getSettingsRequest.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                getSettingsRequest = null;

                final int httpStatus = response.code();
                if (httpStatus < 200 || httpStatus > 299) {
                    if (listener != null) {
                        listener.onCompletion(false);
                    }
                    Util.logError("HTTP error getting app settings: " + httpStatus);
                    startCreateSettingsRequest();
                    return;
                }

                final JsonObject settingsJson = response.body();
                if (settingsJson == null) {
                    if (listener != null) {
                        listener.onCompletion(false);
                    }
                    Util.logError("No response from app settings request");
                    return;
                }

                if (!settingsJson.has(DEFAULTS_KEY)) {
                    JsonObject defaultsJson = getDefaultSettingsJson();
                    settingsJson.add(DEFAULTS_KEY, defaultsJson);
                }

                AppSharedPreferences preferences = new AppSharedPreferences(SettingsServerComms.this.activity);
                preferences.storeServerSettings(settingsJson);

                ArrayList<Map<String, String>> allSettings = getAllSettings();
                final JsonObject values = (JsonObject)settingsJson.get(NAME_VALUE_PAIRS_KEY);
                for (Map.Entry entry : values.entrySet()) {
                    String key = (String)entry.getKey();
                    Map<String, String> settingForKey = findSettingForKey(allSettings, key);
                    if (settingForKey != null) {
                        Object value = values.get(key);
                        if (value instanceof JsonPrimitive) {
                            final JsonPrimitive primitive = (JsonPrimitive)value;
                            if (primitive.isNumber() && isSettingInteger(settingForKey)) {
                                preferences.putIntKeyValue(key, primitive.getAsInt());
                            } else if (primitive.isBoolean() && isSettingBoolean(settingForKey)) {
                                preferences.putBoolKeyValue(key, primitive.getAsBoolean());
                            }
                        }
                    }
                }

                if (listener != null) {
                    listener.onCompletion(true);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                getSettingsRequest = null;
                if (listener != null) {
                    listener.onCompletion(false);
                }
                Util.logError("Failed getting settings", t);
            }
        });
    }

    public void startCreateSettingsRequest() {
        createSettingsRequest = new ESWebService().createAppSettings(context);
        createSettingsRequest.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                createSettingsRequest = null;

                final int httpStatus = response.code();
                if (httpStatus < 200 || httpStatus > 299) {
                    Util.logError("HTTP error creating app settings: " + httpStatus);
                } else {
                    // Now that the elasticsearch index has been created, create the document
                    startPostSettingsRequest();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                createSettingsRequest = null;
                Util.logError("Failed create settings", t);
            }
        });
    }

    private ArrayList<Map<String, String>> getAllSettings() {
        SettingsAdapter adapter = new SettingsAdapter(activity.getBaseContext(), activity, null);
        ArrayList<Map<String, String>> allSettings = adapter.getAllSettings();

        AdvancedSettingsAdapter advancedAdapter = new AdvancedSettingsAdapter(activity.getBaseContext(), activity, null);
        allSettings.addAll(advancedAdapter.getAllSettings());

        return allSettings;
    }

    private JsonObject getSettingsJson() {
        JsonObject jsonObject = new JsonObject();

        SettingsAdapter adapter = new SettingsAdapter(activity.getBaseContext(), activity, null);
        jsonObject = adapter.putSettingsAsJson(jsonObject);

        AdvancedSettingsAdapter advancedAdapter = new AdvancedSettingsAdapter(activity.getBaseContext(), activity, null);
        jsonObject = advancedAdapter.putSettingsAsJson(jsonObject);

        JsonObject rootJsonObject = new JsonObject();
        rootJsonObject.add(NAME_VALUE_PAIRS_KEY, jsonObject);

        return rootJsonObject;
    }

    private JsonObject getDefaultSettingsJson() {
        JsonObject jsonObject = new JsonObject();

        SettingsAdapter adapter = new SettingsAdapter(activity.getBaseContext(), activity, null);
        jsonObject = adapter.putDefaultsAsJson(jsonObject);

        AdvancedSettingsAdapter advancedAdapter = new AdvancedSettingsAdapter(activity.getBaseContext(), activity, null);
        jsonObject = advancedAdapter.putDefaultsAsJson(jsonObject);

        return jsonObject;
    }

    private Map<String, String> findSettingForKey(ArrayList<Map<String, String>> allSettings, String key) {
        if (key != null) {
            for (Map<String, String> setting : allSettings) {
                if (key.equals(setting.get("key"))) {
                    return setting;
                }
            }
        }
        return null;
    }

    private boolean isSettingBoolean(Map<String, String> setting) {
        return (SwitchRowHolder.LAYOUT_ID+"").equals(setting.get("layout"));
    }

    private boolean isSettingInteger(Map<String, String> setting) {
        return (NumberEntryRowHolder.LAYOUT_ID+"").equals(setting.get("layout")) ||
               (NumberSelectRowHolder.LAYOUT_ID+"").equals(setting.get("layout"));
    }

    private String getProject() {
        try {
            Dao<Project, ?> dao = databaseHelper.getProjectDao();
            for (Project project : dao) {
                return project.getProject();
            }
        } catch (SQLException e) {
            Util.logError("Failed reading projects from database", e);
        }
        return null;
    }
}
