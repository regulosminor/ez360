package com.sister.ivana.settings.types;

import android.content.res.Resources;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sister.ivana.settings.BaseSettingsAdapter;
import com.sister.ivana.settings.SettingsServerComms;

import java.util.Arrays;
import java.util.Map;

public class BaseRowValue {

    public Map<String,String> item;
    protected BaseSettingsAdapter adapter;

    public BaseRowValue(Map<String,String> item, BaseSettingsAdapter adapter) {
        this.item = item;
        this.adapter = adapter;
    }

    public Object retrieveValue() {
        return null;
    }

    public Object retrieveDefaultValue(JsonObject serverSettings) {
        return null;
    }

    public void storeValue(Object value) {
    }

    public Object getServerDefaultSetting(JsonObject serverSettings) {
        return null;
    }

    public JsonElement getServerDefaultJsonElement(JsonObject serverSettings) {
        String key = item.get("key");
        if (key != null && serverSettings != null) {
            JsonElement serverDefaultSettings = serverSettings.get(SettingsServerComms.DEFAULTS_KEY);
            if (serverDefaultSettings != null) {
                if (serverDefaultSettings.isJsonObject()) {
                    JsonObject jsonObject = serverDefaultSettings.getAsJsonObject();
                    JsonElement element = jsonObject.get(key);
                    return element;
                }
            }
        }
        return null;
    }

    public String[] getAllValues() {
        String[] values = null;
        String valuesId = item.get("values");
        if (valuesId != null) {
            Resources res = adapter.getContext().getResources();
            values = res.getStringArray(Integer.parseInt(valuesId));
        }
        return values;
    }

    public String convertDisplayValueToValue(String displayValue) {
        String value = null;

        String valuesDisplayId = item.get("values_display");
        if (valuesDisplayId != null) {
            Resources res = adapter.getContext().getResources();
            final String[] valuesDisplay = res.getStringArray(Integer.parseInt(valuesDisplayId));
            int valueDisplayIndex = Arrays.asList(valuesDisplay).indexOf(displayValue);

            String valuesId = item.get("values");
            if (valuesDisplayId != null) {
                final String[] values = res.getStringArray(Integer.parseInt(valuesId));
                if (values != null && valueDisplayIndex >= 0 && valueDisplayIndex < values.length) {
                    value = values[valueDisplayIndex];
                }
            }
        }

        return value;
    }
}
