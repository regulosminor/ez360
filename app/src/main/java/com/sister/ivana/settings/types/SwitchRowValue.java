package com.sister.ivana.settings.types;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sister.ivana.settings.BaseSettingsAdapter;

import java.util.Map;

public class SwitchRowValue extends BaseRowValue {

    public SwitchRowValue(Map<String,String> item, BaseSettingsAdapter adapter) {
        super(item, adapter);
    }

    @Override
    public Object retrieveValue() {
        String key = item.get("key");
        Boolean value = false;
        if (key != null) {
            JsonObject serverSettings = adapter.getSharedPreferences().retrieveServerSettings();
            value = adapter.getSharedPreferences().getBoolKeyValue(key, (Boolean) retrieveDefaultValue(serverSettings));
        }
        return value;
    }

    @Override
    public Object retrieveDefaultValue(JsonObject serverSettings) {
        Boolean defaultValue = false;
        String defaultDisplayValueString = item.get("default");
        Object defaultDisplayValueObject = getServerDefaultSetting(serverSettings);
        if (defaultDisplayValueObject != null && defaultDisplayValueObject instanceof String) {
            defaultDisplayValueString = (String) defaultDisplayValueObject;
        }
        if (defaultDisplayValueString != null) {
            String defaultValueString = convertDisplayValueToValue(defaultDisplayValueString);
            if (defaultValueString != null) {
                Boolean parsedDefaultValue = Boolean.parseBoolean(defaultValueString);
                if (parsedDefaultValue != null) {
                    defaultValue = parsedDefaultValue;
                }
            }
        }

        return defaultValue;
    }

    @Override
    public void storeValue(Object value) {
        String key = item.get("key");
        if (key != null) {
            adapter.getSharedPreferences().putBoolKeyValue(key, (Boolean)value);
        }
    }

    @Override
    public Object getServerDefaultSetting(JsonObject serverSettings) {
        JsonElement element = super.getServerDefaultJsonElement(serverSettings);
        if (element != null) {
            return element.getAsBoolean();
        }
        return null;
    }

}