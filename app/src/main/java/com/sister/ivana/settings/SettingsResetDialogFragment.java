package com.sister.ivana.settings;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sister.ivana.R;
import com.sister.ivana.dialog.AppDialogFragment;

public class SettingsResetDialogFragment extends AppDialogFragment {
    public interface SettingsResetListener {
        void onSettingsResetPositiveClick();

        void onSettingsResetNegativeClick();
    }

    private static final String KEY_VIN = "KEY_VIN";

    public static SettingsResetDialogFragment newInstance(AppCompatActivity activity) {
        SettingsResetDialogFragment fragment = new SettingsResetDialogFragment();
        Bundle args = new Bundle();
        final String text = activity.getResources().getString(R.string.settings_reset_all_confirmation);
        AppDialogFragment.setMessage(args, text);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_button_yes);
        AppDialogFragment.setNegativeButtonText(args, R.string.dialog_button_no);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        SettingsResetDialogFragment.SettingsResetListener listener = (SettingsResetDialogFragment.SettingsResetListener) getActivity();
        if (i == DialogInterface.BUTTON_POSITIVE) {
            listener.onSettingsResetPositiveClick();
            dismiss();
        } else if (i == DialogInterface.BUTTON_NEGATIVE) {
            listener.onSettingsResetNegativeClick();
            dismiss();
        }
        dismiss();
    }
}
