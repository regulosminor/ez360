package com.sister.ivana.settings.types;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sister.ivana.R;
import com.sister.ivana.dialog.AppDialogFragment;
import com.sister.ivana.logging.LogUploaderService;
import com.sister.ivana.settings.BaseSettingsAdapter;

import java.util.Map;

public class ButtonRowHolder extends BaseRowHolder {
    public static final int LAYOUT_ID = R.layout.setting_button;
    public final View view;
    public final TextView title;
    public final Button button;

    public ButtonRowHolder(View view, BaseSettingsAdapter adapter) {
        super(view, adapter);
        this.view = view;

        title = (TextView) view.findViewById(R.id.title);

        final Context context = view.getContext();
        final AppCompatActivity activity = (AppCompatActivity) context;
        button = (Button) view.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // This class is only used for one setting currently so the action and dialog
                // are hard coded. We can make it more general if we need to reuse it for
                // another setting.
                LogUploaderService.startUpload(context);
                AppDialogFragment.show(activity, "Started sending logs");
            }
        });
    }

    @Override
    public void bind(Context context, final Map<String, String> item) {
        super.bind(context, item);
        title.setText(item.get("title"));
        view.setBackgroundColor(Color.WHITE);
    }
}
