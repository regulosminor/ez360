package com.sister.ivana.settings.types;

import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sister.ivana.R;
import com.sister.ivana.settings.BaseSettingsAdapter;
import com.sister.ivana.settings.ItemSelectedListener;
import com.sister.ivana.settings.SettingsItemChooserDialog;

import java.util.Arrays;
import java.util.Map;

public class NumberSelectRowHolder extends BaseRowHolder {

    public static final int LAYOUT_ID = R.layout.setting_simple;

    public final View view;
    public final TextView title;
    public final TextView subtitle;
    public final LinearLayout divider;
    String currentValue = "";

    public NumberSelectRowHolder(View view, BaseSettingsAdapter adapter) {
        super(view, adapter);
        this.view = view;

        title = (TextView) view.findViewById(R.id.title);
        subtitle = (TextView) view.findViewById(R.id.subtitle);
        divider = (LinearLayout) view.findViewById(R.id.divider);

        this.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valuesDisplayId = rowValue.item.get("values_display");
                if (valuesDisplayId != null) {
                    Resources res = NumberSelectRowHolder.this.adapter.getContext().getResources();
                    final String[] valuesDisplay = res.getStringArray(Integer.parseInt(valuesDisplayId));

                    int dialogButtonId = R.string.dialog_button_ok;
                    if (rowValue.item.get("dialog_button") != null) {
                        dialogButtonId = Integer.parseInt(rowValue.item.get("dialog_button"));
                    }
                    SettingsItemChooserDialog dialog = SettingsItemChooserDialog.newInstance(valuesDisplay, getDefaultValue(), rowValue.item.get("dialog_title"), dialogButtonId);
                    dialog.setListener(new ItemSelectedListener() {
                        @Override
                        public void itemSelected(String selectedItem) {
                            if ("Reset Default".equals(selectedItem) && rowValue.item.get("default") != null) {
                                currentValue = rowValue.item.get("default");
                                subtitle.setText(currentValue);
                            } else {
                                currentValue = selectedItem;
                                subtitle.setText(currentValue);
                            }
                            int valueIndex = Arrays.asList(valuesDisplay).indexOf(subtitle.getText().toString());

                            String valuesId = rowValue.item.get("values");
                            if (valuesId != null) {
                                Resources res = NumberSelectRowHolder.this.adapter.getContext().getResources();
                                final String[] values = res.getStringArray(Integer.parseInt(valuesId));
                                rowValue.storeValue(Integer.parseInt(values[valueIndex]));
                            }

                            if (NumberSelectRowHolder.this.adapter.getListener() != null) {
                                NumberSelectRowHolder.this.adapter.getListener().itemChanged(rowValue.item);
                            }
                        }
                    });
                    dialog.show(NumberSelectRowHolder.this.adapter.getActivity().getFragmentManager(), "theta_info");
                }
            }
        });
    }

    String getDefaultValue() {
        return currentValue;
    }

    @Override
    public void bind(Context context, final Map<String,String> item) {
        super.bind(context, item);
        rowValue = new NumberSelectRowValue(item, adapter);

        String subtitleString = item.get("subtitle");

        Integer subtitleInt = (Integer) rowValue.retrieveValue();
        if (subtitleInt != null) {
            subtitleString = getDisplayValue(subtitleInt);
        }

        title.setText(item.get("title"));
        subtitle.setText(subtitleString);
        currentValue = subtitleString;

        String showDivider = item.get("divider");
        divider.setVisibility("false".equals(showDivider) ? View.INVISIBLE : View.VISIBLE);
    }

    private String getDisplayValue(int value) {
        String displayValueString = rowValue.item.get("subtitle");
        if (TextUtils.isEmpty(displayValueString)) {
            displayValueString = Integer.toString(value);
        }

        String valuesId = rowValue.item.get("values");
        if (valuesId != null) {
            Resources res = adapter.getContext().getResources();
            final String[] values = res.getStringArray(Integer.parseInt(valuesId));
            int valueIndex = Arrays.asList(values).indexOf(Integer.toString(value));

            String valuesDisplayId = rowValue.item.get("values_display");
            if (valuesDisplayId != null) {
                final String[] valuesDisplay = res.getStringArray(Integer.parseInt(valuesDisplayId));
                if (valuesDisplay != null && valueIndex >= 0 && valueIndex < valuesDisplay.length) {
                    displayValueString = valuesDisplay[valueIndex];
                }
            }
        }
        return displayValueString;
    }
}
