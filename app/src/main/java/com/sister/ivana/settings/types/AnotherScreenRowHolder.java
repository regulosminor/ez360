package com.sister.ivana.settings.types;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.sister.ivana.R;
import com.sister.ivana.settings.BaseSettingsAdapter;

import java.util.Map;

public class AnotherScreenRowHolder extends BaseRowHolder {

    public static final int LAYOUT_ID = R.layout.setting_another_screen;

    public final View view;
    public final TextView title;
    public final ImageView icon;

    public AnotherScreenRowHolder(View view, BaseSettingsAdapter adapter) {
        super(view, adapter);
        this.view = view;

        title = (TextView) view.findViewById(R.id.title);
        icon = (ImageView) view.findViewById(R.id.icon);

        this.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String classString = rowValue.item.get("class");
                if (classString != null) {
                    try {
                        Intent intent = new Intent(AnotherScreenRowHolder.this.adapter.getActivity(), Class.forName("com.sister.ivana.settings." + classString));
                        AnotherScreenRowHolder.this.adapter.getActivity().startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();;
                    }
                }
            }
        });
    }

    @Override
    public void bind(Context context, final Map<String,String> item) {
        super.bind(context, item);

        title.setText(item.get("title"));
        icon.setImageResource(Integer.parseInt(item.get("icon")));
    }
}
