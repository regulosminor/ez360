package com.sister.ivana.settings.types;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sister.ivana.settings.BaseSettingsAdapter;

import java.util.Map;

public class NumberSelectRowValue extends BaseRowValue {

    public NumberSelectRowValue(Map<String,String> item, BaseSettingsAdapter adapter) {
        super(item, adapter);
    }

    @Override
    public Object retrieveValue() {
        String key = item.get("key");
        Integer value = null;
        if (key != null) {
            JsonObject serverSettings = adapter.getSharedPreferences().retrieveServerSettings();
            value = adapter.getSharedPreferences().getIntKeyValue(key, (Integer) retrieveDefaultValue(serverSettings));
        }
        return value;
    }

    @Override
    public Object retrieveDefaultValue(JsonObject serverSettings) {
        Integer defaultValue = 0;
        String defaultDisplayValueString = item.get("default");
        Object defaultDisplayValueObject = getServerDefaultSetting(serverSettings);
        if (defaultDisplayValueObject != null && defaultDisplayValueObject instanceof String) {
            defaultDisplayValueString = (String) defaultDisplayValueObject;
        }
        if (defaultDisplayValueString != null) {
            String defaultValueString = convertDisplayValueToValue(defaultDisplayValueString);
            if (defaultValueString != null) {
                defaultValue = Integer.parseInt(defaultValueString);
            }
        }

        return defaultValue;
    }

    @Override
    public void storeValue(Object value) {
        String key = item.get("key");
        if (key != null) {
            if (value instanceof Double) {
                Double valueDouble = (Double)value;
                adapter.getSharedPreferences().putIntKeyValue(key, valueDouble.intValue());
            } else if (value instanceof Integer) {
                adapter.getSharedPreferences().putIntKeyValue(key, (Integer) value);
            }
        }
    }

    @Override
    public Object getServerDefaultSetting(JsonObject serverSettings) {
        JsonElement element = super.getServerDefaultJsonElement(serverSettings);
        if (element != null) {
            return element.getAsInt();
        }
        return null;
    }

}
