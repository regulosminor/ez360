package com.sister.ivana.settings;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.sister.ivana.ActionBarUploadStatus;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.R;
import com.sister.ivana.Util;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.OrmLiteBaseAppCompatActivity;

import java.util.Map;

public class AdvancedSettingsActivity extends OrmLiteBaseAppCompatActivity<DatabaseHelper> implements SettingsResetDialogFragment.SettingsResetListener {

    private AdvancedSettingsAdapter adapter;
    private ActionBarUploadStatus actionBarStatus;

    private final SettingsItemChangeListener listener = new SettingsItemChangeListener() {
        @Override
        public void itemChanged(Map<String,String> item) {
            String key = item.get("key");
            if (AppSharedPreferences.KEY_MASTER_SLAVE_ENABLED.equals(key) || AppSharedPreferences.KEY_DEVICE_ROLE.equals(key)) {
                adapter.setupSettings();

                // IllegalStateException if we try to notifyDataSetChanged while the RecyclerView is computing its layout.  Handler is the work-around.
                Handler handler = new Handler();
                final Runnable r = new Runnable() {
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                };
                handler.post(r);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_2);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView item_list = (RecyclerView) findViewById(R.id.item_list);
        item_list.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        item_list.setLayoutManager(linearLayoutManager);
        adapter = new AdvancedSettingsAdapter(this.getBaseContext(), this, listener);
        item_list.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        actionBarStatus = new ActionBarUploadStatus();
        actionBarStatus.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        actionBarStatus.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_vehicle_activity, menu);

        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        MenuItem enableUploads = menu.findItem(R.id.action_enable_upload);
        enableUploads.setVisible(!preferences.areUploadsEnabled());
        MenuItem disableUploads = menu.findItem(R.id.action_disable_upload);
        disableUploads.setVisible(preferences.areUploadsEnabled());

        MenuItem preview = menu.findItem(R.id.action_preview);
        preview.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final AppSharedPreferences preferences = new AppSharedPreferences(this);

        switch (item.getItemId()) {
            case R.id.action_logout:
                Util.logout(this);
                return true;

            case R.id.action_enable_upload:
                preferences.putUploadsEnabled(true);
                invalidateOptionsMenu();
                return true;

            case R.id.action_disable_upload:
                preferences.putUploadsEnabled(false);
                invalidateOptionsMenu();
                return true;

            case android.R.id.home:
                finish();
                return true;

            default:
                return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void onSettingsResetPositiveClick() {
        SettingsAdapter settingsAdapter = new SettingsAdapter(getBaseContext(), this, null);
        settingsAdapter.resetAllToDefaults();

        AdvancedSettingsAdapter advancedAdapter = new AdvancedSettingsAdapter(getBaseContext(), this, null);
        advancedAdapter.resetAllToDefaults();

        adapter.setupSettings();
        adapter.notifyDataSetChanged();
    }

    public void onSettingsResetNegativeClick() {
    }

}
