package com.sister.ivana.settings;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.sister.ivana.*;
import com.sister.ivana.settings.types.AnotherScreenRowHolder;
import com.sister.ivana.settings.types.HeaderRowHolder;
import com.sister.ivana.settings.types.NumberEntryRowHolder;
import com.sister.ivana.settings.types.NumberSelectRowHolder;
import com.sister.ivana.settings.types.SwitchRowHolder;

import java.util.ArrayList;
import java.util.Map;

public class SettingsAdapter extends BaseSettingsAdapter {

    public static final int ANIMATED_FRAMES_PER_SECOND_MIN = 5;
    public static final int ANIMATED_FRAMES_PER_SECOND_MAX = 30;
    public static final int ANIMATED_FRAMES_PER_SECOND_DEFAULT = 15;

    public SettingsAdapter(Context context, Activity activity, SettingsItemChangeListener listener) {
        super(context, activity, listener);

        setupSettings();
    }

    public void setupSettings() {
        settings.clear();
        addTurnTableSelectionSettings(settings);

        boolean hasTurntable = sharedPreferences.getBoolKeyValue(AppSharedPreferences.KEY_HAVE_TURN_TABLE, false);
        if (hasTurntable) {
            addTurnTableSettings(settings);
        } else {
            addNoTurnTableSettings(settings);
        }

        addOtherSettings(settings);
    }

    @Override
    public ArrayList<Map<String, String>> getAllSettings() {
        ArrayList<Map<String, String>> allSettings = new ArrayList<Map<String, String>>();
        addTurnTableSelectionSettings(allSettings);
        addTurnTableSettings(allSettings);
        addNoTurnTableSettings(allSettings);
        addOtherSettings(allSettings);
        return allSettings;
    }

    private void addTurnTableSelectionSettings(ArrayList<Map<String, String>> settingsParam) {
        settingsParam.add(createMap(new String[][] {
                {"title", "Spin method"},
                {"layout", HeaderRowHolder.LAYOUT_ID+""}
        }));

        boolean hasTurntable = sharedPreferences.getBoolKeyValue(AppSharedPreferences.KEY_HAVE_TURN_TABLE, false);
        settingsParam.add(createMap(new String[][] {
                {"key", AppSharedPreferences.KEY_HAVE_TURN_TABLE},
                {"title", "Do you have a turn table or AirBooth?"},
                {"subtitle", hasTurntable ? "Yes" : "No"},
                {"default", "Yes"},
                {"values", R.array.yes_no_values+""},
                {"values_display", R.array.yes_no_display+""},
                {"layout", SwitchRowHolder.LAYOUT_ID+""}
        }));
    }

    private void addTurnTableSettings(ArrayList<Map<String, String>> settingsParam) {
        settingsParam.add(createMap(new String[][] {
                {"key", AppSharedPreferences.KEY_LENGTH_OF_SPIN},
                {"title", "Length of Spin?"},
                {"subtitle", "60"},
                {"default", "60"},
                {"dialog_title", "Length of Spin?"},
                {"dialog_button", R.string.dialog_button_ok+""},
                {"layout", NumberEntryRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"key", AppSharedPreferences.KEY_HOW_MANY_SPINS},
                {"title", "How many spins?"},
                {"subtitle", "1"},
                {"default", "1"},
                {"values", R.array.how_many_spins_values+""},
                {"values_display", R.array.how_many_spins_display+""},
                {"dialog_title", "How many spins?"},
                {"dialog_button", R.string.dialog_button_ok+""},
                {"layout", NumberSelectRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"key", AppSharedPreferences.KEY_FRAMES_PER_SPIN},
                {"title", "How many frames per Spin?"},
                {"subtitle", "30 frames (recommended)"},
                {"default", "30 frames (recommended)"},
                {"values", R.array.frames_per_spin_values+""},
                {"values_display", R.array.frames_per_spin_display+""},
                {"dialog_title", "How many frames per Spin?"},
                {"dialog_button", R.string.dialog_button_ok+""},
                {"layout", NumberSelectRowHolder.LAYOUT_ID+""}
        }));
    }

    private void addNoTurnTableSettings(ArrayList<Map<String, String>> settingsParam) {
        settingsParam.add(createMap(new String[][] {
                {"key", AppSharedPreferences.KEY_IS_CUT_FROM_VIDEO},
                {"title", "Individual pictures or cut from video"},
                {"subtitle", "Pictures"},
                {"default", "Pictures"},
                {"values", R.array.is_cut_from_video_values+""},
                {"values_display", R.array.is_cut_from_video_display+""},
                {"layout", SwitchRowHolder.LAYOUT_ID+""}
        }));

        if (sharedPreferences.getBoolKeyValue(AppSharedPreferences.KEY_IS_CUT_FROM_VIDEO, false)) {
            settingsParam.add(createMap(new String[][]{
                    {"key", AppSharedPreferences.KEY_FRAMES_IN_SPIN_NO_TT},
                    {"title", "How many frames in the Spin?"},
                    {"subtitle", "10"},
                    {"default", "10"},
                    {"dialog_title", "How many frames in the Spin?"},
                    {"dialog_button", R.string.dialog_button_ok + ""},
                    {"layout", NumberEntryRowHolder.LAYOUT_ID + ""}
            }));
        } else {
            settingsParam.add(createMap(new String[][]{
                    {"key", AppSharedPreferences.KEY_SECONDS_BETWEEN_PICTURES},
                    {"title", "How many seconds between pictures?"},
                    {"subtitle", "2"},
                    {"default", "2"},
                    {"values", R.array.seconds_between_pictures_values + ""},
                    {"values_display", R.array.seconds_between_pictures_display + ""},
                    {"dialog_title", "How many seconds between pictures?"},
                    {"dialog_button", R.string.dialog_button_ok + ""},
                    {"layout", NumberSelectRowHolder.LAYOUT_ID + ""}
            }));
        }
    }

    private void addOtherSettings(ArrayList<Map<String, String>> settingsParam) {
        settingsParam.add(createMap(new String[][]{
                {"key", AppSharedPreferences.KEY_COUNTDOWN_TIMER},
                {"title", "Camera countdown duration."},
                {"subtitle", "5"},
                {"default", "5"},
                {"divider", "false"},
                {"values", R.array.countdown_timer_values + ""},
                {"values_display", R.array.countdown_timer_display + ""},
                {"dialog_title", "Camera countdown duration."},
                {"dialog_button", R.string.dialog_button_ok + ""},
                {"layout", NumberSelectRowHolder.LAYOUT_ID + ""}
        }));

        // Commented out so that we can release a version of the app without the animated
        // pictures feature
//        settingsParam.add(createMap(new String[][] {
//                {"title", "Animated Pictures"},
//                {"layout", HeaderRowHolder.LAYOUT_ID+""}
//        }));
//
//        boolean allowAnimatedPictures = sharedPreferences.allowAnimatedPictures();
//        settingsParam.add(createMap(new String[][] {
//                {"key", AppSharedPreferences.KEY_ALLOW_ANIMATED_PICTURES},
//                {"title", "Allow animated pictures?"},
//                {"subtitle", allowAnimatedPictures ? "Yes" : "No"},
//                {"default", "Yes"},
//                {"values", R.array.yes_no_values+""},
//                {"values_display", R.array.yes_no_display+""},
//                {"layout", SwitchRowHolder.LAYOUT_ID+""}
//        }));
//
//        settingsParam.add(createMap(new String[][] {
//                {"key", AppSharedPreferences.KEY_ANIMATED_FRAMES_PER_SECOND},
//                {"title", "How many frames per second?"},
//                {"subtitle", ANIMATED_FRAMES_PER_SECOND_DEFAULT + ""},
//                {"default", ANIMATED_FRAMES_PER_SECOND_DEFAULT + ""},
//                {"max", ANIMATED_FRAMES_PER_SECOND_MAX + ""},
//                {"min", ANIMATED_FRAMES_PER_SECOND_MIN + ""},
//                {"dialog_title", "How many frames per second?"},
//                {"dialog_button", R.string.dialog_button_ok+""},
//                {"layout", NumberEntryRowHolder.LAYOUT_ID+""}
//        }));

        settingsParam.add(createMap(new String[][] {
                {"title", "Automated External Pictures"},
                {"layout", HeaderRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"key", AppSharedPreferences.KEY_AUTOMATED_EXTERNAL_PICTURES},
                {"title", "How many External pictures (from the Spin)"},
                {"subtitle", "10"},
                {"default", "10"},
                {"divider", "false"},
                {"values", R.array.external_pictures_values+""},
                {"values_display", R.array.external_pictures_display+""},
                {"dialog_title", "How many External pictures (from the Spin)"},
                {"dialog_button", R.string.dialog_button_ok+""},
                {"layout", NumberSelectRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"title", ""},
                {"layout", HeaderRowHolder.LAYOUT_ID+""}
        }));

        final String userString = String.format("Logged in as %s", new AppSharedPreferences(activity).getCurrentUser());
        settingsParam.add(createMap(new String[][] {
                {"title", "User Info"},
                {"subtitle", userString},
                {"layout", NumberSelectRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"title", "Version"},
                {"subtitle", getVersion()},
                {"layout", NumberSelectRowHolder.LAYOUT_ID+""}
        }));

        settingsParam.add(createMap(new String[][] {
                {"title", "Advanced"},
                {"icon", R.drawable.ic_settings_white_48dp+""},
                {"class", "AdvancedSettingsActivity"},
                {"layout", AnotherScreenRowHolder.LAYOUT_ID+""}
        }));
    }

    private String getVersion() {
        String version = "Unknown version";
        try {
            PackageManager packageManager = activity.getPackageManager();
            PackageInfo packageInfo = packageManager.getPackageInfo(activity.getPackageName(), 0);
            version = String.format("Version %s", packageInfo.versionName);
        } catch (PackageManager.NameNotFoundException e) {
            Util.logError("Error reading version info", e);
        }

        return version;
    }
}
