package com.sister.ivana.settings;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.EditText;

import com.sister.ivana.R;
import com.sister.ivana.dialog.AppDialogFragment;

public class SettingsEnterTextDialog extends AppDialogFragment {

    private String selectedItem;
    private EditText editText;
    private ItemSelectedListener listener;

    public static SettingsEnterTextDialog newInstance(String title, int buttonTitle) {
        SettingsEnterTextDialog fragment = new SettingsEnterTextDialog();

        Bundle args = new Bundle();
        setTitle(args, title);
        setContentView(args, R.layout.dialog_enter_text);
        setPositiveButtonText(args, buttonTitle);
        fragment.setArguments(args);
        return fragment;
    }

    public void setListener(ItemSelectedListener listener) {
        this.listener = listener;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        editText = (EditText) dialog.findViewById(R.id.edit_text);

        return dialog;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (listener != null) {
            String value = editText.getText().toString();
            if (value != null && value.trim().length() > 0) {
                listener.itemSelected(value.trim());
            }
        }

        dismiss();
    }

}
