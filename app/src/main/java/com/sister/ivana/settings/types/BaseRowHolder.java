package com.sister.ivana.settings.types;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.sister.ivana.settings.BaseSettingsAdapter;

import java.util.Map;

public class BaseRowHolder extends RecyclerView.ViewHolder {

    protected BaseRowValue rowValue;
    protected BaseSettingsAdapter adapter;

    public BaseRowHolder(View view, BaseSettingsAdapter adapter) {
        super(view);
        this.adapter = adapter;
    }

    public void bind(Context context, final Map<String,String> item) {
        this.rowValue = new BaseRowValue(item, adapter);
    }

}
