package com.sister.ivana.settings;

import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import com.sister.ivana.ActionBarUploadStatus;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.CameraBlankActivity;
import com.sister.ivana.EnvironmentSettingsActivity;
import com.sister.ivana.LoginActivity;
import com.sister.ivana.R;
import com.sister.ivana.Util;
import com.sister.ivana.VinSearchActivity;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.OrmLiteBaseAppCompatActivity;

import java.util.Map;

public class SettingsActivity extends OrmLiteBaseAppCompatActivity<DatabaseHelper> {

    private SettingsAdapter adapter;
    private ActionBarUploadStatus actionBarStatus;
    private boolean initialIsSlave;

    private final SettingsItemChangeListener listener = new SettingsItemChangeListener() {
        @Override
        public void itemChanged(Map<String,String> item) {
            String key = item.get("key");
            if (AppSharedPreferences.KEY_ANIMATED_FRAMES_PER_SECOND.equals(key)) {
                final AppSharedPreferences preferences = new AppSharedPreferences(SettingsActivity.this);
                int defaultInt = 15;
                try {
                    defaultInt = Integer.parseInt(item.get("default"));
                } catch (NumberFormatException e) {
                    // use the initialized value
                }
                int maxInt = 30;
                try {
                    maxInt = Integer.parseInt(item.get("max"));
                } catch (NumberFormatException e) {
                    // use the initialized value
                }
                int minInt = 5;
                try {
                    minInt = Integer.parseInt(item.get("min"));
                } catch (NumberFormatException e) {
                    // use the initialized value
                }
                int enteredValue = preferences.getIntKeyValue(AppSharedPreferences.KEY_ANIMATED_FRAMES_PER_SECOND, defaultInt);
                if (enteredValue > maxInt) {
                    enteredValue = maxInt;
                } else if (enteredValue < minInt) {
                    enteredValue = minInt;
                }
                preferences.putIntKeyValue(AppSharedPreferences.KEY_ANIMATED_FRAMES_PER_SECOND, enteredValue);
            }
            if (AppSharedPreferences.KEY_HAVE_TURN_TABLE.equals(key) || AppSharedPreferences.KEY_IS_CUT_FROM_VIDEO.equals(key) || AppSharedPreferences.KEY_ANIMATED_FRAMES_PER_SECOND.equals(key)) {
                adapter.setupSettings();

                // IllegalStateException if we try to notifyDataSetChanged while the RecyclerView is computing its layout.  Handler is the work-around.
                Handler handler = new Handler();
                final Runnable r = new Runnable() {
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                };
                handler.post(r);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_2);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SettingsServerComms settingsServer = new SettingsServerComms(this, getHelper());
        settingsServer.startGetSettingsRequest(new SettingsServerComms.GetSettingsCompleteListener() {
            @Override
            public void onCompletion(boolean success) {
                if (success) {
                    runOnUiThread(new Runnable() {
                        public void run() {
                            adapter.setupSettings();
                            adapter.notifyDataSetChanged();
                        }
                    });
                }
            }
        });

        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        initialIsSlave = preferences.isMasterSlaveEnabled() && !preferences.isMaster();

        RecyclerView item_list = (RecyclerView) findViewById(R.id.item_list);
        item_list.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        item_list.setLayoutManager(linearLayoutManager);
        adapter = new SettingsAdapter(this.getBaseContext(), this, listener);
        item_list.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        actionBarStatus = new ActionBarUploadStatus();
        actionBarStatus.onStart(this);
    }

    @Override
    protected void onStop() {
        super.onStop();

        SettingsServerComms settingsServerComms = new SettingsServerComms(this, getHelper());
        settingsServerComms.startPostSettingsRequest();

        actionBarStatus.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_vehicle_activity, menu);

        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        MenuItem enableUploads = menu.findItem(R.id.action_enable_upload);
        enableUploads.setVisible(!preferences.areUploadsEnabled());
        MenuItem disableUploads = menu.findItem(R.id.action_disable_upload);
        disableUploads.setVisible(preferences.areUploadsEnabled());

        MenuItem preview = menu.findItem(R.id.action_preview);
        preview.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final AppSharedPreferences preferences = new AppSharedPreferences(this);

        switch (item.getItemId()) {
            case R.id.action_logout:
                Util.logout(this);
                return true;

            case R.id.action_enable_upload:
                preferences.putUploadsEnabled(true);
                invalidateOptionsMenu();
                return true;

            case R.id.action_disable_upload:
                preferences.putUploadsEnabled(false);
                invalidateOptionsMenu();
                return true;

            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return false;
        }
    }

    @Override public void onBackPressed() {
        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        final boolean isSlave = preferences.isMasterSlaveEnabled() && !preferences.isMaster();
        if (initialIsSlave != isSlave) {
            // The Master / Slave setting has changed, rebuild the back-stack accordingly.
            if (isSlave) {
                Intent cameraBlankActivity = new Intent(SettingsActivity.this, CameraBlankActivity.class);
                TaskStackBuilder taskStackBuilder = android.app.TaskStackBuilder.create(SettingsActivity.this)
                        .addNextIntentWithParentStack(cameraBlankActivity);
                taskStackBuilder.startActivities();
            } else  {
                Intent vinSearchIntent = new Intent(SettingsActivity.this, VinSearchActivity.class);
                vinSearchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                TaskStackBuilder taskStackBuilder = android.app.TaskStackBuilder.create(SettingsActivity.this)
                        .addNextIntentWithParentStack(vinSearchIntent);
                taskStackBuilder.startActivities();
            }
        } else {
            finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        adapter.setupSettings();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

}
