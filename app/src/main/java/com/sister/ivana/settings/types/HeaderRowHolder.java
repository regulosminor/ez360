package com.sister.ivana.settings.types;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.sister.ivana.R;
import com.sister.ivana.settings.BaseSettingsAdapter;

import java.util.Map;

public class HeaderRowHolder extends BaseRowHolder {

    public static final int LAYOUT_ID = R.layout.setting_header;

    public final View view;
    public final TextView title;

    public HeaderRowHolder(View view, BaseSettingsAdapter adapter) {
        super(view, adapter);
        this.view = view;

        title = (TextView) view.findViewById(R.id.title);
    }

    @Override
    public void bind(Context context, final Map<String,String> item) {
        super.bind(context, item);

        title.setText(item.get("title"));
    }
}
