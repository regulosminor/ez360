package com.sister.ivana.settings;

import java.util.Map;

public abstract class SettingsItemChangeListener {
    public abstract void itemChanged(Map<String,String> item);
}
