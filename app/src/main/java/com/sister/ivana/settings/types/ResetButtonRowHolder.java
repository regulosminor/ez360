package com.sister.ivana.settings.types;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.sister.ivana.R;
import com.sister.ivana.settings.BaseSettingsAdapter;
import com.sister.ivana.settings.SettingsResetDialogFragment;

import java.util.Map;

public class ResetButtonRowHolder extends BaseRowHolder {

    public static final int LAYOUT_ID = R.layout.setting_reset;

    public final View view;
    public final Button button;

    public ResetButtonRowHolder(View view, BaseSettingsAdapter adapter) {
        super(view, adapter);
        this.view = view;
        final Context context = ResetButtonRowHolder.this.view.getContext();
        final AppCompatActivity activity = (AppCompatActivity)context;

        button = (Button) view.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SettingsResetDialogFragment dialog = SettingsResetDialogFragment.newInstance(activity);
                dialog.show(activity.getFragmentManager(), "create");
            }
        });

    }

    @Override
    public void bind(Context context, final Map<String,String> item) {
        super.bind(context, item);
    }
}
