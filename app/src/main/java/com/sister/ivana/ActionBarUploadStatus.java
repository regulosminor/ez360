package com.sister.ivana;


import android.content.SharedPreferences;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sister.ivana.upload.UploadStatus;
import com.sister.ivana.upload.UploaderService;
import com.squareup.otto.Subscribe;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActionBarUploadStatus {
    @BindView(R.id.title_text) TextView titleTextView;
    @BindView(R.id.detail_text) TextView detailTextView;
    @BindView(R.id.upload_progress) ProgressBar uploadProgressBar;

    private Handler handler;
    private Runnable retryMessage;
    private AppSharedPreferences preferences;
    private AppCompatActivity activity;
    final private SharedPreferences.OnSharedPreferenceChangeListener preferenceListener = new SharedPreferences.OnSharedPreferenceChangeListener() {
        @Override
        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
            onPreferencesChanged();
        }
    };

    /*
     * Should be called from client's onStart
     */
    public void onStart(AppCompatActivity activity) {
        this.activity = activity;
        handler = new Handler();
        final ActionBar actionBar = this.activity.getSupportActionBar();
        //noinspection ConstantConditions
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setCustomView(R.layout.action_bar_upload_status);
        ButterKnife.bind(this, actionBar.getCustomView());

        Toolbar toolbar = (Toolbar) actionBar.getCustomView().getParent();
        if (toolbar != null) {
            // Remove large space between up arrow and logo
            toolbar.setContentInsetStartWithNavigation(0);
        }

        Util.getBus().register(this);
        preferences = new AppSharedPreferences(activity);
        preferences.registerOnSharedPreferenceChangeListener(preferenceListener);
        update(UploadStatus.getStatus(activity));
    }

    /*
     * Should be called from client's onStop
     */
    public void onStop() {
        Util.getBus().unregister(this);
        cancelErrorMessage();
        preferences.unregisterOnSharedPreferenceChangeListener(preferenceListener);
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onUpdateEvent(UploadStatus uploadStatus) {
        update(uploadStatus);
    }

    private void update(UploadStatus status) {
        if (!preferences.areUploadsEnabled()) {
            titleTextView.setText(getPendingVehiclesDescription(status.getPendingVehicleCount()));
            titleTextView.setVisibility(View.VISIBLE);
            setDetailText("Uploads Disabled");
            uploadProgressBar.setVisibility(View.GONE);
            return;
        }

        final UploadStatus.Upload upload = status.getCurrentUpload();
        if (upload == null) {
            titleTextView.setVisibility(View.GONE);
            uploadProgressBar.setVisibility(View.GONE);

            final int count = status.getPendingVehicleCount();
            titleTextView.setText(getPendingVehiclesDescription(status.getPendingVehicleCount()));
            titleTextView.setVisibility(View.VISIBLE);

            if (!new Network(activity).haveWiFi()) {
                setDetailText("No Wi-Fi");
            } else if (count > 0) {
                UploadStatus.VideoCompressionOperation operation = status.getCurrentVideoCompressionOperation();
                if (operation != null) {
                    // We are waiting for a compression operation to complete before uploading.
                    showCompressingVideoStatus(operation);
                    return;
                }

                UploadStatus.FrameExtractionOperation extractionOperation = status.getCurrentFrameExtractionOperation();
                if (extractionOperation != null) {
                    showExtractingFramesStatus(extractionOperation);
                    return;
                }

                // This isn't always an error. It takes several steps to upload some vehicles such
                // as extraction, compression and downloading. Between these steps there are
                // brief periods when we have cards to download but are doing nothing. To avoid
                // flashing error messages all of the time we wait a small amount of time
                // before displaying something.
                displayErrorMessage();
            } else {
                cancelErrorMessage();
                detailTextView.setVisibility(View.GONE);
            }

            uploadProgressBar.setVisibility(View.GONE);
            return;
        }

        switch (upload.getState()) {
            case PROCESSING_MEDIA:
                showProcessingStatus(upload);
                break;

            case UPLOADING_MEDIA:
                showUploadingStatus(upload);
                break;
        }
    }

    private String getTitle(UploadStatus.VehicleOperation upload) {
        StringBuilder builder = new StringBuilder("Now uploading:");
        final String year = upload.getYear();
        final String make = upload.getMake();
        final String model = upload.getModel();

        if (!TextUtils.isEmpty(year)) {
            builder.append(' ');
            builder.append(year);
        }
        if (!TextUtils.isEmpty(make)) {
            builder.append(' ');
            builder.append(make);
        }
        if (!TextUtils.isEmpty(model)) {
            builder.append(' ');
            builder.append(model);
        }

        return builder.toString();
    }

    private String getPendingVehiclesDescription(int count) {
        final String message;
        if (count == 0) {
            message = "All vehicles have been uploaded";
        } else if (count == 1) {
            message = String.format("There is %d vehicle ready for upload", count);
        } else {
            message = String.format("There are %d vehicles ready for upload", count);
        }

        return message;
    }

    private String getProgressDescription(UploadStatus.Upload upload) {
        return String.format("%s of %s %s", upload.getCompletedFiles() + 1, upload.getTotalFiles(), upload.getDisplayUnits());
    }

    private void showUploadingStatus(UploadStatus.Upload upload) {
        titleTextView.setText(getTitle(upload));
        setDetailText(getProgressDescription(upload));
        uploadProgressBar.setIndeterminate(false);
        uploadProgressBar.setProgress(upload.getCurrentProgress());
        setVisibile();
    }

    private void showProcessingStatus(UploadStatus.Upload upload) {
        titleTextView.setText(getTitle(upload));
        setDetailText("Sending to SiSTeR");
        uploadProgressBar.setIndeterminate(true);

        setVisibile();
    }

    private void showCompressingVideoStatus(UploadStatus.VideoCompressionOperation operation) {
        titleTextView.setText(getTitle(operation));
        setDetailText("Compressing video");
        uploadProgressBar.setIndeterminate(true);

        setVisibile();
    }

    private void showExtractingFramesStatus(UploadStatus.FrameExtractionOperation operation) {
        titleTextView.setText(getTitle(operation));
        setDetailText("Extracting frames");
        uploadProgressBar.setIndeterminate(true);

        setVisibile();
    }

    private void setVisibile() {
        titleTextView.setVisibility(View.VISIBLE);
        detailTextView.setVisibility(View.VISIBLE);
        uploadProgressBar.setVisibility(View.VISIBLE);
    }

    private void onPreferencesChanged() {
        activity.invalidateOptionsMenu();
        update(UploadStatus.getStatus(activity));

        if (preferences.areUploadsEnabled()) {
            // Give uploader a chance to run
            activity.startService(UploaderService.createScheduleJobIntent(activity));
        }
    }

    private void setDetailText(String message) {
        cancelErrorMessage();
        detailTextView.setText(message);
        detailTextView.setVisibility(View.VISIBLE);
    }

    private void displayErrorMessage() {
        if (retryMessage == null) {
            retryMessage = new Runnable() {
                @Override
                public void run() {
                    detailTextView.setText("Waiting to retry some uploads");
                    detailTextView.setVisibility(View.VISIBLE);
                }
            };
            // This is some delay between the various steps of processing and uploading a vehicle.
            // If we display the error message immediately it can flash on and off during these
            // normal delays. To avoid this only display the retry message after a minimum amount
            // of time has passed with no updates.
            detailTextView.setVisibility(View.INVISIBLE);
            handler.postDelayed(retryMessage, 5000);
        }
    }

    private void cancelErrorMessage() {
        if (retryMessage != null) {
            handler.removeCallbacks(retryMessage);
            retryMessage = null;
        }
    }
}
