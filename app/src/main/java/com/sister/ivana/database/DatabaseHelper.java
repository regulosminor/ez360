package com.sister.ivana.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.sister.ivana.Util;

import java.sql.SQLException;

import timber.log.Timber;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    private static final String DATABASE_NAME = "ivana.db";
    private static final int DATABASE_VERSION = 8;

    private Dao<Project, Integer> projectDao;
    private Dao<Vehicle, String> vehicleDao;
    private Dao<UploadGroup, String> uploadGroupDao;
    private Dao<UploadFile, String> uploadFileDao;
    private Dao<Magazine, Integer> magazineDao;
    private Dao<MagazineShot, Integer> magazineShotDao;
    private Dao<Peer, String> peerDao;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Project.class);
            TableUtils.createTable(connectionSource, Vehicle.class);
            TableUtils.createTable(connectionSource, UploadGroup.class);
            TableUtils.createTable(connectionSource, UploadFile.class);
            TableUtils.createTable(connectionSource, Magazine.class);
            TableUtils.createTable(connectionSource, MagazineShot.class);
            TableUtils.createTable(connectionSource, Peer.class);
        } catch (SQLException e) {
            Util.logError("Error creating database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        if (oldVersion < 2) {
            try {
                final Dao<UploadGroup, String> dao = getDao(UploadGroup.class);
                dao.executeRaw("ALTER TABLE `uploadGroup` ADD COLUMN sourceVideoPathname VARCHAR NOT NULL DEFAULT '';");
                dao.executeRaw("ALTER TABLE `uploadGroup` ADD COLUMN frameCount INTEGER NOT NULL DEFAULT 0;");
                dao.executeRaw("ALTER TABLE `uploadGroup` ADD COLUMN extractionRequestCode BIGINT NOT NULL DEFAULT 0;");
                dao.executeRaw("ALTER TABLE `uploadGroup` ADD COLUMN extractionRequestTimestamp BIGINT NOT NULL DEFAULT 0;");
            } catch (SQLException e) {
                Timber.e(e, "Error upgrading database from %d to %d", oldVersion, newVersion);
            }
        }
        if (oldVersion < 3) {
            try {
                final Dao<UploadGroup, String> dao = getDao(UploadGroup.class);
                dao.executeRaw("ALTER TABLE `vehicle` ADD COLUMN isCameraOnly SMALLINT NOT NULL DEFAULT 0;");
            } catch (SQLException e) {
                Timber.e(e, "Error upgrading database from %d to %d", oldVersion, newVersion);
            }
        }
        if (oldVersion < 4) {
            try {
                final Dao<UploadGroup, String> dao = getDao(UploadGroup.class);
                dao.executeRaw("ALTER TABLE `uploadGroup` ADD COLUMN automatedPictureCount INTEGER NOT NULL DEFAULT 0;");
                dao.executeRaw("ALTER TABLE `uploadGroup` ADD COLUMN spinDuration INTEGER NOT NULL DEFAULT 0;");
                dao.executeRaw("ALTER TABLE `uploadGroup` ADD COLUMN extractedPictureCount INTEGER NOT NULL DEFAULT 0;");
            } catch (SQLException e) {
                Timber.e(e, "Error upgrading database from %d to %d", oldVersion, newVersion);
            }
        }
        if (oldVersion < 5) {
            try {
                final Dao<UploadGroup, String> dao = getDao(UploadGroup.class);
                dao.executeRaw("ALTER TABLE `uploadGroup` ADD COLUMN cameraRole VARCHAR NOT NULL DEFAULT '';");
            } catch (SQLException e) {
                Timber.e(e, "Error upgrading database from %d to %d", oldVersion, newVersion);
            }
        }
        if (oldVersion < 6) {
            try {
                TableUtils.createTable(connectionSource, Peer.class);
            } catch (SQLException e) {
                Timber.e(e, "Error upgrading database from %d to %d", oldVersion, newVersion);
            }
        }
        if (oldVersion < 7) {
            try {
                final Dao<UploadGroup, String> dao = getDao(UploadGroup.class);
                dao.executeRaw("ALTER TABLE `uploadFile` ADD COLUMN uploadSubGroupID INTEGER NOT NULL DEFAULT 0;");
            } catch (SQLException e) {
                Timber.e(e, "Error upgrading database from %d to %d", oldVersion, newVersion);
            }
        }
        if (oldVersion < 8) {
            try {
                final Dao<UploadGroup, String> dao = getDao(UploadGroup.class);
                dao.executeRaw("ALTER TABLE `uploadFile` ADD COLUMN spinNumber INTEGER NOT NULL DEFAULT 0;");
                dao.executeRaw("ALTER TABLE `uploadFile` ADD COLUMN spinIndex INTEGER NOT NULL DEFAULT 0;");
                dao.executeRaw("ALTER TABLE `uploadFile` ADD COLUMN addToMagazine TINYINT(1) NOT NULL DEFAULT 0;");
                dao.executeRaw("ALTER TABLE `uploadFile` ADD COLUMN title VARCHAR NOT NULL DEFAULT '';");
                dao.executeRaw("ALTER TABLE `uploadFile` ADD COLUMN description VARCHAR NOT NULL DEFAULT '';");
                dao.executeRaw("ALTER TABLE `uploadFile` ADD COLUMN x DOUBLE PRECISION NOT NULL DEFAULT 0;");
                dao.executeRaw("ALTER TABLE `uploadFile` ADD COLUMN y DOUBLE PRECISION NOT NULL DEFAULT 0;");
            } catch (SQLException e) {
                Timber.e(e, "Error upgrading database from %d to %d", oldVersion, newVersion);
            }
        }

    }

    public Dao<Project, Integer> getProjectDao() throws SQLException {
        if (projectDao == null) {
            projectDao = getDao(Project.class);
        }
        return projectDao;
    }

    public Dao<Vehicle, String> getVehicleDao() throws SQLException {
        if (vehicleDao == null) {
            vehicleDao = getDao(Vehicle.class);
        }
        return vehicleDao;
    }

    public Dao<UploadGroup, String> getUploadGroupDao() throws SQLException {
        if (uploadGroupDao == null) {
            uploadGroupDao = getDao(UploadGroup.class);
        }
        return uploadGroupDao;
    }

    public Dao<UploadFile, String> getUploadFileDao() throws SQLException {
        if (uploadFileDao == null) {
            uploadFileDao = getDao(UploadFile.class);
        }
        return uploadFileDao;
    }

    public Dao<Magazine, Integer> getMagazineDao() throws SQLException {
        if (magazineDao == null) {
            magazineDao = getDao(Magazine.class);
        }
        return magazineDao;
    }

    public Dao<MagazineShot, Integer> getMagazineShotDao() throws SQLException {
        if (magazineShotDao == null) {
            magazineShotDao = getDao(MagazineShot.class);
        }
        return magazineShotDao;
    }

    public Dao<Peer, String> getPeerDao() throws SQLException {
        if (peerDao == null) {
            peerDao = getDao(Peer.class);
        }
        return peerDao;
    }

    public void clearTables() {
        try {
            TableUtils.clearTable(connectionSource, Project.class);
            TableUtils.clearTable(connectionSource, Vehicle.class);
            TableUtils.clearTable(connectionSource, UploadGroup.class);
            TableUtils.clearTable(connectionSource, UploadFile.class);
            TableUtils.clearTable(connectionSource, Magazine.class);
            TableUtils.clearTable(connectionSource, MagazineShot.class);
            TableUtils.clearTable(connectionSource, Peer.class);
        } catch (SQLException e) {
            Util.logError("Error clearing database tables", e);
        }

    }
}
