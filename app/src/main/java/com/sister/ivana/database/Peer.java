package com.sister.ivana.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "peer")
public class Peer {
    public static final String COLUMN_DEVICE_ID = "deviceID";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_LAST_ADVERTISEMENT = "lastAdvertisement";

    @DatabaseField(id = true, canBeNull = false, columnName = COLUMN_DEVICE_ID, defaultValue = "")
    private String deviceID;

    @DatabaseField(canBeNull = false, columnName = COLUMN_ADDRESS, defaultValue = "")
    private String address;

    @DatabaseField(canBeNull = false, columnName = COLUMN_LAST_ADVERTISEMENT)
    private long lastAdvertisement;

    public Peer() {
    }

    @Override
    public String toString() {
        return String.format("%s:%s", address, deviceID);
    }

    public Peer(String deviceID, String address, long lastAdvertisement) {
        this.deviceID = deviceID;
        this.address = address;
        this.lastAdvertisement = lastAdvertisement;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public String getAddress() {
        return address;
    }

    public long getLastAdvertisement() {
        return lastAdvertisement;
    }
}
