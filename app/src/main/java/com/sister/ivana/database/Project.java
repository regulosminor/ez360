package com.sister.ivana.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "project")
public class Project {
    public static final String COLUMN_PROJECT = "project";
    public static final String COLUMN_LIBRARY = "library";
    public static final String COLUMN_DEALER_NAME = "dealerName";

    public Project() {
        library = "";
        project = "";
        dealerName = "";
    }

    public Project(String library, String project) {
        this.library = library;
        this.project = project;
        dealerName = "";
    }

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false, columnName = COLUMN_LIBRARY)
    private String library;

    @DatabaseField(canBeNull = false, columnName = COLUMN_PROJECT)
    private String project;

    @DatabaseField(canBeNull = false, columnName = COLUMN_DEALER_NAME, defaultValue = "")
    private String dealerName;

    public String getProject() {
        return project;
    }

    public String getLibrary() {
        return library;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = (dealerName == null) ? "" : dealerName;
    }
}
