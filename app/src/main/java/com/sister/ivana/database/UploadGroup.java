package com.sister.ivana.database;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.sister.ivana.upload.UploadType;

@DatabaseTable(tableName = "uploadGroup")
public class UploadGroup {
    public enum UploadGroupStatus {
        FILES_PENDING(0),
        PENDING(1),
        COMPLETE(2),
        FAILED(3),
        EXTRACTION_PENDING(4);

        private final int databaseCode;

        UploadGroupStatus(int databaseCode) {
            this.databaseCode = databaseCode;
        }

        public static UploadGroupStatus create(int databaseCode) {
            switch (databaseCode) {
                case 0:
                    return FILES_PENDING;
                case 1:
                    return PENDING;
                case 2:
                    return COMPLETE;
                case 3:
                    return FAILED;
                case 4:
                    return EXTRACTION_PENDING;
                default:
                    return null;
            }
        }

        public int getDatabaseCode() {
            return databaseCode;
        }
    }

    public static final String COLUMN_VIN = "vin";
    public static final String COLUMN_PROJECT = "project";
    public static final String COLUMN_LIBRARY = "library";
    public static final String COLUMN_REPLACE = "replace";
    public static final String COLUMN_UPLOAD_TYPE = "uploadType";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_MAKE = "make";
    public static final String COLUMN_MODEL = "model";
    public static final String COLUMN_UPLOAD_STATUS = "uploadStatus";
    public static final String COLUMN_TIMESTAMP = "timestamp";
    public static final String COLUMN_USED_TURNTABLE = "usedTurntable";
    public static final String COLUMN_SOURCE_VIDEO_PATHNAME = "sourceVideoPathname";
    public static final String COLUMN_FRAME_COUNT = "frameCount";
    public static final String COLUMN_EXTRACTION_REQUEST_CODE = "extractionRequestCode";
    public static final String COLUMN_EXTRACTION_REQUEST_TIMESTAMP = "extractionRequestTimestamp";
    public static final String COLUMN_AUTOMATED_PICTURE_COUNT = "automatedPictureCount";
    public static final String COLUMN_SPIN_DURATION = "spinDuration";
    public static final String COLUMN_EXTRACTED_PICTURE_COUNT = "extractedPictureCount";
    public static final String COLUMN_CAMERA_ROLE = "cameraRole";

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = false, columnName = COLUMN_VIN)
    private String vin;

    @DatabaseField(canBeNull = false, columnName = COLUMN_PROJECT)
    private String project;

    @DatabaseField(canBeNull = false, columnName = COLUMN_LIBRARY)
    private String library;

    @DatabaseField(canBeNull = false, columnName = COLUMN_REPLACE)
    private boolean replace;

    @DatabaseField(canBeNull = false, columnName = COLUMN_UPLOAD_TYPE)
    private int uploadType;

    @DatabaseField(canBeNull = false, columnName = COLUMN_YEAR)
    private String year;

    @DatabaseField(canBeNull = false, columnName = COLUMN_MAKE)
    private String make;

    @DatabaseField(canBeNull = false, columnName = COLUMN_MODEL)
    private String model;

    @DatabaseField(canBeNull = false, columnName = COLUMN_UPLOAD_STATUS, defaultValue = "0")
    private int uploadStatus;

    @DatabaseField(canBeNull = false, columnName = COLUMN_TIMESTAMP)
    private long timestamp;

    @DatabaseField(canBeNull = false, columnName = COLUMN_USED_TURNTABLE, defaultValue = "false")
    private boolean usedTurntable;

    @DatabaseField(canBeNull = false, columnName = COLUMN_SOURCE_VIDEO_PATHNAME, defaultValue = "")
    private String sourceVideoPathname;

    @DatabaseField(canBeNull = false, columnName = COLUMN_FRAME_COUNT, defaultValue = "0")
    private int frameCount;

    @ForeignCollectionField(orderColumnName = UploadFile.COLUMN_SEQUENCE_NUMBER, orderAscending = true)
    ForeignCollection<UploadFile> files;

    @DatabaseField(canBeNull = false, columnName = COLUMN_EXTRACTION_REQUEST_CODE, defaultValue = "0")
    private long extractionRequestCode;

    @DatabaseField(canBeNull = false, columnName = COLUMN_EXTRACTION_REQUEST_TIMESTAMP, defaultValue = "0")
    private long extractionRequestTimestamp;

    @DatabaseField(canBeNull = false, columnName = COLUMN_AUTOMATED_PICTURE_COUNT, defaultValue = "0")
    private int automatedPictureCount;

    @DatabaseField(canBeNull = false, columnName = COLUMN_SPIN_DURATION, defaultValue = "0")
    private int spinDuration;

    @DatabaseField(canBeNull = false, columnName = COLUMN_EXTRACTED_PICTURE_COUNT, defaultValue = "0")
    private int extractedPictureCount;

    @DatabaseField(canBeNull = false, columnName = COLUMN_CAMERA_ROLE, defaultValue = "")
    private String cameraRole;

    public UploadGroup() {
    }

    public UploadGroup(String vin, String project, String library, String year, String make, String model, boolean replace, int uploadType, long timestamp, boolean usedTurntable, int automatedPictureCount, int spinDuration, int extractedPictureCount, String cameraRole) {
        this.vin = vin;
        this.project = project;
        this.library = library;
        this.year = year != null ? year : "";
        this.make = make != null ? make : "";
        this.model = model != null ? model : "";
        this.replace = replace;
        this.uploadType = uploadType;
        this.timestamp = timestamp;
        this.usedTurntable = usedTurntable;
        this.automatedPictureCount = automatedPictureCount;
        this.spinDuration = spinDuration;
        this.extractedPictureCount = extractedPictureCount;
        this.cameraRole = cameraRole;
    }

    @Override
    public String toString() {
        return String.format("UploadGroup[vin=%s, replace=%b, uploadType=%s]", getVin(), isReplace(), UploadType.create(uploadType));
    }

    public String getVin() {
        return vin;
    }

    public String getProject() {
        return project;
    }

    public String getLibrary() {
        return library;
    }

    public boolean isReplace() {
        return replace;
    }

    public int getUploadType() {
        return uploadType;
    }

    public ForeignCollection<UploadFile> getFiles() {
        return files;
    }

    public String getYear() {
        return year;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

    public UploadGroupStatus getUploadStatus() {
        return UploadGroupStatus.create(uploadStatus);
    }

    public void setUploadStatus(UploadGroupStatus status) {
        uploadStatus = status.getDatabaseCode();
    }

    public boolean getUsedTurntable() {
        return usedTurntable;
    }

    public long getExtractionRequestCode() {
        return extractionRequestCode;
    }

    public void setExtractionRequestCode(long extractionRequestCode) {
        this.extractionRequestTimestamp = System.currentTimeMillis();
        this.extractionRequestCode = extractionRequestCode;
    }

    public long getExtractionRequestTimestamp() {
        return extractionRequestTimestamp;
    }

    public void setExtractionRequestTimestamp(long extractionRequestTimestamp) {
        this.extractionRequestTimestamp = extractionRequestTimestamp;
    }

    public String getSourceVideoPathname() {
        return sourceVideoPathname;
    }

    public void setSourceVideoPathname(String sourceVideoPathname) {
        this.sourceVideoPathname = sourceVideoPathname;
    }

    public int getFrameCount() {
        return frameCount;
    }

    public void setFrameCount(int frameCount) {
        this.frameCount = frameCount;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int getAutomatedPictureCount() {
        return automatedPictureCount;
    }

    public int getSpinDuration() {
        return spinDuration;
    }

    public int getExtractedPictureCount() {
        return extractedPictureCount;
    }

    public String getCameraRole() {
        return cameraRole;
    }
}
