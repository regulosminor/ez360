package com.sister.ivana.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;
import com.sister.ivana.Util;
import com.sister.ivana.webapi.elasticsearch.MagazineResults;

import java.util.Collection;

@DatabaseTable(tableName = "magazine")
public class Magazine {
    public static final String COLUMN_INDEX = "id";
    public static final String COLUMN_ORDER = "order";
    public static final String COLUMN_PROJECT = "project";
    public static final String COLUMN_VEHICLE_TYPE = "vehicleType";
    public static final String COLUMN_YEAR = "year";
    public static final String COLUMN_MAKE = "make";
    public static final String COLUMN_MODEL = "model";

    public Magazine() {
    }

    @DatabaseField(generatedId = true, columnName = COLUMN_INDEX)
    private int id;

    @DatabaseField(canBeNull = false, columnName = COLUMN_ORDER)
    private long order;

    @DatabaseField(canBeNull = false, columnName = COLUMN_PROJECT, defaultValue = "")
    private String project;

    @DatabaseField(canBeNull = false, columnName = COLUMN_VEHICLE_TYPE, defaultValue = "")
    private String vehicleType;

    @DatabaseField(canBeNull = false, columnName = COLUMN_YEAR, defaultValue = "")
    private String year;

    @DatabaseField(canBeNull = false, columnName = COLUMN_MAKE, defaultValue = "")
    private String make;

    @DatabaseField(canBeNull = false, columnName = COLUMN_MODEL, defaultValue = "")
    private String model;

    @ForeignCollectionField(eager = true)
    private Collection<MagazineShot> shots;

    public static Magazine create(MagazineResults.MagazineData magazineData, long order) {
        Magazine magazine = new Magazine();
        magazine.order = order;
        magazine.project = Util.enforceNonNull(magazineData.project_id);
        magazine.vehicleType = Util.enforceNonNull(magazineData.vehicle_type);
        magazine.year = Util.enforceNonNull(magazineData.year);
        magazine.make = Util.enforceNonNull(magazineData.make);
        magazine.model = Util.enforceNonNull(magazineData.model);
        return magazine;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getID() {
        return id;
    }

    public Collection<MagazineShot> getShots() {
        return shots;
    }

    public String getYear() {
        return year;
    }

    public String getMake() {
        return make;
    }

    public String getModel() {
        return model;
    }

}
