package com.sister.ivana.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "uploadFile")
public class UploadFile {
    public enum UploadFileStatus {
        PENDING(0),
        COMPLETE(1),
        FAILED(2);

        private final int databaseCode;

        UploadFileStatus(int databaseCode) {
            this.databaseCode = databaseCode;
        }

        public static UploadFileStatus create(int databaseCode) {
            switch (databaseCode) {
                case 1:
                    return COMPLETE;
                case 2:
                    return FAILED;
                default:
                    return PENDING;
            }
        }

        public int getDatabaseCode() {
            return databaseCode;
        }
    }

    public static final String COLUMN_PATHNAME = "pathname";
    public static final String COLUMN_UPLOAD_GROUP_ID = "uploadGroupID";
    public static final String COLUMN_TRANSFER_ID = "transferID";
    public static final String COLUMN_AWS_KEY = "awsKey";
    public static final String COLUMN_UPLOAD_STATUS = "uploadStatus";
    public static final String COLUMN_TIMESTAMP = "timestamp";
    public static final String COLUMN_PRIORITY = "priority";
    public static final String COLUMN_COMPRESSED_PATHNAME = "compressedPathname";
    public static final String COLUMN_COMPRESSION_REQUEST_CODE = "compressionRequestCode";
    public static final String COLUMN_FEATURE_CODE = "featureCode";
    public static final String COLUMN_FEATURE_TITLE = "featureTitle";
    public static final String COLUMN_SEQUENCE_NUMBER = "sequenceNumber";
    public static final String COLUMN_COMPRESSION_REQUEST_TIMESTAMP = "compressionRequestTimestamp";
    public static final String COLUMN_UPLOAD_SUB_GROUP_ID = "uploadSubGroupID";
    public static final String COLUMN_SPIN_NUMBER = "spinNumber";
    public static final String COLUMN_SPIN_INDEX = "spinIndex";
    public static final String COLUMN_ADD_TO_MAGAZINE = "addToMagazine";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_DESCRIPTION = "description";
    public static final String COLUMN_COORDINATE_X = "x";
    public static final String COLUMN_COORDINATE_Y = "y";

    @DatabaseField(id = true, canBeNull = false, columnName = COLUMN_PATHNAME, defaultValue = "")
    private String pathname;

    @DatabaseField(canBeNull = false, foreign = true, foreignAutoRefresh = true, columnName = COLUMN_UPLOAD_GROUP_ID)
    private UploadGroup uploadGroup;

    @DatabaseField(columnName = COLUMN_TRANSFER_ID)
    private Integer transferID;

    @DatabaseField(canBeNull = false, columnName = COLUMN_AWS_KEY)
    private String awsKey;

    @DatabaseField(canBeNull = false, columnName = COLUMN_UPLOAD_STATUS, defaultValue = "0")
    private int uploadStatus;

    @DatabaseField(canBeNull = false, columnName = COLUMN_PRIORITY)
    private int priority;

    @DatabaseField(canBeNull = false, columnName = COLUMN_TIMESTAMP)
    private long timestamp;

    @DatabaseField(canBeNull = false, columnName = COLUMN_COMPRESSED_PATHNAME, defaultValue = "")
    private String compressedPathname;

    @DatabaseField(canBeNull = false, columnName = COLUMN_COMPRESSION_REQUEST_CODE, defaultValue = "0")
    private long compressionRequestCode;

    @DatabaseField(canBeNull = true, columnName = COLUMN_FEATURE_CODE, defaultValue = "")
    private String featureCode;

    @DatabaseField(canBeNull = false, columnName = COLUMN_FEATURE_TITLE, defaultValue = "")
    private String featureTitle;

    @DatabaseField(canBeNull = false, columnName = COLUMN_SEQUENCE_NUMBER, defaultValue = "0")
    private int sequenceNumber;

    @DatabaseField(canBeNull = false, columnName = COLUMN_COMPRESSION_REQUEST_TIMESTAMP, defaultValue = "0")
    private long compressionRequestTimestamp;

    @DatabaseField(canBeNull = false, columnName = COLUMN_UPLOAD_SUB_GROUP_ID, defaultValue = "0")
    private int uploadSubGroup;

    @DatabaseField(canBeNull = false, columnName = COLUMN_TITLE, defaultValue = "")
    private String title;

    @DatabaseField(canBeNull = false, columnName = COLUMN_DESCRIPTION, defaultValue = "")
    private String description;

    @DatabaseField(canBeNull = false, columnName = COLUMN_SPIN_NUMBER, defaultValue = "0")
    private int spinNumber;

    @DatabaseField(canBeNull = false, columnName = COLUMN_SPIN_INDEX, defaultValue = "0")
    private int spinIndex;

    @DatabaseField(canBeNull = false, columnName = COLUMN_ADD_TO_MAGAZINE, defaultValue = "false")
    private boolean addToMagazine;

    @DatabaseField(canBeNull = false, columnName = COLUMN_COORDINATE_X, defaultValue = "0")
    private double x;

    @DatabaseField(canBeNull = false, columnName = COLUMN_COORDINATE_Y, defaultValue = "0")
    private double y;

    public UploadFile() {
    }

    public UploadFile(UploadGroup uploadGroup, String pathname, String awsKey, int priority, long timestamp) {
        this.uploadGroup = uploadGroup;
        this.pathname = pathname;
        this.awsKey = awsKey;
        this.priority = priority;
        this.timestamp = timestamp;
    }

    public UploadFile(UploadGroup uploadGroup, String pathname, String awsKey, int priority, long timestamp, String title, String description, int spinNumber, int spinIndex, boolean addToMagazine, double x, double y) {
        this.uploadGroup = uploadGroup;
        this.pathname = pathname;
        this.awsKey = awsKey;
        this.priority = priority;
        this.timestamp = timestamp;
        this.title = title;
        this.description = description;
        this.spinNumber = spinNumber;
        this.spinIndex = spinIndex;
        this.addToMagazine = addToMagazine;
        this.x = x;
        this.y = y;
    }

    public UploadFile(UploadGroup uploadGroup, String pathname, String awsKey, int priority, long timestamp, double x, double y) {
        this.uploadGroup = uploadGroup;
        this.pathname = pathname;
        this.awsKey = awsKey;
        this.priority = priority;
        this.timestamp = timestamp;
        this.x = x;
        this.y = y;
    }

    public UploadFile(UploadGroup uploadGroup, String pathname, String awsKey, String featureCode, String featureTitle, int priority, long timestamp, int sequenceNumber, int uploadSubGroup) {
        this.uploadGroup = uploadGroup;
        this.pathname = pathname;
        this.featureCode = featureCode;
        this.featureTitle = featureTitle;
        this.awsKey = awsKey;
        this.priority = priority;
        this.timestamp = timestamp;
        this.sequenceNumber = sequenceNumber;
        this.uploadSubGroup = uploadSubGroup;
    }

    @Override
    public String toString() {
        return String.format("UploadFile[vin=%s, pathname=%s, compressedPathname=%s]", uploadGroup.getVin(), pathname, compressedPathname);
    }

    public String getPathname() {
        return pathname;
    }

    public Integer getTransferID() {
        return transferID;
    }

    public void setTransferID(int transferID) {
        this.transferID = transferID;
    }

    public void resetTransferID() {
        transferID = null;
    }

    public String getAWSKey() {
        return awsKey;
    }

    public UploadGroup getUploadGroup() {
        return uploadGroup;
    }

    public int getUploadSubGroup() {
        return uploadSubGroup;
    }

    public void setUploadStatus(UploadFileStatus status) {
        this.uploadStatus = status.databaseCode;
    }

    public boolean isUploadComplete() {
        return this.uploadStatus == UploadFileStatus.COMPLETE.databaseCode;
    }

    public String getCompressedPathname() {
        return compressedPathname;
    }

    public void putCompressedPathname(String compressedPathname) {
        this.compressedPathname = compressedPathname != null ? compressedPathname : "";
    }

    public void setCompressionRequestCode(long requestCode) {
        compressionRequestCode = requestCode;
        compressionRequestTimestamp = System.currentTimeMillis();
    }

    public long getCompressionRequestCode() {
        return compressionRequestCode;
    }

    public String getFeatureCode() {
        return featureCode;
    }

    public String getFeatureTitle() {
        return featureTitle;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public long getCompressionRequestTimestamp() {
        return compressionRequestTimestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getSpinNumber() {
        return spinNumber;
    }

    public int getSpinIndex() {
        return spinIndex;
    }

    public boolean getAddToMagazine() {
        return addToMagazine;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}
