package com.sister.ivana;


import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.text.TextUtils;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.MagazineShot;

import java.io.File;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static android.content.Context.DOWNLOAD_SERVICE;

public class ImageDownloader {
    public interface ImageDownloaderListener {
        void imageDownloadFailed();

        void imageDownloadComplete();

        void imageDownloadProgress(int total, int complete);
    }

    private static final int STATUS_POLL_PERIOD_MS = 5000;
    private final Context context;
    private final ScheduledExecutorService primaryExecutor;
    private final ExecutorService updateExecutor;
    private final DownloadManager downloadManager;
    private final DatabaseHelper databaseHelper;
    private ImageDownloaderListener listener;
    private final Handler mainHandler;
    private final Set<Long> requestIDs = Collections.synchronizedSet(new HashSet<Long>());
    private volatile int totalRequests;
    private volatile int completedRequests;
    private ScheduledFuture<?> statusTask;

    final private BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // We use a different thread for this because initially it can take awhile to
            // enqueue all the requests (about 20 seconds for 300 or more). During that time
            // we still need to be able to display updates to the user.
            final long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
            updateExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    downloadComplete(id);
                }
            });
        }
    };

    public ImageDownloader(Context context, DatabaseHelper databaseHelper, ImageDownloaderListener listener) {
        this.mainHandler = new Handler(context.getMainLooper());
        this.context = context;
        this.listener = listener;
        primaryExecutor = Executors.newSingleThreadScheduledExecutor();
        // updateExecutor = Executors.newSingleThreadExecutor();
        updateExecutor = Executors.newSingleThreadExecutor();
        downloadManager = (DownloadManager) context.getSystemService(DOWNLOAD_SERVICE);
        this.databaseHelper = databaseHelper;
        context.registerReceiver(downloadReceiver, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
    }

    public void start() {
        primaryExecutor.execute(new Runnable() {
            @Override
            public void run() {
                startDownloads();
            }
        });
    }

    public void stop() {
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                context.unregisterReceiver(downloadReceiver);
            }
        });

        primaryExecutor.execute(new Runnable() {
            @Override
            public void run() {
                cancelRequests();
            }
        });
    }

    private void startDownloads() {
        deleteImages();

        List<Uri> uris;
        try {
            uris = getImageURIs();
        } catch (SQLException e) {
            Util.logError("Error getting image URLs from database", e);
            postDownloadFailed();
            return;
        }

        if (uris.isEmpty()) {
            postDownloadComplete();
            return;
        }

        // Display initial count since it can take awhile to enqueue all of the requests.
        totalRequests = uris.size();
        postProgress();

        for (Uri uri : uris) {
            final DownloadManager.Request request = createDownloadRequest(uri);
            final long id = downloadManager.enqueue(request);
            requestIDs.add(id);
        }

        scheduleStatusCheck(false);
    }

    private void scheduleStatusCheck(boolean immediate) {
        if (statusTask != null) {
            statusTask.cancel(false);
            statusTask = null;
        }

        statusTask = primaryExecutor.schedule(new Runnable() {
            @Override
            public void run() {
                checkStatus();
            }
        }, immediate ? 0 : STATUS_POLL_PERIOD_MS, TimeUnit.MILLISECONDS);
    }

    /*
     * We don't get a broadcast from the download service if it is not downloading, for example if
     * WiFi is not available. Periodically we poll to see if it looks like it is still running.
     */
    private void checkStatus() {
        if (requestIDs.isEmpty()) {
            postDownloadComplete();
            return;
        }

        for (long id : requestIDs) {
            Integer status = getRequestStatus(id);
            if (status == null || status == DownloadManager.STATUS_PAUSED) {
                cancelRequests();
                postDownloadFailed();
            }
        }
    }

    private void postDownloadFailed() {
        final ImageDownloaderListener downloadListener = listener;
        listener = null;

        if (downloadListener != null) {
            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    downloadListener.imageDownloadFailed();
                }
            });
        }
    }

    private void postDownloadComplete() {
        final ImageDownloaderListener downloadListener = listener;
        listener = null;

        if (downloadListener != null) {
            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    downloadListener.imageDownloadComplete();
                }
            });
        }
    }

    private void postProgress() {
        final ImageDownloaderListener downloadListener = listener;
        if (downloadListener == null) {
            return;
        }

        final int total = totalRequests;
        final int complete = completedRequests;
        mainHandler.post(new Runnable() {
            @Override
            public void run() {
                downloadListener.imageDownloadProgress(total, complete);
            }
        });
    }

    private void downloadComplete(long requestID) {
        if (requestIDs.remove(requestID)) {
            completedRequests++;
            Integer status = getRequestStatus(requestID);
            if (status == null || status != DownloadManager.STATUS_SUCCESSFUL) {
                Util.logError("Failed downloading image: " + status);
                postDownloadFailed();
                return;
            }

            postProgress();

            if (completedRequests >= totalRequests) {
                scheduleStatusCheck(true);
            } else {
                scheduleStatusCheck(true);
            }
        }
    }

    private Integer getRequestStatus(long id) {
        Integer status = null;
        Cursor cursor = null;
        try {
            final DownloadManager.Query query = new DownloadManager.Query().setFilterById(id);
            cursor = downloadManager.query(query);
            if (cursor.moveToNext()) {
                status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
            }

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return status;
    }

    private void cancelRequests() {
        for (Long id : requestIDs) {
            downloadManager.remove(id);
        }
        requestIDs.clear();
    }

    private void deleteImages() {
        File[] images = FileUtil.getImagesDirectory().listFiles();
        if (images != null) {
            for (File image : images) {
                if (!image.delete()) {
                    Util.logError("Failed deleting image " + image.getAbsolutePath());
                }
            }
        }
    }

    private DownloadManager.Request createDownloadRequest(Uri uri) {
        return new DownloadManager.Request(uri)
                .setDestinationUri(Uri.fromFile(FileUtil.getLocalImageFile(uri)))
                .setAllowedOverMetered(false)
                .setNotificationVisibility(DownloadManager.Request.VISIBILITY_HIDDEN);
    }


    private List<Uri> getImageURIs() throws SQLException {
        final Dao<MagazineShot, Integer> shotDao = databaseHelper.getMagazineShotDao();
        final QueryBuilder<MagazineShot, Integer> builder =
                shotDao.queryBuilder()
                        .selectColumns(MagazineShot.COLUMN_IMAGE)
                        .distinct();
        final List<MagazineShot> shots = shotDao.query(builder.prepare());
        final ArrayList<Uri> uris = new ArrayList<>();
        if (shots != null) {
            for (MagazineShot shot : shots) {
                // Some shots don't have image URIs.
                final String uriString = shot.getImage();
                if (!TextUtils.isEmpty(uriString)) {
                    uris.add(Uri.parse(shot.getImage()));
                }
            }
        }

        return uris;
    }
}
