package com.sister.ivana;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.media.AudioManager;
import android.media.ToneGenerator;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.table.TableUtils;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.Magazine;
import com.sister.ivana.database.MagazineShot;
import com.sister.ivana.database.OrmLiteBaseAppCompatActivity;
import com.sister.ivana.database.Project;
import com.sister.ivana.database.Vehicle;
import com.sister.ivana.dialog.AppDialogFragment;
import com.sister.ivana.messaging.AdvertiserService;
import com.sister.ivana.messaging.InterDeviceCommand;
import com.sister.ivana.messaging.NetworkService;
import com.sister.ivana.messaging.UDPListenerService;
import com.sister.ivana.settings.SettingsServerComms;
import com.sister.ivana.upload.UploaderService;
import com.sister.ivana.webapi.CurrentVersion;
import com.sister.ivana.webapi.WebService;
import com.sister.ivana.webapi.elasticsearch.BannerResults;
import com.sister.ivana.webapi.elasticsearch.ESWebService;
import com.sister.ivana.webapi.elasticsearch.InventoryResults;
import com.sister.ivana.webapi.elasticsearch.MagazineResults;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class VinSearchActivity extends OrmLiteBaseAppCompatActivity<DatabaseHelper>
        implements VinConfirmationDialogFragment.VinConfirmationListener, CreateVehicleDialogFragment.CreateVehicleListener, UpgradeAppDialogFragment.UpgradeAppListener {

    private static final int REQUEST_CODE_SCAN_BARCODE = 101;
    private static final int REQUEST_CODE_PERMISSIONS_REQUEST = 102;

    private class InventoryUpdateTask extends AsyncTask<InventoryResults, Void, Void> {
        private final DownloadStatus downloadStatus;

        public InventoryUpdateTask(DownloadStatus downloadStatus) {
            this.downloadStatus = downloadStatus;
        }

        @Override
        protected Void doInBackground(InventoryResults... inventoryResults) {
            // We don't delete existing vehicles until the first good results come back in
            // case the network call fails.
            if (downloadStatus.isFirst()) {
                try {
                    TableUtils.clearTable(getConnectionSource(), Vehicle.class);
                } catch (SQLException e) {
                    Util.logError("Error clearing vehicle table", e);
                    return null;
                }
            }

            if (inventoryResults.length > 0) {
                final InventoryResults results = inventoryResults[0];
                if (results.hits != null && results.hits.hits != null) {
                    for (InventoryResults.VehicleHit vehicleHit : results.hits.hits) {
                        publishProgress();
                        insertVehicle(vehicleHit._source);
                        downloadStatus.downloadedVehicles++;
                    }
                }
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            updateProgress(downloadStatus);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            downloadStatus.next();
            startVehicleRequest(downloadStatus);
        }

        private void insertVehicle(InventoryResults.VehicleData vehicleData) {
            try {
                final Vehicle vehicle = Vehicle.create(vehicleData);
                getHelper().getVehicleDao().createOrUpdate(vehicle);
            } catch (SQLException e) {
                Util.logError("Failed adding vehicle " + vehicleData, e);
            }catch (IllegalStateException il) {
                Util.logError(il.getMessage() + ". Exception occured while adding vehicle " + vehicleData, il);
            }
        }
    }

    private class MagazineUpdateTask extends AsyncTask<MagazineResults, Void, Void> {
        private final MagazineDownloadStatus status;

        public MagazineUpdateTask(MagazineDownloadStatus status) {
            this.status = status;
        }

        @Override
        protected Void doInBackground(MagazineResults... magazineResults) {
            if (status.isFirst()) {
                try {
                    TableUtils.clearTable(getConnectionSource(), Magazine.class);
                    TableUtils.clearTable(getConnectionSource(), MagazineShot.class);
                } catch (SQLException e) {
                    Util.logError("Error clearing magazine tables", e);
                    return null;
                }
            }

            if (magazineResults.length > 0) {
                final MagazineResults results = magazineResults[0];
                int index = 0;
                for (MagazineResults.MagazineHit magazineHit : results.hits.hits) {
                    final MagazineResults.MagazineData magazineData = magazineHit._source;
                    insertMagazine(magazineData, index);
                    index++;
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            status.next();
            startNextMagazineRequest(status);
        }

        private void insertMagazine(MagazineResults.MagazineData magazineData, int index) {
            final Magazine magazine = Magazine.create(magazineData, index);
            try {
                getHelper().getMagazineDao().createOrUpdate(magazine);

                if (magazineData.features != null) {
                    insertShots(magazine, magazineData.features);
                }

            } catch (SQLException e) {
                Util.logError("Failed adding magazine", e);
            }
        }

        private void insertShots(Magazine magazine, List<MagazineResults.MagazineShotData> shots) {
            int i = 0;
            for (MagazineResults.MagazineShotData shotData : shots) {
                if (TextUtils.isEmpty(shotData.reference_image)) {
                    // Some shots come back from server with no reference image and the app does
                    // not care about those.
                    continue;
                }

                MagazineShot shot = MagazineShot.create(magazine, shotData, i);
                try {
                    getHelper().getMagazineShotDao().createOrUpdate(shot);
                } catch (SQLException e) {
                    Util.logError("Failed creating magazine shot");
                }

                i++;
            }
        }
    }

    private static class MagazineDownloadStatus {
        final List<String> projects = new ArrayList<>();
        int current;

        boolean isFirst() {
            return current == 0;
        }

        boolean isComplete() {
            return current >= projects.size();
        }

        String currentProject() {
            return (current < projects.size()) ? projects.get(current) : "";
        }

        void next() {
            current++;
        }
    }

    private static class BannerDownloadStatus {
        final List<String> projects = new ArrayList<>();
        final HashMap<String, String> urls = new HashMap<>();
        int current;

        boolean isBannerInfoComplete() {
            return current >= projects.size();
        }

        String currentProject() {
            return (current < projects.size()) ? projects.get(current) : "";
        }

        void next() {
            current++;
        }

        void addURL(String project, String url) {
            urls.put(project, url);
        }
    }

    private static class DownloadStatus {
        final List<String> projects = new ArrayList<>();
        int current;
        int totalVehicles;
        int downloadedVehicles;

        boolean isFirst() {
            return current == 0;
        }

        boolean isComplete() {
            return current >= projects.size();
        }

        void reset() {
            current = 0;
        }

        String currentProject() {
            return (current < projects.size()) ? projects.get(current) : "";
        }

        void next() {
            current++;
        }
    }

    private class VinWatcher implements TextWatcher {
        // SiSTeR requested this limit because their system does not handle really long VINs.
        private static final int MAX_VIN_LENGTH = 50;
        private final PreparedQuery<Vehicle> vehicleQuery;
        private final SelectArg vinArg = new SelectArg();
        private final SelectArg stockArg = new SelectArg();
        private boolean isAutoMatchEnabled;
        private String beforeTextChangedString = "";
        private ToneGenerator toneGenerator;

        public VinWatcher() {
            isAutoMatchEnabled = true;
            PreparedQuery<Vehicle> query = null;
            try {
                QueryBuilder<Vehicle, ?> builder = getHelper().getVehicleDao().queryBuilder();
                builder.limit(2L)
                        .where()
                        .like(Vehicle.COLUMN_VIN, vinArg)
                        .or()
                        .like(Vehicle.COLUMN_STOCK, stockArg);
                query = builder.prepare();
            } catch (SQLException e) {
                // Keep going. We just won't be able to automatically match as the user types.
                Util.logError("Failed creating vehicle query", e);
            }

            vehicleQuery = query;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            beforeTextChangedString = charSequence.toString();
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            final String vin = editable.toString().toUpperCase().trim();
            if (createTestCheckBox.isChecked()) {
                if (vin.length() > MAX_VIN_LENGTH) {
                    if (toneGenerator == null) {
                        // We use to create one of these for each beep but it you create several
                        // close together you can get a crash because the resources aren't
                        // released quickly enough (see https://groups.google.com/forum/?fromgroups=#!topic/android-developers/p8p0Ech3Yu8)
                        toneGenerator = new ToneGenerator(AudioManager.STREAM_MUSIC, 100);
                    }
                    toneGenerator.stopTone();
                    toneGenerator.startTone(ToneGenerator.TONE_CDMA_PIP,150);

                    vinEditText.setText(beforeTextChangedString);
                    vinEditText.setSelection(vinEditText.getText().length());
                    nextButton.setEnabled(true);
                } else {
                    nextButton.setEnabled(vin.length() > 0);
                }
                return;
            }

            if (vin.length() == 0) {
                // If we've disabled auto matching, re-enabled it if the user clears the
                // text field.
                isAutoMatchEnabled = true;
            }

            if (vehicleQuery != null && isAutoMatchEnabled && vin.length() > 1) {
                final String arg = ("%" + vin + "%");
                vinArg.setValue(arg);
                stockArg.setValue(arg);

                try {
                    List<Vehicle> matches = getHelper().getVehicleDao().query(vehicleQuery);
                    if (matches.size() == 1) {
                        // Sometimes Android calls the text watcher more than once for the same
                        // text. Once we've found a match we don't want to match again.
                        isAutoMatchEnabled = false;

                        final Vehicle vehicle = matches.get(0);
                        VinConfirmationDialogFragment dialog = VinConfirmationDialogFragment.newInstance(vehicle.getVin(), vehicle.getStock(), vehicle.getYear(), vehicle.getMake(), vehicle.getModel(), vehicle.getThumbnail(), vehicle.getProject(), vehicle.getLibrary());
                        dialog.show(getFragmentManager(), "vin");
                        return;
                    }
                } catch (SQLException e) {
                    Util.logError("Failed querying for VIN", e);
                }
            }

            // Sometimes the barcode printed on the door of the vehicle has a 'I' prefix to indicate
            // an import but this is not part of the VIN. If we scan such a barcode try to drop
            // the leading I and scan it.
            if (vin.startsWith("I") && vin.length() == Util.VIN_LENGTH + 1) {
                findVehicle(vin.substring(1));
            } else if (!vin.startsWith("I") && vin.length() == Util.VIN_LENGTH) {
                findVehicle(vin);
            }
        }
    }

    @BindView(R.id.vin_field) EditText vinEditText;
    @BindView(R.id.create_test_vehicle) CheckBox createTestCheckBox;
    @BindView(R.id.camera_only_mode) CheckBox cameraOnlyModeCheckBox;
    @BindView(R.id.share_input) CheckBox shareInputCheckBox;
    @BindView(R.id.next_button) Button nextButton;
    @BindView(R.id.search_button) Button searchButton;

    private static boolean isDownloadActive;
    private Call<InventoryResults> inventoryRequest;
    private Call<MagazineResults> magazineRequest;
    private Call<BannerResults> bannerRequest;
    private Call<CurrentVersion> currentVersionRequest;
    private ProgressDialog progressDialog;
    private VinWatcher vinWatcher;
    private ActionBarUploadStatus actionBarStatus;
    private ImageDownloader imageDownloader;
    private BannerDownloader bannerDownloader;
    private PowerManager.WakeLock updateWakeLock;

    /**
     * This is to work around a crash when we receive a shared VIN that recreates this activity
     * and interrupts the inventory download. Since it should be rare that a phone is downloading
     * inventory when a VIN is shared and downloading the inventory is probably higher priority,
     * we ignore shared VINs while downloading inventory.
     */
    public static boolean isInventoryDownloadActive() {
        return isDownloadActive;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vin_search);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        addAllCapsFilter(vinEditText);
    }

    @Override
    protected void onStart() {
        super.onStart();

        actionBarStatus = new ActionBarUploadStatus();
        actionBarStatus.onStart(this);

        vinEditText.setText("");
        vinWatcher = new VinWatcher();
        vinEditText.addTextChangedListener(vinWatcher);

        vinEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int action, KeyEvent keyEvent) {
                if (action == EditorInfo.IME_ACTION_DONE && !createTestCheckBox.isChecked()) {
                    findVehicle(vinEditText.getText().toString().trim().toUpperCase());
                    return true;
                }

                return false;
            }
        });

        // Automatically start download of magazines and vehicles if we have no vehicles. Otherwise
        // the user needs to start download manually.
        if (!haveVehicles()) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                downloadMagazines();
            }
        }

        // Periodically start the uploader service. One case we might need this is if the app
        // crashed with outstanding compression requests. Kicking the uploader service will
        // cause him to restart the requests.
        startService(UploaderService.createScheduleJobIntent(this));

        Intent udpListener = new Intent(this, UDPListenerService.class);
        startService(udpListener);

        Intent advertiser = new Intent(this, AdvertiserService.class);
        startService(advertiser);

        Intent network = new Intent(this, NetworkService.class);
        startService(network);
        invalidateOptionsMenu();

        checkPermissions();
    }

    private void checkPermissions() {
        final String[] required = new String[]{
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.CAMERA
        };

        final ArrayList<String> ungranted = new ArrayList<>();
        for (String permission : required) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                ungranted.add(permission);
            }
        }

        if (!ungranted.isEmpty()) {
            final String[] requests = ungranted.toArray(new String[ungranted.size()]);
            ActivityCompat.requestPermissions(this,
                    requests,
                    REQUEST_CODE_PERMISSIONS_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode != REQUEST_CODE_PERMISSIONS_REQUEST) {
            return;
        }

        Timber.i("Permissions request result");
        for (int i = 0; i < permissions.length; i++) {
            Timber.i("%s: %s", permissions[i], grantResults[i]);
        }

        downloadMagazines();
    }

    private static void addAllCapsFilter(EditText editText) {
        final InputFilter[] existing = editText.getFilters();
        final InputFilter[] updated = new InputFilter[existing.length + 2];
        System.arraycopy(existing, 0, updated, 0, existing.length);
        updated[updated.length - 2] = new InputFilter.AllCaps();

        updated[updated.length - 1] = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (!Character.isLetterOrDigit(source.charAt(i)) && source.charAt(i) != '_') {
                        return "";
                    }
                }
                return null;
            }
        };

        editText.setFilters(updated);
    }

    @Override
    protected void onResume() {
        super.onResume();

        SettingsServerComms settingsServerComms = new SettingsServerComms(this, getHelper());
        if (settingsServerComms.shouldPollSettings()) {
            settingsServerComms.startGetSettingsRequest(null);
        }

        AppSharedPreferences sharedPreferences = new AppSharedPreferences(this);
        long lastVersionPoll = sharedPreferences.getLastVersionPoll();
        long oneDay = 24 * 60 * 60 * 1000;
        if (System.currentTimeMillis() - lastVersionPoll > oneDay) {
            sharedPreferences.updateLastVersionPoll();
            startCurrentVersionRequest();
        }

        createTestCheckBox.setChecked(false);
        shareInputCheckBox.setChecked(true);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }

        switch (requestCode) {
            case REQUEST_CODE_SCAN_BARCODE: {
                //if user hits the back button without scanning something, catch the error and set scan to ""
                String contents;
                try {
                    // Read off the  XING Barcode Scanner intent extras to pull the vin
                    contents = data.getStringExtra("SCAN_RESULT");
                    if (contents != null && contents.length() > 0) {
                        // Some VIN barcodes start with 'I' to indicate an import but it is not part
                        // of the VIN.
                        if (contents.startsWith("I") && contents.length() == Util.VIN_LENGTH + 1) {
                            contents = contents.substring(1);
                        }

                        vinEditText.setText(contents);
                        onSearchClick();
                    }
                } catch (Exception e) {
                }
            }
            break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_vin_search, menu);

        AppSharedPreferences preferences = new AppSharedPreferences(this);
        MenuItem enableUploads = menu.findItem(R.id.action_enable_upload);
        enableUploads.setVisible(!preferences.areUploadsEnabled());
        MenuItem disableUploads = menu.findItem(R.id.action_disable_upload);
        disableUploads.setVisible(preferences.areUploadsEnabled());

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final AppSharedPreferences preferences = new AppSharedPreferences(this);

        switch (item.getItemId()) {
            case R.id.action_logout:
                Util.logout(this);
                return true;

            case R.id.action_enable_upload:
                preferences.putUploadsEnabled(true);
                invalidateOptionsMenu();
                return true;

            case R.id.action_disable_upload:
                preferences.putUploadsEnabled(false);
                invalidateOptionsMenu();
                return true;

            case R.id.action_download_inventory:
                downloadMagazines();
                return true;

            case R.id.action_settings:
                startActivity(new Intent(this, com.sister.ivana.settings.SettingsActivity.class));
//                startActivity(new Intent(this, SettingsActivity.class));
                return true;

            default:
                return false;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        actionBarStatus.onStop();
        if (inventoryRequest != null) {
            inventoryRequest.cancel();
            inventoryRequest = null;
        }
        if (magazineRequest != null) {
            magazineRequest.cancel();
            magazineRequest = null;
        }
        if (imageDownloader != null) {
            imageDownloader.stop();
            imageDownloader = null;
        }

        if (bannerRequest != null) {
            bannerRequest.cancel();
            bannerRequest = null;
        }

        if (currentVersionRequest != null) {
            currentVersionRequest.cancel();
            currentVersionRequest = null;
        }

        if (bannerDownloader != null) {
            bannerDownloader.stop();
            bannerDownloader = null;
        }

        vinEditText.removeTextChangedListener(vinWatcher);
        vinWatcher = null;

        hideProgress();
    }

    @Override
    public void onVinConfirmationPositiveClick(String vin, String project, String library) {
        startVehicleActivity(vin, project, library);
    }

    @Override
    public void onVinConfirmationNegativeClick() {
    }

    @Override
    public void onCreateVehiclePositiveClick(String vin) {
        final VinValidator validator = new VinValidator(vin);
        if (validator.isValid()) {
            addVehicle(vin, false);
        } else {
            AppDialogFragment.show(this, R.string.vin_error_title, validator.getErrorString());
        }
    }

    @Override
    public void onCreateVehicleNegativeClick() {
    }

    @OnClick(R.id.search_button)
    void onSearchClick() {
        findVehicle(vinEditText.getText().toString().trim().toUpperCase());
    }

    @OnClick(R.id.scan_barcode_button)
    void onScanBarcodeClick() {
        Intent intent = new Intent("com.google.zxing.client.android.SCAN");

        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        if (list.size() > 0) {
            startActivityForResult(intent, REQUEST_CODE_SCAN_BARCODE);
        } else {
            AppDialogFragment.show(this, R.string.zxing_error_title, R.string.zxing_not_found);
        }
    }

    @OnClick(R.id.create_test_vehicle)
    void onTestVehicleClick() {
        setTestVehicleEnabled(createTestCheckBox.isChecked());
    }

    @OnClick(R.id.camera_only_mode)
    void onCameraOnlyModeClick() {
        setCameraOnlyModeEnabled(cameraOnlyModeCheckBox.isChecked());
        if (cameraOnlyModeCheckBox.isChecked()) {
            createTestCheckBox.setChecked(false);
        }
    }

    @OnClick(R.id.next_button)
    void onNextClick() {
        if (createTestCheckBox.isChecked()) {
            addVehicle(vinEditText.getText().toString().trim(), false);
        } else if (cameraOnlyModeCheckBox.isChecked()) {
            addVehicle(generateCameraOnlyVin(), true);
        }
    }

    /**
     * Update enabled property for controls based on whether the user is creating a test vehicle.
     */
    private void setTestVehicleEnabled(boolean isEnabled) {
        if (isEnabled) {
            vinEditText.setEnabled(true);
            cameraOnlyModeCheckBox.setChecked(false);
            vinEditText.setText(generateTestVin());
            vinEditText.requestFocus();
            vinEditText.setSelection(vinEditText.getText().length());
        } else {
            // Clear the VIN so that we don't allow a VIN that is too long to be entered.
            vinEditText.setText("");
        }
        nextButton.setEnabled(isEnabled);
        searchButton.setEnabled(!isEnabled);
    }

    private void setCameraOnlyModeEnabled(boolean isEnabled) {
        if (isEnabled) {
            vinEditText.setText("");
        }
        nextButton.setEnabled(isEnabled);
        vinEditText.setEnabled(!isEnabled);
        searchButton.setEnabled(!isEnabled);
    }

    private boolean haveVehicles() {
        try {
            QueryBuilder<Vehicle, ?> builder = getHelper().getVehicleDao().queryBuilder();
            Vehicle vehicle = builder.selectColumns(Vehicle.COLUMN_VIN).queryForFirst();
            return vehicle != null;
        } catch (SQLException e) {
            Util.logError("Error checking for empty inventory", e);
        }

        return false;
    }

    private void downloadMagazines() {
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            updateWakeLock = powerManager.newWakeLock(PowerManager.FULL_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "VinSearchActivity:UpdateLock");
            updateWakeLock.acquire();
        }
        isDownloadActive = true;

        MagazineDownloadStatus status = new MagazineDownloadStatus();

        try {
            Dao<Project, ?> dao = getHelper().getProjectDao();
            for (Project project : dao) {
                status.projects.add(project.getProject());
            }

            // If we have at least one project, we'll wait until the results come back to delete
            // existing data
            if (status.projects.isEmpty()) {
                TableUtils.clearTable(getConnectionSource(), Magazine.class);
                TableUtils.clearTable(getConnectionSource(), MagazineShot.class);
                downloadBanners();
                return;
            }


        } catch (SQLException e) {
            releaseWakeLock();
            Util.logError("Failed reading projects from database", e);
            hideProgress();
            return;
        }

        showProgress("Getting magazine data from SiSTeR Cloud...", true);
        startNextMagazineRequest(status);
    }

    private void releaseWakeLock() {
        if (updateWakeLock != null) {
            updateWakeLock.release();
            updateWakeLock = null;
        }
        isDownloadActive = false;
    }

    private void downloadImages() {
        if (imageDownloader != null) {
            imageDownloader.stop();
            imageDownloader = null;
        }


        imageDownloader = new ImageDownloader(this, getHelper(), new ImageDownloader.ImageDownloaderListener() {
            @Override
            public void imageDownloadFailed() {
                releaseWakeLock();
                imageDownloader.stop();
                imageDownloader = null;
                hideProgress();
                AppDialogFragment.show(VinSearchActivity.this, "Download Error", "Failed downloading magazine images. Ensure WiFi is available and try again.");
            }

            @Override
            public void imageDownloadComplete() {
                if (imageDownloader != null) {
                    imageDownloader.stop();
                    imageDownloader = null;
                }
                downloadBanners();
            }

            @Override
            public void imageDownloadProgress(int total, int complete) {
                if (progressDialog != null) {
                    progressDialog.setMax(total);
                    progressDialog.setProgress(complete);
                }
            }
        });


        showProgress("Downloading images", false);
        imageDownloader.start();
    }

    private void startNextMagazineRequest(MagazineDownloadStatus status) {
        startMagazineRequest(status);
    }

    private void startMagazineRequest(final MagazineDownloadStatus status) {
        if (status.isComplete()) {
            downloadImages();
            return;
        }

        magazineRequest = new ESWebService().getMagazines(this, status.currentProject());
        magazineRequest.enqueue(new Callback<MagazineResults>() {
            @Override
            public void onResponse(Call<MagazineResults> call, Response<MagazineResults> response) {
                magazineRequest = null;

                final int httpStatus = response.code();
                if (httpStatus < 200 || httpStatus > 299) {
                    releaseWakeLock();
                    Util.logError("HTTP error getting magazines: " + httpStatus);
                    hideProgress();
                    displayDownloadErrorText();
                    return;
                }

                final MagazineResults results = response.body();
                if (results == null) {
                    releaseWakeLock();
                    Util.logError("No response from magazine request");
                    hideProgress();
                    displayDownloadErrorText();
                    return;
                }

                if (results.hits == null || results.hits.hits == null || results.hits.hits.isEmpty()) {
                    // It is OK to have no magazines so just go on
                    status.next();
                    startNextMagazineRequest(status);
                    return;
                }

                new MagazineUpdateTask(status).execute(results);
            }

            @Override
            public void onFailure(Call<MagazineResults> call, Throwable t) {
                releaseWakeLock();
                magazineRequest = null;
                Util.logError("Failed getting magazines", t);
                hideProgress();
                displayDownloadErrorText();

            }

        });
    }

    private void displayDownloadErrorText() {
        AppDialogFragment.show(VinSearchActivity.this, "Error", "Failed downloading magazines. Check network settings and try again.");
    }

    private void downloadBanners() {
        BannerDownloadStatus status = new BannerDownloadStatus();
        try {
            Dao<Project, ?> dao = getHelper().getProjectDao();
            for (Project project : dao) {
                status.projects.add(project.getProject());
            }

            if (status.projects.isEmpty()) {
                downloadInventory();
                return;
            }
        } catch (SQLException e) {
            releaseWakeLock();
            Util.logError("Failed reading projects from database", e);
            hideProgress();
            return;
        }

        showProgress("Getting banner data from SiSTeR Cloud...", true);
        startBannerRequest(status);
    }

    private void startBannerRequest(final BannerDownloadStatus status) {
        if (status.isBannerInfoComplete()) {
            downloadBannerImages(status);
            return;
        }

        bannerRequest = new ESWebService().getBannerSettings(this, status.currentProject());
        bannerRequest.enqueue(new Callback<BannerResults>() {
            @Override
            public void onResponse(Call<BannerResults> call, Response<BannerResults> response) {
                bannerRequest = null;

                final int httpStatus = response.code();
                if (httpStatus < 200 || httpStatus > 299) {
                    releaseWakeLock();
                    Util.logError("HTTP error getting banners: " + httpStatus);
                    hideProgress();
                    displayError();
                    return;
                }

                final BannerResults results = response.body();
                if (results == null) {
                    releaseWakeLock();
                    Util.logError("No response from banner request");
                    hideProgress();
                    displayError();
                    return;
                }

                if (results.hits == null || results.hits.hits == null || results.hits.hits.isEmpty()) {
                    // It is OK to have no banners so just go on
                    status.next();
                    startBannerRequest(status);
                    return;
                }

                final BannerResults.BannerHit hit = results.hits.hits.get(0);
                if (hit._source == null || TextUtils.isEmpty(hit._source.banner_url)) {
                    Util.logInfo("Missing banner URL");
                    status.next();
                    startBannerRequest(status);
                    return;
                }

                final String url = hit._source.banner_url;
                status.addURL(status.currentProject(), url);
                status.next();
                startBannerRequest(status);
            }

            @Override
            public void onFailure(Call<BannerResults> call, Throwable t) {
                releaseWakeLock();
                bannerRequest = null;
                Util.logError("Failed getting banners", t);
                hideProgress();
                displayError();
            }

            private void displayError() {
                AppDialogFragment.show(VinSearchActivity.this, "Error", "Failed downloading banner images. Check network settings and try again.");
            }
        });
    }

    private void downloadBannerImages(BannerDownloadStatus status) {
        if (bannerDownloader != null) {
            bannerDownloader.stop();
            bannerDownloader = null;
        }

        if (status.urls.isEmpty()) {
            downloadInventory();
        }

        bannerDownloader = new BannerDownloader(this, status.urls, new BannerDownloader.BannerDownloaderListener() {
            @Override
            public void bannerDownloadFailed() {
                releaseWakeLock();
                if (bannerDownloader != null) {
                    bannerDownloader.stop();
                    bannerDownloader = null;
                }
                hideProgress();
                AppDialogFragment.show(VinSearchActivity.this, "Download Error", "Failed downloading banner images. Ensure WiFi is available and try again.");
            }

            @Override
            public void bannerDownloadComplete() {
                if (bannerDownloader != null) {
                    bannerDownloader.stop();
                    bannerDownloader = null;
                }
                downloadInventory();
            }
        });
        bannerDownloader.start();
    }

    private void downloadInventory() {
        DownloadStatus status = new DownloadStatus();

        try {
            Dao<Project, ?> dao = getHelper().getProjectDao();
            for (Project project : dao) {
                status.projects.add(project.getProject());
            }

            // If we have at least one project, we'll wait until the results come back to delete
            // current vehicles. Otherwise delete existing vehicles here.
            if (status.projects.isEmpty()) {
                hideProgress();
                releaseWakeLock();
                TableUtils.clearTable(getConnectionSource(), Vehicle.class);
                return;
            }
        } catch (SQLException e) {
            Util.logError("Failed reading projects from database", e);
        }

        startCountRequest(status);
    }

    private void showProgress(String message, boolean indeterminate) {
        hideProgress();

        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(indeterminate);
        progressDialog.setProgressStyle(indeterminate ? ProgressDialog.STYLE_SPINNER : ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    private void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    private void updateProgress(DownloadStatus status) {
        StringBuilder builder = new StringBuilder();
        builder.append("Downloading inventory\n");

        builder.append("for ");
        if (status.current < status.projects.size()) {
            builder.append(status.projects.get(status.current));
        }
        builder.append('\n');

        builder.append("On vehicle: ");
        if (status.totalVehicles > 0) {
            builder.append(status.downloadedVehicles + 1);
            builder.append(" of total ");
            builder.append(status.totalVehicles);
        }

        if (progressDialog != null) {
            progressDialog.setMessage(builder.toString());
        }
    }

    /**
     * Iterate through the projects and get just the counts so we can display progress as we download
     * the full vehicle data.
     */
    private void startCountRequest(final DownloadStatus status) {
        showProgress("Getting vehicle data from SiSTeR Cloud...", true);


        if (status.isComplete()) {
            // We have the total count, so start downloading vehicles
            status.reset();
            startVehicleRequest(status);
            return;
        }

        final String project = status.currentProject();
        status.next();

        // We only care about count here so just ask for a single result.
        inventoryRequest = new ESWebService().getInventory(this, 1, project);
        inventoryRequest.enqueue(new Callback<InventoryResults>() {
            @Override
            public void onResponse(Call<InventoryResults> call, Response<InventoryResults> response) {
                inventoryRequest = null;
                final InventoryResults results = response.body();
                if (results != null && results.hits != null) {
                    status.totalVehicles += results.hits.total;
                }

                startCountRequest(status);
            }

            @Override
            public void onFailure(Call<InventoryResults> call, Throwable t) {
                inventoryRequest = null;
                releaseWakeLock();
                Util.logError("Failed getting counts", t);
                hideProgress();
                AppDialogFragment.show(VinSearchActivity.this, R.string.dialog_inventory_download_error_title, R.string.dialog_inventory_download_error_message);
            }
        });
    }

    private void startVehicleRequest(final DownloadStatus status) {
        if (status.isComplete()) {
            releaseWakeLock();
            hideProgress();
            return;
        }

        updateProgress(status);
        inventoryRequest = new ESWebService().getInventory(this, 3000, status.currentProject());
        inventoryRequest.enqueue(new Callback<InventoryResults>() {
            @Override
            public void onResponse(Call<InventoryResults> call, Response<InventoryResults> response) {
                inventoryRequest = null;
                final InventoryResults results = response.body();
                new InventoryUpdateTask(status).execute(results);
            }

            @Override
            public void onFailure(Call<InventoryResults> call, Throwable t) {
                releaseWakeLock();
                inventoryRequest = null;
                Util.logError("Failed getting vehicles for project " + status.currentProject(), t);
                hideProgress();
                showDownloadErrorDialog();
            }
        });
    }

    private void showDownloadErrorDialog() {
        AppDialogFragment.show(this, R.string.dialog_inventory_download_error_title, R.string.dialog_inventory_download_error_message);
    }

    private void findVehicle(String vin) {
        if (vin.length() == 0) {
            AppDialogFragment.show(this, R.string.dialog_vin_search_empty_vin_title);
            return;
        }

        dismissKeyboard();

        try {
            final Vehicle vehicle = getHelper().getVehicleDao().queryForId(vin);

            // We usually won't find a match here because we are trying to find a unique match
            // as the user is typing. We would only get a match here if the user typed a string
            // that matched one string exactly and another partially, e.g., a stock number that
            // is also part of a VIN for another vehicle.
            if (vehicle != null) {
                startVehicleActivity(vin, vehicle.getProject(), vehicle.getLibrary());
            } else {
                CreateVehicleDialogFragment dialog = CreateVehicleDialogFragment.newInstance(vin);
                dialog.show(getFragmentManager(), "create");
            }
        } catch (SQLException e) {
            Util.logError("Error querying for vehicle by VIN", e);
        }
    }

    private void dismissKeyboard() {
        final View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager manager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            manager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @SuppressWarnings("SpellCheckingInspection")
    private String generateTestVin() {
        return "TEST_" + new SimpleDateFormat("MMddkkmmss", Locale.US).format(new Date());
    }

    private String generateCameraOnlyVin() {
        return "CAMERA_TEST";
    }

    private void addVehicle(final String vin, final boolean isCameraOnly) {
        try {
            final List<Project> projects = getHelper().getProjectDao().queryForAll();
            if (projects.size() == 1) {
                addVehicle(vin, projects.get(0), isCameraOnly);
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Please select a dealer");

                CharSequence[] dealers = new CharSequence[projects.size() + 1];
                for (int i = 0; i < projects.size(); i++) {
                    dealers[i] = projects.get(i).getDealerName();
                }
                dealers[dealers.length - 1] = "I don't know";

                builder.setItems(dealers, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i < projects.size()) {
                            final Project project = projects.get(i);
                            addVehicle(vin, project, isCameraOnly);
                        } else {
                            AppDialogFragment.show(VinSearchActivity.this, "Dealer Required", "You must select a dealer to add a VIN.");
                        }
                    }
                });
                builder.show();
            }

        } catch (SQLException e) {
            Util.logError("Database error querying projects", e);
            AppDialogFragment.show(this, R.string.dialog_add_vehicle_error_title, R.string.dialog_add_vehicle_error_message);
        }
    }

    private void addVehicle(String vin, Project project, boolean isCameraOnly) {
        try {
            final String library = project.getLibrary().replace(",", "");
            final String projectID = project.getProject().replace(",", "");

            getHelper().getVehicleDao().createIfNotExists(new Vehicle(vin, projectID, library, isCameraOnly));
            startVehicleActivity(vin, projectID, library);
        } catch (SQLException e) {
            Util.logError("Database error adding new VIN", e);
            AppDialogFragment.show(this, R.string.dialog_add_vehicle_error_title, R.string.dialog_add_vehicle_error_message);
        }
    }

    private void startVehicleActivity(String vin, String project, String library) {
        AppSharedPreferences sharedPreferences = new AppSharedPreferences(this);
        if (shareInputCheckBox.isChecked() && sharedPreferences.acceptSharedInput()) {
            final InterDeviceCommand command = createNearbyCommand(vin, project, library);
            command.data.command  = InterDeviceCommand.COMMAND_CHANGE_VIN;
            Util.logInfo("Master: Sent udp message: " + command.data.command);

            NetworkService.sendMessage(this, command);
        }

        Timber.i("startVehicleActivity master/slave: %b master: %b trigger slaves: %b", sharedPreferences.isMasterSlaveEnabled(), sharedPreferences.isMaster(), sharedPreferences.triggerSlaves());
        if (sharedPreferences.isMasterSlaveEnabled() && sharedPreferences.isMaster() && sharedPreferences.triggerSlaves()) {
            final InterDeviceCommand command = createNearbyCommand(vin, project, library);
            command.data.command  = InterDeviceCommand.COMMAND_CHANGE_SLAVE_VIN;
            Util.logInfo("Master: Sent udp message: " + command.data.command);

            NetworkService.sendMessage(this, command);
        }

        startActivity(VehicleActivity.createIntent(VinSearchActivity.this, vin));
    }

    private InterDeviceCommand createNearbyCommand(String vin, String project, String library) {
        final InterDeviceCommand command = new InterDeviceCommand();

        InterDeviceCommand.CommandData commandData = new InterDeviceCommand.CommandData();
        commandData.vin = vin;
        commandData.project = project;
        commandData.library = library;
        commandData.isCameraOnly = cameraOnlyModeCheckBox.isChecked();
        command.data = commandData;

        return command;
    }

    private void startCurrentVersionRequest() {
        currentVersionRequest = WebService.getDebugApi(this).getCurrentVersion();
        currentVersionRequest.enqueue(new Callback<CurrentVersion>() {
            @Override
            public void onResponse(Call<CurrentVersion> call, Response<CurrentVersion> response) {
                currentVersionRequest = null;
                final CurrentVersion currentVersion = response.body();
                if (currentVersion != null) {
                    try {
                        PackageInfo pInfo = VinSearchActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
                        int versionCode = pInfo.versionCode;
                        if (versionCode < currentVersion.versionCode) {
                            UpgradeAppDialogFragment dialog = UpgradeAppDialogFragment.newInstance(currentVersion.versionName);
                            dialog.show(getFragmentManager(), "create");
                        }
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<CurrentVersion> call, Throwable t) {
                currentVersionRequest = null;
            }

        });
    }

    @Override
    public void onUpgradeAppPositiveClick() {
        String appPackageName = getPackageName();
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
    }

    @Override
    public void onUpgradeAppNegativeClick() {

    }
}
