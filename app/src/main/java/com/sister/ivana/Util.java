package com.sister.ivana;


import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.provider.Settings;
import android.text.TextUtils;

import com.j256.ormlite.dao.Dao;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.OrmLiteBaseAppCompatActivity;
import com.sister.ivana.database.Project;
import com.squareup.otto.Bus;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

import timber.log.Timber;

import static android.content.Context.WIFI_SERVICE;

public class Util {
    public static final int VIN_LENGTH = 17;

    private static final Bus bus = new Bus();

    public static boolean connectedToTheta(Context context) {
        final WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
        return wifiManager.getConnectionInfo().getSSID().contains("THETA");
    }

    // Prefer using Timber methods directly (Timber.i(), Timber.d(), etc.) over these methods
    // as the Timber versions automatically generate a tag indicating the class from which the
    // log was generated. These methods were created before Timber was added to the project.
    public static void logError(String message) {
        Timber.e(message);
    }

    public static void logInfo(String message) {
        Timber.i(message);
    }

    public static void logError(String message, Throwable throwable) {
        if (throwable != null) {
            Timber.e(throwable, message);
        } else {
            Timber.e(message);
        }
    }

    public static void logout(OrmLiteBaseAppCompatActivity<DatabaseHelper> activity) {
        try {
            Dao<Project, ?> dao = activity.getHelper().getProjectDao();
            for (Project project : dao) {
                String projectId = project.getProject();
                File banner = FileUtil.getBannerFile(projectId);
                if (banner != null && banner.exists()) {
                    if (!banner.delete()) {
                        Timber.w("Failed deleting banner file %s", banner.getAbsolutePath());
                    }
                }
            }
        } catch (SQLException e) {
            // Log but keep going because this isn't catastrophic. Most likely we'll copy over
            // the banner when we login again and the worst case is a stale banner image.
            Util.logError("Failed reading projects from database during banner delete", e);
        }

        activity.getHelper().clearTables();
        final AppSharedPreferences preferences = new AppSharedPreferences(activity);
        preferences.resetUser();
        preferences.resetMasterSlave();

        Intent intent = new Intent(activity, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        activity.startActivity(intent);
    }

    public static Bus getBus() {
        return bus;
    }

    public static String getVehicleDisplayString(String year, String make, String model) {
        StringBuilder builder = new StringBuilder();

        if (!TextUtils.isEmpty(year)) {
            builder.append(year);
        }
        if (!TextUtils.isEmpty(make)) {
            if (builder.length() > 0) {
                builder.append(' ');
            }
            builder.append(make);
        }
        if (!TextUtils.isEmpty(model)) {
            if (builder.length() > 0) {
                builder.append(' ');
            }
            builder.append(' ');
            builder.append(model);
        }

        return builder.toString();
    }

    public static String enforceNonNull(String string) {
        return string != null ? string : "";
    }

    public static String getLibrary(DatabaseHelper helper) {
        String library = null;

        try {
            // All projects associated with a username should belong to the same library
            // so just get the library from the first one.
            List<Project> projects = helper.getProjectDao().queryForAll();
            if (projects != null && !projects.isEmpty()) {
                library = projects.get(0).getLibrary();
            }
        } catch (SQLException e) {
            Timber.e(e, "Error querying for library");
        }

        return library != null ? library : "";
    }

    public static String getLogDeviceID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static boolean shouldSendSlaveCommand(Context context) {
        final AppSharedPreferences preferences = new AppSharedPreferences(context);
        return preferences.isMasterSlaveEnabled()
                && preferences.isMaster()
                && preferences.triggerSlaves();
    }
}
