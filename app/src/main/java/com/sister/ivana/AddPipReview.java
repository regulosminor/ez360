package com.sister.ivana;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.sister.ivana.dialog.AppDialogFragment;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import timber.log.Timber;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class AddPipReview extends AppDialogFragment implements DeletePhotoDialogFragment.DeletePhotoListener {
    public interface AddPipReviewListener {
        void onAddPipReviewSubmit();
        void onAddPipReviewCancel();
    }

    @BindView(R.id.topContainer) ViewGroup topContainer;
    @BindView(R.id.imageView) ImageView imageView;
    @BindView(R.id.insetImageView) ImageView insetImageView;

    private static final String KEY_WIDE_IMAGE_FILENAME = "KEY_WIDE_IMAGE_FILENAME";
    private static final String KEY_DETAIL_IMAGE_FILENAME = "KEY_DETAIL_IMAGE_FILENAME";
    private Unbinder unbinder;
    private String wideImagePathname;
    private String detailImagePathname;

    public static AddPipReview newInstance(String wideImagePathname, String detailImagePathname) {
        AddPipReview fragment = new AddPipReview();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, R.string.dialog_add_exterior_poi_review_title);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_done);
        AppDialogFragment.setNegativeButtonText(args, R.string.dialog_button_cancel);
        AppDialogFragment.setContentView(args, R.layout.dialog_add_pip_review);
        args.putString(KEY_WIDE_IMAGE_FILENAME, wideImagePathname);
        args.putString(KEY_DETAIL_IMAGE_FILENAME, detailImagePathname);
        fragment.setArguments(args);
        return fragment;
    }

    public void onResume() {
        super.onResume();

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

        int width = (int) (getResources().getDimension(R.dimen.shot_selector_width));
        width += (int) Math.ceil(32 * metrics.density);

        getDialog().getWindow().setLayout(width, WRAP_CONTENT);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);

        wideImagePathname = getArguments().getString(KEY_WIDE_IMAGE_FILENAME);
        detailImagePathname = getArguments().getString(KEY_DETAIL_IMAGE_FILENAME);

        Picasso picasso = Picasso.get();

        File detailImageFile = new File(detailImagePathname);
        picasso.load(detailImageFile)
                .fit()
                .into(imageView);

        File wideImageFile = new File(wideImagePathname);
        picasso.load(wideImageFile)
                .fit()
                .into(insetImageView);

        return dialog;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        dismiss();

        if (i == DialogInterface.BUTTON_NEGATIVE) {
            deleteImages();
            final AddPipReviewListener listener = (AddPipReviewListener) getActivity();
            listener.onAddPipReviewCancel();

        } else if (i == DialogInterface.BUTTON_POSITIVE) {
            final AddPipReviewListener listener = (AddPipReviewListener) getActivity();
            listener.onAddPipReviewSubmit();
        }

        dismiss();
    }

    @OnClick(R.id.button_delete)
    void onDeleteClick() {
        final DeletePhotoDialogFragment fragment = DeletePhotoDialogFragment.newInstance();
        fragment.setTargetFragment(this, 1);
        fragment.show(getFragmentManager(), "delete_photo");
    }

    @Override
    public void onDeletePhotoPositiveClick() {
        deleteImages();
        final AddPipReviewListener listener = (AddPipReviewListener) getActivity();
        listener.onAddPipReviewCancel();
    }

    @Override
    public void onDeletePhotoNegativeClick() {
    }

    private void deleteImages() {
        if (new File(wideImagePathname).delete()) {
            Timber.i("user deleted picture for pip wide image: %s", wideImagePathname);
        } else {
            Timber.e("failed deleting picture for pip wide image: %s", wideImagePathname);
        }

        if (new File(detailImagePathname).delete()) {
            Timber.i("user deleted picture for pip detail image: %s", detailImagePathname);
        } else {
            Timber.e("failed deleting picture for pip detail image: %s", detailImagePathname);
        }
    }
}
