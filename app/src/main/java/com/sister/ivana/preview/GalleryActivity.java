package com.sister.ivana.preview;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.adobe.xmp.XMPException;
import com.adobe.xmp.XMPMeta;
import com.sister.ivana.ActionBarUploadStatus;
import com.sister.ivana.AppSharedPreferences;
import com.sister.ivana.PanoViewActivity;
import com.sister.ivana.R;
import com.sister.ivana.SettingsActivity;
import com.sister.ivana.Util;
import com.sister.ivana.WebViewActivity;
import com.sister.ivana.XmpUtil;
import com.sister.ivana.database.DatabaseHelper;
import com.sister.ivana.database.OrmLiteBaseAppCompatActivity;
import com.sister.ivana.upload.PendingMediaCounts;
import com.sister.ivana.upload.UploadQueue;
import com.sister.ivana.upload.UploadType;
import com.sister.ivana.upload.UploaderService;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardGridView;

public class GalleryActivity extends OrmLiteBaseAppCompatActivity<DatabaseHelper> {
    public enum Mode {
        NONE(0),
        DETAIL_PICTURES(1),
        EZ_360(2),
        SPINIT_PICTURES(3),
        FULLMO_VIDEO(4);

        private final int code;

        Mode(int code) {
            this.code = code;
        }

        UploadType getUploadType() {
            switch (this) {
                case DETAIL_PICTURES:
                    return UploadType.DETAIL_IMAGES;

                case EZ_360:
                    return UploadType.EZ360;

                case SPINIT_PICTURES:
                    return UploadType.SPIN_IT_IMAGES;

                case FULLMO_VIDEO:
                    return UploadType.FULL_MOTION_VIDEO;

                default:
                    return null;
            }
        }

        int getCode() {
            return code;
        }

        static Mode create(int code) {
            switch (code) {
                case 1:
                    return DETAIL_PICTURES;
                case 2:
                    return EZ_360;
                case 3:
                    return SPINIT_PICTURES;
                case 4:
                    return FULLMO_VIDEO;
                default:
                    return NONE;
            }
        }
    }

    class FullScreenCard extends Card {
        @BindView(R.id.vehicle_card_image) ImageView image;
        @BindView(R.id.vehicle_card_video) ImageView video;
        @BindView(R.id.countText) TextView countText;
        @BindView(R.id.selectAlphaLayer) ImageView alphaLayer;

        public final String path;
        public final String count;
        public boolean checked;

        public FullScreenCard(Context context, String path, String count) {
            super(context, R.layout.image_listcard_innerlayout);
            this.count = count == null ? "" : count;
            this.path = path == null ? "" : path;
        }

        @Override
        public void setupInnerViewElements(ViewGroup parent, View view) {
            ButterKnife.bind(this, parent);

            if (checked) {
                image.setBackgroundColor(getColor(android.R.color.holo_blue_light));
                video.setBackgroundColor(getColor(android.R.color.holo_blue_light));
            } else {
                image.setBackgroundColor(0x0);
                video.setBackgroundColor(0x0);
            }

            if (isImageFile(path)) {
                video.setVisibility(View.INVISIBLE);
                image.setVisibility(View.VISIBLE);
                try {
                    Picasso.get().load("file://" + path).into(image);
                } catch (Exception e) {
                    Util.logError("Error loading image file " + path, e);
                }
            } else if (isVideoFile(path)) {
                video.setVisibility(View.VISIBLE);
                image.setVisibility(View.INVISIBLE);
                Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Images.Thumbnails.MINI_KIND);
                if (bitmap != null) {
                    video.setImageBitmap(bitmap);
                }
            }
            countText.setText(this.count);
        }
    }

    private static final String EXTRA_VIN = "Ivana.EXTRA_VIN";
    private static final String EXTRA_PROJECT = "Ivana.EXTRA_PROJECT";
    private static final String EXTRA_MODE = "Ivana.EXTRA_MODE";

    @BindView(R.id.fullscreenImage) ImageView fullScreenImage;
    @BindView(R.id.picsGalleryButton) ImageButton picsGalleryButton;
    @BindView(R.id.ez360GalleryButton) ImageButton ez360GalleryButton;
    @BindView(R.id.spinitGalleryButton) ImageButton spinitGalleryButton;
    @BindView(R.id.fullmoGalleryButton) ImageButton fullmoGalleryButton;
    @BindView(R.id.mediaLayerButton) ImageButton mediaLayerButton;
    @BindView(R.id.numPicsText) TextView picsNumText;
    @BindView(R.id.num360Text) TextView ez360NumText;
    @BindView(R.id.numSpinitText) TextView spinItNumText;
    @BindView(R.id.numFullmoText) TextView fullmoNumText;
    @BindView(R.id.nocontentlayout) RelativeLayout noContentLayout;
    @BindView(R.id.galleryGrid) CardGridView gridView;

    private Mode mode;
    private ArrayList<Card> cards = new ArrayList<>();
    private String vin;
    private String project;
    private ActionBarUploadStatus actionBarStatus;
    private final ArrayList<ImageButton> selectionButtons = new ArrayList<>();

    public static Intent createIntent(Context context, String vin, String project) {
        return createIntent(context, vin, project, Mode.NONE);
    }

    public static Intent createIntent(Context context, String vin, String project, Mode mode) {
        Intent intent = new Intent(context, GalleryActivity.class);
        intent.putExtra(EXTRA_VIN, vin);
        intent.putExtra(EXTRA_PROJECT, project);
        intent.putExtra(EXTRA_MODE, mode.getCode());
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_gallery_grid);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        selectionButtons.clear();
        selectionButtons.add(spinitGalleryButton);
        selectionButtons.add(ez360GalleryButton);
        selectionButtons.add(fullmoGalleryButton);
        selectionButtons.add(picsGalleryButton);

        vin = getIntent().getStringExtra(EXTRA_VIN);
        project = getIntent().getStringExtra(EXTRA_PROJECT);
        mode = Mode.create(getIntent().getIntExtra(EXTRA_MODE, Mode.NONE.getCode()));
    }

    @Override
    protected void onStart() {
        super.onStart();
        Util.getBus().register(this);
        actionBarStatus = new ActionBarUploadStatus();
        actionBarStatus.onStart(this);

        PendingMediaCounts counts = UploadQueue.getCounts(getHelper(), vin);
        updateCounts(counts);
        invalidateOptionsMenu();
    }

    @Override
    protected void onStop() {
        super.onStop();
        actionBarStatus.onStop();
        Util.getBus().unregister(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        final AppSharedPreferences preferences = new AppSharedPreferences(this);

        switch (item.getItemId()) {
            case R.id.action_delete_vehicles:
                deleteSelectedItems();
                return true;

            case R.id.action_logout:
                Util.logout(this);
                return true;

            case R.id.action_enable_upload:
                preferences.putUploadsEnabled(true);
                invalidateOptionsMenu();
                return true;

            case R.id.action_disable_upload:
                preferences.putUploadsEnabled(false);
                invalidateOptionsMenu();
                return true;

            case R.id.action_settings:
                startActivity(new Intent(this, com.sister.ivana.settings.SettingsActivity.class));
//                startActivity(new Intent(this, SettingsActivity.class));
                return true;

            default:
                return false;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.multi_select_gallery_list, menu);

        final AppSharedPreferences preferences = new AppSharedPreferences(this);
        MenuItem enableUploads = menu.findItem(R.id.action_enable_upload);
        enableUploads.setVisible(!preferences.areUploadsEnabled());
        MenuItem disableUploads = menu.findItem(R.id.action_disable_upload);
        disableUploads.setVisible(preferences.areUploadsEnabled());

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        final Spinner spinner = (Spinner) (menu.findItem(R.id.spinner_item).getActionView());
        prepareSpinner(spinner);
        return true;
    }

    @OnClick(R.id.picsGalleryButton)
    void onPicsGalleryButtonClick() {
        setMode(Mode.DETAIL_PICTURES);
    }

    @OnClick(R.id.ez360GalleryButton)
    void onEZ360GalleryButtonClick() {
        setMode(Mode.EZ_360);
    }

    @OnClick(R.id.spinitGalleryButton)
    void onSpinitGalleryButtonClick() {
        setMode(Mode.SPINIT_PICTURES);
    }

    @OnClick(R.id.fullmoGalleryButton)
    void onFullmoGalleryButtonClick() {
        setMode(Mode.FULLMO_VIDEO);
    }

    @OnClick(R.id.mediaLayerButton)
    void onMediaLayerButtonClick() {
        loadMediaLayer();
    }

    @SuppressWarnings("unused")
    @Subscribe
    public void onUpdateCounts(PendingMediaCounts counts) {
        if (counts.getVin().contentEquals(vin)) {
            updateCounts(counts);
        }
    }

    private static long getCountsForMode(PendingMediaCounts counts, Mode mode) {
        switch (mode) {
            case DETAIL_PICTURES:
                return counts.getPicturesCount();
            case EZ_360:
                return counts.getEz360PictureCount();
            case SPINIT_PICTURES:
                return counts.getSpinItPictureCount();
            case FULLMO_VIDEO:
                return counts.getFullMotionVideoCount();
            default:
                return 0;
        }
    }

    private void updateCounts(PendingMediaCounts counts) {
        updateCount(picsNumText, picsGalleryButton, counts.getPicturesCount());
        updateCount(ez360NumText, ez360GalleryButton, counts.getEz360PictureCount());
        updateCount(spinItNumText, spinitGalleryButton, counts.getSpinItPictureCount());
        updateCount(fullmoNumText, fullmoGalleryButton, counts.getFullMotionVideoCount());

        if (getCountsForMode(counts, mode) == 0) {
            // Handle case where there is no media for selected mode
            mode = getAutoSelectMediaType(counts);
        }

        setMode(mode);
    }

    /*
     * Automatically pick media type based on what is available.
     */
    private static Mode getAutoSelectMediaType(PendingMediaCounts counts) {
        if (counts.getPicturesCount() > 0) {
            return Mode.DETAIL_PICTURES;
        }
        if (counts.getEz360PictureCount() > 0) {
            return Mode.EZ_360;
        } else if (counts.getSpinItPictureCount() > 0) {
            return Mode.SPINIT_PICTURES;
        } else if (counts.getFullMotionVideoCount() > 0) {
            return Mode.FULLMO_VIDEO;
        } else {
            return Mode.NONE;
        }
    }

    private void updateCount(TextView textView, ImageButton button, long count) {
        if (count > 0) {
            textView.setText(String.valueOf(count));
            textView.setVisibility(View.VISIBLE);
            button.setEnabled(true);
            button.setAlpha(1f);
        } else {
            textView.setVisibility(View.INVISIBLE);
            button.setEnabled(false);
            button.setAlpha(0.3f);
            button.setBackgroundColor(getColor(R.color.translucent_gray));
        }
    }

    private void setMode(Mode mode) {
        this.mode = mode;
        updatedSelectedButtonStyle();

        cards = createCards();
        noContentLayout.setVisibility(cards.isEmpty() ? View.VISIBLE : View.INVISIBLE);
        gridView.setAdapter(new CardArrayAdapter(this, cards));
        invalidateOptionsMenu();
    }

    private ArrayList<Card> createCards() {
        ArrayList<Card> imageCards = new ArrayList<>();
        if (mode == Mode.NONE) {
            return imageCards;
        }

        List<String> files = UploadQueue.getMediaFiles(getHelper(), mode.getUploadType(), vin);
        for (int i = 0; i < files.size(); i++) {
            FullScreenCard card = createFullScreenCard(i + 1, new File(files.get(i)));
            imageCards.add(card);
        }
        return imageCards;
    }

    @NonNull
    private FullScreenCard createFullScreenCard(int count, File file) {
        FullScreenCard card = new FullScreenCard(GalleryActivity.this,
                file.getAbsolutePath(), String.valueOf(count));
        card.setBackgroundColorResourceId(R.color.black);
        card.setShadow(false);

        card.setOnLongClickListener(new Card.OnLongCardClickListener() {
            @Override
            public boolean onLongClick(Card card, View view) {
                onCardLongClick((FullScreenCard) card);
                return true;
            }

        });

        card.setOnClickListener(new Card.OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                onCardClick((FullScreenCard) card, view);
            }
        });
        return card;
    }

    private void onCardClick(FullScreenCard card, View view) {
        if (card.checked) {
            view.findViewById(R.id.vehicle_card_video).setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
            view.findViewById(R.id.vehicle_card_image).setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
            card.checked = false;
        } else {
            view.findViewById(R.id.vehicle_card_image).setBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_blue_light));
            view.findViewById(R.id.vehicle_card_video).setBackgroundColor(ContextCompat.getColor(this, android.R.color.holo_blue_light));
            card.checked = true;
        }

        invalidateOptionsMenu();
    }

    private void onCardLongClick(FullScreenCard card) {
        if (isImageFile(card.path)) {
            if (card.path.toLowerCase().contains("ez360")) {
                launchPanoViewer(card.path);
            } else {
                try {
                    Picasso.get().load("file://" + card.path).into(fullScreenImage);
                    fullScreenImage.setVisibility(View.VISIBLE);

                    fullScreenImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            fullScreenImage.setVisibility(View.INVISIBLE);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else if (isVideoFile(card.path)) {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse("file://" + card.path), URLConnection.guessContentTypeFromName(card.path));
            startActivity(intent);
        }
    }

    private ImageButton getSelectedButton() {
        switch (mode) {
            case DETAIL_PICTURES:
                return picsGalleryButton;

            case EZ_360:
                return ez360GalleryButton;

            case FULLMO_VIDEO:
                return fullmoGalleryButton;

            case SPINIT_PICTURES:
                return spinitGalleryButton;

            case NONE:
                return null;

            default:
                return null;
        }
    }

    private void updatedSelectedButtonStyle() {
        final int selectedColor = getColor(R.color.holo_blue);
        final ImageButton selected = getSelectedButton();

        for (ImageButton button : selectionButtons) {
            final int color;
            if (button == selected) {
                color = selectedColor;
            } else {
                color = 0;
            }
            button.setBackgroundColor(color);
        }
    }

    private void prepareSpinner(Spinner spinner) {
        final String numSelected = String.valueOf(getCheckedCards().size());
        String[] selectOptions = {numSelected + " selected", "select all", " select none"};
        ArrayAdapter<String> adapter = new ArrayAdapter<>(GalleryActivity.this,
                android.R.layout.simple_spinner_dropdown_item, selectOptions);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 1:
                        selectAll();
                        break;

                    case 2:
                        selectNone();
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void selectNone() {
        for (Card card : cards) {
            card.setShadow(false);
            ((FullScreenCard) card).checked = false;
        }
        CardArrayAdapter adapter = (CardArrayAdapter) gridView.getAdapter();
        adapter.notifyDataSetChanged();
    }

    private void selectAll() {
        for (Card card : cards) {
            ((FullScreenCard) card).checked = true;
        }
        CardArrayAdapter adapter = (CardArrayAdapter) gridView.getAdapter();
        adapter.notifyDataSetChanged();
    }

    private boolean isVideoFile(String path) {
        return path.toLowerCase().contains(".mp4") || path.toLowerCase().contains(".3gp");
    }

    private boolean isImageFile(String path) {
        return path.toLowerCase().contains(".jpg") || path.toLowerCase().contains(".jpeg");
    }

    private void launchPanoViewer(String pathname) {
        //launches the panoramic viewer provided by google play libraries (see gradle build file for
        // list of gms dependencies

        //get the raw 360 image file to pass as param to panoramic viewer activity
        File thetaImage = new File(pathname);
        String thetaUri = "file://";
        if (thetaImage.exists()) {
            thetaUri += thetaImage.getAbsolutePath();
        }

        // in order for the google pano viewer to work, we must add this XMP data to the image file
        XMPMeta xmpMeta = XmpUtil.createXMPMeta();

        String xlmns = "http://ns.google.com/photos/1.0/panorama/";
        try {

            xmpMeta.setProperty(xlmns, "GPano:ProjectionType", "equirectangular");
            xmpMeta.setProperty(xlmns, "GPano:CroppedAreaLeftPixels", 0);
            xmpMeta.setProperty(xlmns, "GPano:CroppedAreaTopPixels", 0);
            xmpMeta.setProperty(xlmns, "GPano:CroppedAreaImageWidthPixels", 2048);
            xmpMeta.setProperty(xlmns, "GPano:CroppedAreaImageHeightPixels", 1024);
            xmpMeta.setProperty(xlmns, "GPano:FullPanoWidthPixels", 2048);
            xmpMeta.setProperty(xlmns, "GPano:FullPanoHeightPixels", 1024);
        } catch (XMPException e) {
            e.printStackTrace();
        }
        XmpUtil.writeXMPMeta(thetaImage.getAbsolutePath(), xmpMeta);
        Intent intent = new Intent(GalleryActivity.this, PanoViewActivity.class);
        intent.putExtra("pano_uri", thetaUri);
        startActivity(intent);
    }

    private void loadMediaLayer() {
        Intent mediaLayerWebView = new Intent(GalleryActivity.this, WebViewActivity.class);
        mediaLayerWebView.putExtra("url_extra", "http://medialayer.sister.tv/production/medialayer.html?vin=" + vin + "&dealerid=" + project);
        startActivity(mediaLayerWebView);
    }

    private void deleteSelectedItems() {
        ArrayList<String> deletes = new ArrayList<>();

        ArrayList<Card> items = getCheckedCards();
        for (Card item : items) {
            deletes.add(((GalleryActivity.FullScreenCard) item).path);
        }

        if (!deletes.isEmpty()) {
            // Defer to the uploader service to actually do the delete. The request may not be
            // honored, for example if the the file has already been uploaded or is in the
            // process of being uploaded.
            final Intent intent = UploaderService.createDeleteMediaIntent(this, deletes);
            startService(intent);
        }
    }

    private ArrayList<Card> getCheckedCards() {
        ArrayList<Card> checkedCards = new ArrayList<>();
        for (Card card : cards) {

            if (((FullScreenCard) card).checked) {
                checkedCards.add(card);
            }
        }
        return checkedCards;
    }
}