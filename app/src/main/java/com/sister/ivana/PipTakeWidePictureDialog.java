package com.sister.ivana;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import timber.log.Timber;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class PipTakeWidePictureDialog extends DialogFragment implements DialogInterface.OnClickListener {
    public interface PipTakeWidePictureListener {
        void onPipTakeWidePicturePositiveClick();
    }

    @BindView(R.id.positive_button) Button positiveButton;
    @BindView(R.id.negative_button) Button negativeButton;
    @BindView(R.id.dont_show_again_checkbox) CheckBox dontShowAgainCheckBox;

    private Unbinder unbinder;

    public static PipTakeWidePictureDialog newInstance() {
        return new PipTakeWidePictureDialog();
    }

    public void onResume() {
        super.onResume();

        final Rect frame = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        final int width = (int) (frame.width() * 0.8);
        getDialog().getWindow().setLayout(width, WRAP_CONTENT);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        if (i == DialogInterface.BUTTON_POSITIVE) {
            if (dontShowAgainCheckBox.isChecked()) {
                new AppSharedPreferences(getContext()).putDontShowPipMessage(true);
            }

            PipTakeWidePictureListener listener = (PipTakeWidePictureListener) getActivity();
            listener.onPipTakeWidePicturePositiveClick();
        }
        dismiss();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_pip_take_wide_picture, null);
        unbinder = ButterKnife.bind(this, view);

        final Dialog dialog = new Dialog(getContext(), R.style.AppDialogTheme);
        dialog.setContentView(view);

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PipTakeWidePictureDialog.this.onClick(dialog, DialogInterface.BUTTON_POSITIVE);
            }
        });

        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PipTakeWidePictureDialog.this.onClick(dialog, DialogInterface.BUTTON_NEGATIVE);
            }
        });

        return dialog;
    }

}
