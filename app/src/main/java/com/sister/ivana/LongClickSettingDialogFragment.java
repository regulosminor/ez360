package com.sister.ivana;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewTreeObserver;
import android.widget.EditText;

import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

public class LongClickSettingDialogFragment extends AppDialogFragment {

    public interface LongClickSettingListener {
        void onLongClickSettingPositiveClick(int magazineID, int requestCode);

        void onLongClickSettingNegativeClick();
    }

    private static final int MIN_CAPTURE_LENGTH = 1;
    private static final int MAX_CAPTURE_LENGTH = 8;

    private static final int MIN_NUMBER_DETAIL_PICS = 1;
    private static final int MAX_NUMBER_DETAIL_PICS = 8;

    private Unbinder unbinder;
    private AppSharedPreferences preferences;

    @BindView(R.id.max_capture_length_edittext)
    EditText maxCaptureLengthEditText;
    @BindView(R.id.pics_extracted_edittext)
    EditText picsExtractedEditText;

    public static LongClickSettingDialogFragment newInstance() {
        LongClickSettingDialogFragment fragment = new LongClickSettingDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, R.string.dialog_long_click_setting_title);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_next);
        AppDialogFragment.setNegativeButtonText(args, R.string.dialog_button_cancel);
        AppDialogFragment.setContentView(args, R.layout.dialog_long_click_setting);
        fragment.setArguments(args);
        return fragment;
    }

    public void onResume() {
        super.onResume();

        final Rect frame = new Rect();
        getActivity().getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
        final int width = (int) (frame.width() * 0.8);
        getDialog().getWindow().setLayout(width, WRAP_CONTENT);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);
        preferences = new AppSharedPreferences(getContext());

        maxCaptureLengthEditText.setText(Integer.toString(preferences.getMaxCaptureLength() / 1000));
        picsExtractedEditText.setText(Integer.toString(preferences.getNumOfPicsExtracted()));

        // Start with cursor at end of text
        maxCaptureLengthEditText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                maxCaptureLengthEditText.setSelection(maxCaptureLengthEditText.getText().length());
                maxCaptureLengthEditText.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        picsExtractedEditText.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                picsExtractedEditText.setSelection(picsExtractedEditText.getText().length());
                picsExtractedEditText.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        return dialog;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        LongClickSettingListener listener = (LongClickSettingListener) getActivity();

        if (maxCaptureLengthEditText != null) {
            int maxCaptureLength = 0;
            try {
                maxCaptureLength = Integer.parseInt(maxCaptureLengthEditText.getText().toString());
            } catch (NumberFormatException e) {
                Util.logError("Poorly formatted maximum capture length", e);
            }

            if (maxCaptureLength < MIN_CAPTURE_LENGTH || maxCaptureLength > MAX_CAPTURE_LENGTH) {
                final String message = String.format("Length of capture must be between %s and %s seconds", MIN_CAPTURE_LENGTH, MAX_CAPTURE_LENGTH);
                AppDialogFragment.show((AppCompatActivity) getActivity(), message);
                return;
            }
            preferences.putMaxCaptureLength(maxCaptureLength * 1000);
        }

        if (picsExtractedEditText != null) {
            int picsExtracted = 0;
            try {
                picsExtracted = Integer.parseInt(picsExtractedEditText.getText().toString());
            } catch (NumberFormatException e) {
                Util.logError("Poorly formatted pics extracted", e);
            }

            if (picsExtracted < MIN_NUMBER_DETAIL_PICS || picsExtracted > MAX_NUMBER_DETAIL_PICS) {
                final String message = String.format("Count of pics must be between %s and %s", MIN_NUMBER_DETAIL_PICS, MAX_NUMBER_DETAIL_PICS);
                AppDialogFragment.show((AppCompatActivity) getActivity(), message);
                return;
            }
            preferences.putNumOfPicsExtracted(picsExtracted);
        }

        if (i == DialogInterface.BUTTON_POSITIVE) {
            listener.onLongClickSettingPositiveClick(0, 0);
        } else {
            listener.onLongClickSettingNegativeClick();
            dismiss();
            return;
        }
        dismiss();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
