package com.sister.ivana;

import android.content.DialogInterface;
import android.os.Bundle;

import com.sister.ivana.dialog.AppDialogFragment;

public class UpgradeAppDialogFragment extends AppDialogFragment {
    public interface UpgradeAppListener {
        void onUpgradeAppPositiveClick();

        void onUpgradeAppNegativeClick();
    }

    public static UpgradeAppDialogFragment newInstance(String version) {
        UpgradeAppDialogFragment fragment = new UpgradeAppDialogFragment();
        Bundle args = new Bundle();
        final String text = String.format("Version %s is now available. Would you like to upgrade?", version);
        AppDialogFragment.setMessage(args, text);
        AppDialogFragment.setPositiveButtonText(args, R.string.dialog_button_install);
        AppDialogFragment.setNegativeButtonText(args, R.string.dialog_button_no);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        UpgradeAppListener listener = (UpgradeAppListener) getActivity();
        if (i == DialogInterface.BUTTON_POSITIVE) {
            listener.onUpgradeAppPositiveClick();
            dismiss();
        } else if (i == DialogInterface.BUTTON_NEGATIVE) {
            listener.onUpgradeAppNegativeClick();
            dismiss();
        }
        dismiss();
    }
}
