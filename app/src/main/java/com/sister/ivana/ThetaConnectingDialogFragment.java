package com.sister.ivana;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.sister.ivana.dialog.AppDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ThetaConnectingDialogFragment extends AppDialogFragment {
    private Unbinder unbinder;
    @BindView(R.id.progress) ProgressBar progressBar;

    public static ThetaConnectingDialogFragment newInstance() {
        ThetaConnectingDialogFragment fragment = new ThetaConnectingDialogFragment();
        Bundle args = new Bundle();
        AppDialogFragment.setTitle(args, R.string.dialog_theta_connecting_title);
        AppDialogFragment.setContentView(args, R.layout.dialog_connecting_theta);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        unbinder = ButterKnife.bind(this, dialog);
        progressBar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.green), PorterDuff.Mode.MULTIPLY);
        return dialog;
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();
    }
}
